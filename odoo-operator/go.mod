module gitlab.com/nars/odoo-operator

go 1.13

require (
	github.com/Masterminds/goutils v1.1.0
	github.com/banzaicloud/operator-tools v0.16.0
	github.com/go-logr/logr v0.2.1
	github.com/onsi/ginkgo v1.12.1
	github.com/onsi/gomega v1.10.1
	github.com/zalando/postgres-operator v1.5.1-0.20200903060246-03437b63749e
	k8s.io/api v0.19.2
	k8s.io/apimachinery v0.19.2
	k8s.io/client-go v0.19.2
	sigs.k8s.io/controller-runtime v0.6.2
)

replace github.com/go-logr/zapr => github.com/go-logr/zapr v0.2.0
