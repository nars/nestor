// Copyright 2020 NDP Systèmes. All Rights Reserved.
// See LICENSE file for full licensing details.

package controllers

import (
	"context"

	. "github.com/onsi/ginkgo"
	. "github.com/onsi/gomega"
	appsv1alpha1 "gitlab.com/nars/odoo-operator/api/apps/v1alpha1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var _ = Describe("Odoo Controller", func() {
	Context("When creating an Odoo custom resource", func() {
		It("Should scaffold the whole application", func() {
			ctx := context.Background()
			basicOdoo := &appsv1alpha1.Odoo{
				ObjectMeta: metav1.ObjectMeta{
					Name:      "test-odoo",
					Namespace: "default",
				},
				Spec: appsv1alpha1.OdooSpec{
					Version: "12.0",
				},
			}
			basicOdoo.SetDefaults(k8sClient)
			Expect(k8sClient.Create(ctx, basicOdoo)).Should(Succeed())
		})
	})
})
