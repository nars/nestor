// Copyright 2021 NDP Systèmes. All Rights Reserved.
// See LICENSE file for full licensing details.

package controllers

import (
	"fmt"
	"strings"

	"github.com/banzaicloud/operator-tools/pkg/reconciler"
	appsv1alpha1 "gitlab.com/nars/odoo-operator/api/apps/v1alpha1"
	batchv1 "k8s.io/api/batch/v1"
	batchv1beta1 "k8s.io/api/batch/v1beta1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
)

func getBackupState(odoo *appsv1alpha1.Odoo) reconciler.DesiredState {
	if *odoo.Spec.Backup.Enabled {
		return reconciler.StatePresent
	}
	return reconciler.StateAbsent
}

func (r *OdooReconciler) odooBackupCronJob(odoo *appsv1alpha1.Odoo) (runtime.Object, reconciler.DesiredState, error) {
	values := getOdooValues(odoo)
	labels := values.labels
	labels["app"] = "odoo-job"
	var s3Targets []string
	for _, t := range odoo.Spec.Backup.S3Targets {
		s3Targets = append(s3Targets, fmt.Sprintf("%s:%s|%s:%s", t.Database.Secret, t.Database.Bucket, t.Filestore.Secret, t.Filestore.Bucket))
	}
	cronJob := &batchv1beta1.CronJob{
		ObjectMeta: metav1.ObjectMeta{
			Name:      values.backupName,
			Namespace: odoo.Namespace,
			Labels:    labels,
		},
		Spec: batchv1beta1.CronJobSpec{
			Schedule: odoo.Spec.Backup.Schedule,
			JobTemplate: batchv1beta1.JobTemplateSpec{
				Spec: batchv1.JobSpec{
					Template: corev1.PodTemplateSpec{
						ObjectMeta: metav1.ObjectMeta{
							Namespace: odoo.Namespace,
							Labels:    labels,
						},
						Spec: corev1.PodSpec{
							Containers: []corev1.Container{
								{
									Image:           odoo.Spec.Backup.Image,
									ImagePullPolicy: corev1.PullAlways,
									Name:            values.backupName,
									Env: []corev1.EnvVar{
										{
											Name: "SSH_PRIVATE_KEY",
											ValueFrom: &corev1.EnvVarSource{
												SecretKeyRef: &corev1.SecretKeySelector{
													LocalObjectReference: corev1.LocalObjectReference{
														Name: odoo.Spec.Sources.PullSecret,
													},
													Key: "deploy_key_NDP",
												},
											},
										},
										{
											Name:  "GIT_REPO",
											Value: odoo.Spec.Backup.Script.Repository,
										},
										{
											Name:  "GIT_BRANCH",
											Value: odoo.Spec.Backup.Script.Branch,
										},
										{
											Name:  "SCRIPT",
											Value: odoo.Spec.Backup.Script.Script,
										},
										{
											Name:  "INSTANCE_NAME",
											Value: odoo.Name,
										},
										{
											Name:  "S3_TARGETS",
											Value: strings.Join(s3Targets, ","),
										},
									},
								},
							},
							RestartPolicy:      corev1.RestartPolicyOnFailure,
							ServiceAccountName: odoo.Spec.Backup.ServiceAccountName,
						},
					},
				},
			},
		},
	}
	err := ctrl.SetControllerReference(odoo, cronJob, r.Scheme)
	return cronJob, getBackupState(odoo), err
}
