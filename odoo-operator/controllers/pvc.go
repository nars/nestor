// Copyright 2020 NDP Systèmes. All Rights Reserved.
// See LICENSE file for full licensing details.

package controllers

import (
	"github.com/banzaicloud/operator-tools/pkg/reconciler"
	appsv1alpha1 "gitlab.com/nars/odoo-operator/api/apps/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
)

const defaultVolumeSize = "8Gi"

func getPVCState(odoo *appsv1alpha1.Odoo) reconciler.DesiredState {
	if odoo.Spec.Persistence.Disabled {
		return reconciler.StateAbsent
	}
	return reconciler.StateCreated
}

func (r *OdooReconciler) odooPVC(odoo *appsv1alpha1.Odoo) (runtime.Object, reconciler.DesiredState, error) {
	values := getOdooValues(odoo)
	volumeSize := odoo.Spec.Persistence.Size
	if volumeSize.String() == "0" {
		volumeSize = resource.MustParse(defaultVolumeSize)
	}

	pvc := &corev1.PersistentVolumeClaim{
		TypeMeta: metav1.TypeMeta{},
		ObjectMeta: metav1.ObjectMeta{
			Name:      values.name,
			Namespace: odoo.Namespace,
			Labels:    values.labels,
		},
		Spec: corev1.PersistentVolumeClaimSpec{
			AccessModes: []corev1.PersistentVolumeAccessMode{
				corev1.ReadWriteOnce,
			},
			Resources: corev1.ResourceRequirements{
				Requests: corev1.ResourceList{
					corev1.ResourceStorage: volumeSize,
				},
			},
		},
	}
	scn := odoo.Spec.Persistence.StorageClassName
	switch scn {
	case "":
		// Leave pvc.Spec.StorageClassName to null to use default class
	case "-":
		// Set storageClassName: '' to disable dynamic provisioning
		pvc.Spec.StorageClassName = new(string)
	default:
		// Set the given storageClassName
		pvc.Spec.StorageClassName = &scn
	}
	return pvc, getPVCState(odoo), nil
}
