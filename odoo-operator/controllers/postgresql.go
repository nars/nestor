// Copyright 2020 NDP Systèmes. All Rights Reserved.
// See LICENSE file for full licensing details.

package controllers

import (
	"github.com/Masterminds/goutils"
	"github.com/banzaicloud/operator-tools/pkg/reconciler"
	postgresv1 "github.com/zalando/postgres-operator/pkg/apis/acid.zalan.do/v1"
	appsv1alpha1 "gitlab.com/nars/odoo-operator/api/apps/v1alpha1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
)

const defaultPGVolumeSize = "8Gi"

func getPostgresqlState(odoo *appsv1alpha1.Odoo) reconciler.DesiredState {
	if odoo.Spec.Postgresql.Secret != "" {
		return reconciler.StateAbsent
	}
	return reconciler.StatePresent
}

func (r *OdooReconciler) odooPostgresql(odoo *appsv1alpha1.Odoo) (runtime.Object, reconciler.DesiredState, error) {
	values := getOdooValues(odoo)
	password := odoo.Spec.Options.AdminPassword
	if password == "" {
		password, _ = goutils.CryptoRandomAlphaNumeric(10)
	}
	parameters := odoo.Spec.Postgresql.Parameters
	if parameters == nil {
		parameters = make(map[string]string)
	}
	pgVolumeSize := odoo.Spec.Postgresql.Volume.Size.String()
	if pgVolumeSize == "0" {
		pgVolumeSize = defaultPGVolumeSize
	}
	var replicas int32 = 2
	if odoo.Spec.Postgresql.Replicas != nil {
		replicas = *odoo.Spec.Postgresql.Replicas
	}
	labels := values.labels
	labels["app"] = "postgresql"

	postgresql := &postgresv1.Postgresql{
		TypeMeta: metav1.TypeMeta{},
		ObjectMeta: metav1.ObjectMeta{
			Name:      values.postgresName,
			Namespace: odoo.Namespace,
			Labels:    labels,
		},
		Spec: postgresv1.PostgresSpec{
			TeamID:      "ndp",
			ClusterName: values.postgresCluster,
			Volume: postgresv1.Volume{
				Size: pgVolumeSize,
			},
			NumberOfInstances: replicas,
			Users: map[string]postgresv1.UserFlags{
				"odoo": {"superuser", "createdb"},
			},
			EnableLogicalBackup: odoo.Spec.Postgresql.EnableLogicalBackups,
			PostgresqlParam: postgresv1.PostgresqlParam{
				PgVersion:  "12",
				Parameters: parameters,
			},
			Patroni: postgresv1.Patroni{
				InitDB: make(map[string]string),
				Slots:  make(map[string]map[string]string),
				PgHba:  []string{},
			},
			Resources: postgresv1.Resources{
				ResourceRequests: postgresv1.ResourceDescription{
					CPU:    odoo.Spec.Postgresql.Resources.ResourceRequests.CPU.String(),
					Memory: odoo.Spec.Postgresql.Resources.ResourceRequests.Memory.String(),
				},
				ResourceLimits: postgresv1.ResourceDescription{
					CPU:    odoo.Spec.Postgresql.Resources.ResourceLimits.CPU.String(),
					Memory: odoo.Spec.Postgresql.Resources.ResourceLimits.Memory.String(),
				},
			},
		},
	}
	return postgresql, getPostgresqlState(odoo), nil
}
