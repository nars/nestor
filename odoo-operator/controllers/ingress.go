// Copyright 2020 NDP Systèmes. All Rights Reserved.
// See LICENSE file for full licensing details.

package controllers

import (
	"fmt"

	"github.com/banzaicloud/operator-tools/pkg/reconciler"
	appsv1alpha1 "gitlab.com/nars/odoo-operator/api/apps/v1alpha1"
	networkingv1 "k8s.io/api/networking/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	ctrl "sigs.k8s.io/controller-runtime"
)

func getIngressState(odoo *appsv1alpha1.Odoo) reconciler.DesiredState {
	if !*odoo.Spec.Ingress.Enabled {
		return reconciler.StateAbsent
	}
	return reconciler.StatePresent
}

func (r *OdooReconciler) odooIngress(odoo *appsv1alpha1.Odoo) (runtime.Object, reconciler.DesiredState, error) {
	values := getOdooValues(odoo)
	hostname := odoo.Spec.Ingress.Host
	if hostname == "" {
		hostname = fmt.Sprintf("%s.%s", odoo.Name, odoo.Spec.Ingress.DefaultDomain)
	}
	implSpecific := networkingv1.PathTypeImplementationSpecific

	ingress := &networkingv1.Ingress{
		TypeMeta: metav1.TypeMeta{},
		ObjectMeta: metav1.ObjectMeta{
			Name:      values.name,
			Namespace: odoo.Namespace,
			Labels:    values.labels,
			Annotations: map[string]string{
				"ingress.kubernetes.io/hsts-max-age": "315360000",
				"ingress.kubernetes.io/ssl-redirect": "true",
				"cert-manager.io/cluster-issuer":     "letsencrypt",
			},
		},
		Spec: networkingv1.IngressSpec{
			TLS: []networkingv1.IngressTLS{
				{
					Hosts: []string{
						hostname,
					},
					SecretName: values.tlsSecret,
				},
			},
			Rules: []networkingv1.IngressRule{
				{
					Host: hostname,
					IngressRuleValue: networkingv1.IngressRuleValue{
						HTTP: &networkingv1.HTTPIngressRuleValue{
							Paths: []networkingv1.HTTPIngressPath{
								{
									Path:     "/",
									PathType: &implSpecific,
									Backend: networkingv1.IngressBackend{
										Service: &networkingv1.IngressServiceBackend{
											Name: values.name,
											Port: networkingv1.ServiceBackendPort{
												Name: "http",
											},
										},
									},
								},
								{
									Path:     "/longpolling",
									PathType: &implSpecific,
									Backend: networkingv1.IngressBackend{
										Service: &networkingv1.IngressServiceBackend{
											Name: values.name,
											Port: networkingv1.ServiceBackendPort{
												Name: "longpolling",
											},
										},
									},
								},
							},
						},
					},
				},
			},
		},
	}
	err := ctrl.SetControllerReference(odoo, ingress, r.Scheme)
	return ingress, getIngressState(odoo), err
}
