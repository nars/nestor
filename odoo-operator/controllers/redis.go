// Copyright 2020 NDP Systèmes. All Rights Reserved.
// See LICENSE file for full licensing details.

package controllers

import (
	"fmt"

	"github.com/Masterminds/goutils"
	"github.com/banzaicloud/operator-tools/pkg/reconciler"
	appsv1alpha1 "gitlab.com/nars/odoo-operator/api/apps/v1alpha1"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/intstr"
	ctrl "sigs.k8s.io/controller-runtime"
)

var defaultRedisReplicas int32 = 1

func getRedisState(odoo *appsv1alpha1.Odoo, update bool) reconciler.DesiredState {
	if odoo.Spec.Redis.Enabled {
		if update {
			return reconciler.StatePresent
		}
		return reconciler.StateCreated
	}
	return reconciler.StateAbsent
}

func (r *OdooReconciler) odooRedisSecret(odoo *appsv1alpha1.Odoo) (runtime.Object, reconciler.DesiredState, error) {
	values := getOdooValues(odoo)
	labels := values.labels
	labels["app"] = "redis"
	password, _ := goutils.CryptoRandomAlphaNumeric(10)

	secret := &corev1.Secret{
		TypeMeta: metav1.TypeMeta{},
		ObjectMeta: metav1.ObjectMeta{
			Name:      values.redisName,
			Namespace: odoo.Namespace,
			Labels:    labels,
		},
		StringData: map[string]string{
			"redis-password": password,
		},
	}
	return secret, getRedisState(odoo, false), nil
}

func (r *OdooReconciler) odooRedisHealthConfigMap(odoo *appsv1alpha1.Odoo) (runtime.Object, reconciler.DesiredState, error) {
	values := getOdooValues(odoo)
	labels := values.labels
	labels["app"] = "redis"

	cm := &corev1.ConfigMap{
		ObjectMeta: metav1.ObjectMeta{
			Name:      values.redisHealthCM,
			Namespace: odoo.Namespace,
			Labels:    labels,
		},
		Data: map[string]string{
			"ping_liveness_local.sh": `#!/bin/bash

no_auth_warning=$([[ "$(redis-cli --version)" =~ (redis-cli 5.*) ]] && echo
--no-auth-warning)

response=$(
  timeout -s 3 $1 \
  redis-cli \
    -a $REDIS_PASSWORD $no_auth_warning \
    -h localhost \
    -p $REDIS_PORT \
    ping
)

if [ "$response" != "PONG" ] && [ "$response" != "LOADING Redis is loading
the dataset in memory" ]; then
  echo "$response"
  exit 1
fi
`,
			"ping_liveness_local_and_master.sh": `script_dir="$(dirname "$0")"
exit_status=0
"$script_dir/ping_liveness_local.sh" $1 || exit_status=$?
"$script_dir/ping_liveness_master.sh" $1 || exit_status=$?
exit $exit_status
`,
			"ping_liveness_master.sh": `#!/bin/bash

no_auth_warning=$([[ "$(redis-cli --version)" =~ (redis-cli 5.*) ]] && echo
--no-auth-warning)

response=$(
	timeout -s 3 $1 \
	redis-cli \
		-a $REDIS_MASTER_PASSWORD $no_auth_warning \
		-h $REDIS_MASTER_HOST \
		-p $REDIS_MASTER_PORT_NUMBER \
	ping
)	

if [ "$response" != "PONG" ] && [ "$response" != "LOADING Redis is loading
	the dataset in memory" ]; then
	echo "$response"
	exit 1
fi
`,
			"ping_readiness_local.sh": `#!/bin/bash

no_auth_warning=$([[ "$(redis-cli --version)" =~ (redis-cli 5.*) ]] && echo
--no-auth-warning)

response=$(
	timeout -s 3 $1 \
	redis-cli \
		-a $REDIS_PASSWORD $no_auth_warning \
		-h localhost \
		-p $REDIS_PORT \
	ping
)

if [ "$response" != "PONG" ]; then
	echo "$response"
	exit 1
fi
`,
			"ping_readiness_local_and_master.sh": `
script_dir="$(dirname "$0")"
exit_status=0
"$script_dir/ping_readiness_local.sh" $1 || exit_status=$?
"$script_dir/ping_readiness_master.sh" $1 || exit_status=$?
exit $exit_status
`,
			"ping_readiness_master.sh": `#!/bin/bash

no_auth_warning=$([[ "$(redis-cli --version)" =~ (redis-cli 5.*) ]] && echo
--no-auth-warning)

response=$(
	timeout -s 3 $1 \
	redis-cli \
		-a $REDIS_MASTER_PASSWORD $no_auth_warning \
		-h $REDIS_MASTER_HOST \
		-p $REDIS_MASTER_PORT_NUMBER \
	ping
)

if [ "$response" != "PONG" ]; then
	echo "$response"
	exit 1
fi
`,
		},
	}
	err := ctrl.SetControllerReference(odoo, cm, r.Scheme)
	return cm, getRedisState(odoo, false), err
}

func (r *OdooReconciler) odooRedisConfigConfigMap(odoo *appsv1alpha1.Odoo) (runtime.Object, reconciler.DesiredState, error) {
	values := getOdooValues(odoo)
	labels := values.labels
	labels["app"] = "redis"

	cm := &corev1.ConfigMap{
		ObjectMeta: metav1.ObjectMeta{
			Name:      values.redisName,
			Namespace: odoo.Namespace,
			Labels:    labels,
		},
		Data: map[string]string{
			"master.conf": `
dir /data
rename-command FLUSHDB ""
rename-command FLUSHALL ""
`,
			"redis.conf": `
# User-supplied configuration:
# Enable AOF https://redis.io/topics/persistence#append-only-file
appendonly yes
# Disable RDB persistence, AOF persistence already enabled.
save ""
`,
			"replica.conf": `
dir /data
slave-read-only yes
rename-command FLUSHDB ""
rename-command FLUSHALL ""
`,
		},
	}
	err := ctrl.SetControllerReference(odoo, cm, r.Scheme)
	return cm, getRedisState(odoo, false), err
}

func (r *OdooReconciler) odooRedisStatefulSet(odoo *appsv1alpha1.Odoo) (runtime.Object, reconciler.DesiredState, error) {
	values := getOdooValues(odoo)
	labels := values.labels
	labels["app"] = "redis"
	healthCMName := fmt.Sprintf("%s-health", values.redisName)
	serviceName := fmt.Sprintf("%s-headless", values.redisName)
	var (
		mode420  int32 = 420
		mode493  int32 = 493
		user1001 int64 = 1001
	)

	sf := &appsv1.StatefulSet{
		ObjectMeta: metav1.ObjectMeta{
			Name:      values.redisName,
			Namespace: odoo.Namespace,
			Labels:    labels,
		},
		Spec: appsv1.StatefulSetSpec{
			Replicas: &defaultRedisReplicas,
			Selector: &metav1.LabelSelector{
				MatchLabels: labels,
			},
			Template: corev1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Labels: labels,
				},
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{{
						Name:  "redis",
						Image: "docker.io/bitnami/redis:6.0.6-debian-10-r10",
						Command: []string{
							"/bin/bash",
							"-c",
							`
if [[ -n $REDIS_PASSWORD_FILE ]]; then
  password_aux=$(cat ${REDIS_PASSWORD_FILE})
  export REDIS_PASSWORD=$password_aux
fi
if [[ ! -f /opt/bitnami/redis/etc/master.conf ]];then
  cp /opt/bitnami/redis/mounted-etc/master.conf /opt/bitnami/redis/etc/master.conf
fi
if [[ ! -f /opt/bitnami/redis/etc/redis.conf ]];then
  cp /opt/bitnami/redis/mounted-etc/redis.conf /opt/bitnami/redis/etc/redis.conf
fi
ARGS=("--port" "${REDIS_PORT}")
ARGS+=("--requirepass" "${REDIS_PASSWORD}")
ARGS+=("--masterauth" "${REDIS_PASSWORD}")
ARGS+=("--include" "/opt/bitnami/redis/etc/redis.conf")
ARGS+=("--include" "/opt/bitnami/redis/etc/master.conf")
/run.sh ${ARGS[@]}`,
						},
						Ports: []corev1.ContainerPort{
							{
								ContainerPort: 6379,
								Name:          "redis",
							},
						},
						Env: odooRedisEnv(odoo),
						LivenessProbe: &corev1.Probe{
							Handler: corev1.Handler{
								Exec: &corev1.ExecAction{
									Command: []string{
										"sh",
										"-c",
										"/health/ping_liveness_local.sh 5",
									},
								},
							},
							InitialDelaySeconds: 5,
							TimeoutSeconds:      5,
							PeriodSeconds:       5,
							SuccessThreshold:    1,
							FailureThreshold:    5,
						},
						ReadinessProbe: &corev1.Probe{
							Handler: corev1.Handler{
								Exec: &corev1.ExecAction{
									Command: []string{
										"sh",
										"-c",
										"/health/ping_readiness_local.sh 5",
									},
								},
							},
							InitialDelaySeconds: 5,
							TimeoutSeconds:      5,
							PeriodSeconds:       5,
							SuccessThreshold:    1,
							FailureThreshold:    5,
						},
						VolumeMounts: []corev1.VolumeMount{
							{
								Name:      "health",
								MountPath: "/health",
							},
							{
								Name:      "redis-data",
								MountPath: "/data",
							},
							{
								Name:      "config",
								MountPath: "/opt/bitnami/redis/mounted-etc",
							},
							{
								Name:      "redis-tmp-conf",
								MountPath: "/opt/bitnami/redis/etc",
							},
						},
						SecurityContext: &corev1.SecurityContext{
							RunAsUser: &user1001,
						},
						Resources: corev1.ResourceRequirements{
							Requests: corev1.ResourceList{
								corev1.ResourceCPU:    resource.MustParse("20m"),
								corev1.ResourceMemory: resource.MustParse("100Mi"),
							},
						},
					}},
					Volumes: []corev1.Volume{
						{
							Name: "health",
							VolumeSource: corev1.VolumeSource{
								ConfigMap: &corev1.ConfigMapVolumeSource{
									LocalObjectReference: corev1.LocalObjectReference{
										Name: healthCMName,
									},
									DefaultMode: &mode493,
								},
							},
						},
						{
							Name: "config",
							VolumeSource: corev1.VolumeSource{
								ConfigMap: &corev1.ConfigMapVolumeSource{
									LocalObjectReference: corev1.LocalObjectReference{
										Name: values.redisName,
									},
									DefaultMode: &mode420,
								},
							},
						},
						{
							Name: "redis-data",
							VolumeSource: corev1.VolumeSource{
								EmptyDir: &corev1.EmptyDirVolumeSource{},
							},
						},
						{
							Name: "redis-tmp-conf",
							VolumeSource: corev1.VolumeSource{
								EmptyDir: &corev1.EmptyDirVolumeSource{},
							},
						},
					},
				},
			},
			ServiceName: serviceName,
		},
	}
	if len(odoo.Spec.NodeSelectorTerms) != 0 {
		sf.Spec.Template.Spec.Affinity = &corev1.Affinity{
			NodeAffinity: &corev1.NodeAffinity{
				RequiredDuringSchedulingIgnoredDuringExecution: &corev1.NodeSelector{
					NodeSelectorTerms: odoo.Spec.NodeSelectorTerms,
				},
			},
		}
	}
	err := ctrl.SetControllerReference(odoo, sf, r.Scheme)
	return sf, getRedisState(odoo, true), err
}

func odooRedisEnv(odoo *appsv1alpha1.Odoo) []corev1.EnvVar {
	values := getOdooValues(odoo)

	envVars := []corev1.EnvVar{
		{Name: "REDIS_REPLICATION_MODE", Value: "master"},
		{Name: "REDIS_PASSWORD", ValueFrom: &corev1.EnvVarSource{
			SecretKeyRef: &corev1.SecretKeySelector{
				LocalObjectReference: corev1.LocalObjectReference{
					Name: values.redisName,
				},
				Key: "redis-password",
			}},
		},
		{Name: "REDIS_TLS_ENABLED", Value: "no"},
		{Name: "REDIS_PORT", Value: "6379"},
	}
	return envVars
}

func odooRedisServiceCommon(odoo *appsv1alpha1.Odoo) *corev1.Service {
	values := getOdooValues(odoo)
	labels := values.labels
	labels["app"] = "redis"

	service := &corev1.Service{
		TypeMeta: metav1.TypeMeta{},
		ObjectMeta: metav1.ObjectMeta{
			Namespace: odoo.Namespace,
			Labels:    labels,
		},
		Spec: corev1.ServiceSpec{
			Type: corev1.ServiceTypeClusterIP,
			Ports: []corev1.ServicePort{
				{
					Name:       "redis",
					Port:       6379,
					TargetPort: intstr.Parse("redis"),
				},
			},
			Selector: labels,
		},
	}
	return service
}

func (r *OdooReconciler) odooRedisService(odoo *appsv1alpha1.Odoo) (runtime.Object, reconciler.DesiredState, error) {
	service := odooRedisServiceCommon(odoo)
	service.ObjectMeta.Name = getOdooValues(odoo).redisHost
	err := ctrl.SetControllerReference(odoo, service, r.Scheme)
	return service, getRedisState(odoo, true), err
}

func (r *OdooReconciler) odooRedisServiceHeadless(odoo *appsv1alpha1.Odoo) (runtime.Object, reconciler.DesiredState, error) {
	service := odooRedisServiceCommon(odoo)
	service.ObjectMeta.Name = getOdooValues(odoo).redisServiceHeadless
	service.Spec.ClusterIP = corev1.ClusterIPNone
	err := ctrl.SetControllerReference(odoo, service, r.Scheme)
	return service, getRedisState(odoo, true), err
}
