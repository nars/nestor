// Copyright 2020 NDP Systèmes. All Rights Reserved.
// See LICENSE file for full licensing details.

package controllers

import (
	"github.com/Masterminds/goutils"
	"github.com/banzaicloud/operator-tools/pkg/reconciler"
	appsv1alpha1 "gitlab.com/nars/odoo-operator/api/apps/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
)

func (r *OdooReconciler) odooSecret(odoo *appsv1alpha1.Odoo) (runtime.Object, reconciler.DesiredState, error) {
	values := getOdooValues(odoo)
	password := odoo.Spec.Options.AdminPassword
	if password == "" {
		password, _ = goutils.CryptoRandomAlphaNumeric(10)
	}

	secret := &corev1.Secret{
		TypeMeta: metav1.TypeMeta{},
		ObjectMeta: metav1.ObjectMeta{
			Name:      values.name,
			Namespace: odoo.Namespace,
			Labels:    values.labels,
		},
		StringData: map[string]string{
			"odoo-admin-password": password,
		},
	}
	return secret, reconciler.StateCreated, nil
}
