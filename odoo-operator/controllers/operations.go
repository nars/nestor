// Copyright 2020 NDP Systèmes. All Rights Reserved.
// See LICENSE file for full licensing details.

package controllers

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/banzaicloud/operator-tools/pkg/reconciler"
	appsv1alpha1 "gitlab.com/nars/odoo-operator/api/apps/v1alpha1"
	batchv1 "k8s.io/api/batch/v1"
	corev1 "k8s.io/api/core/v1"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

const (
	operationInProgress = "<IN PROGRESS>"
	jobKeepDuration     = 6 * time.Hour
)

func (r *OdooReconciler) reconcileOperations(ctx context.Context, rec reconciler.ResourceReconciler, odoo *appsv1alpha1.Odoo) (error, bool) {
	log := r.Log.WithValues("odoo", types.NamespacedName{Name: odoo.Name, Namespace: odoo.Namespace})
	values := getOdooValues(odoo)
	if odoo.Status.RunningOperationJob != "" {
		jobNS := types.NamespacedName{Name: odoo.Status.RunningOperationJob, Namespace: odoo.Namespace}
		job := &batchv1.Job{}
		err := r.Get(ctx, jobNS, job)
		if err != nil {
			if apierrors.IsNotFound(err) {
				return r.cleanOperationsSpec(ctx, odoo), false
			}
			return err, false
		}
		var done bool
		for _, c := range job.Status.Conditions {
			if (c.Type == batchv1.JobComplete || c.Type == batchv1.JobFailed) && c.Status == corev1.ConditionTrue {
				done = true
				break
			}
		}
		if !done {
			// Exit early if job is still running
			return nil, false
		}
		var message string
		if job.Status.Failed > 0 {
			for _, c := range job.Status.Conditions {
				message += c.Reason + "\n" + c.Message + "\n"
			}
		}
		// Delete job
		err = r.Delete(ctx, job)
		if err != nil && !apierrors.IsNotFound(err) {
			return err, false
		}
		// Delete job pods after jobKeepDuration
		labels := values.labels
		labels["app"] = "odoo-job"
		var pods corev1.PodList
		err = r.List(ctx, &pods, client.MatchingLabels(labels), client.InNamespace(odoo.Namespace))
		if err != nil {
			return err, false
		}
		for _, pod := range pods.Items {
			if time.Since(pod.Status.StartTime.Time) > jobKeepDuration {
				err = r.Delete(ctx, &pod)
			}
		}
		err = r.cleanOperationsSpec(ctx, odoo)
		if err != nil {
			return err, false
		}
		if message != "" {
			return errors.New(message), false
		}
		return nil, false
	}
	if odoo.Spec.Operations.Install == operationInProgress || odoo.Spec.Operations.Update == operationInProgress {
		// We should not be here
		log.Info("Unexpected situation: resetting operations")
		err := r.cleanOperationsSpec(ctx, odoo)
		if err != nil {
			return err, false
		}
	}
	if odoo.Spec.Operations.Install != "" {

	}
	err, requeue, done := r.createJob(ctx, rec, odoo, &odoo.Spec.Operations.Install, &odoo.Status.RunningOperationJob, values.jobInstall, r.odooOperationJobInstall)
	if done {
		return err, requeue
	}
	err, requeue, done = r.createJob(ctx, rec, odoo, &odoo.Spec.Operations.Update, &odoo.Status.RunningOperationJob, values.jobUpdate, r.odooOperationJobUpdate)
	if done {
		return err, requeue
	}
	return nil, false
}

func (r *OdooReconciler) createJob(ctx context.Context, rec reconciler.ResourceReconciler, odoo *appsv1alpha1.Odoo,
	modules *string, jobStatus *string, jobName string, jobFunc func(*appsv1alpha1.Odoo) (runtime.Object, reconciler.DesiredState, error)) (error, bool, bool) {

	if *modules != "" {
		var (
			err     error
			requeue bool
		)
		job, state, err := jobFunc(odoo)
		if err != nil {
			return err, false, true
		}
		if _, _, err = rec.CreateIfNotExist(job, state); err != nil {
			return err, requeue, true
		}
		patch := client.MergeFrom(odoo.DeepCopy())
		*modules = ""
		err = r.Patch(ctx, odoo, patch)
		if err != nil {
			return err, false, true
		}
		*jobStatus = jobName
		err = r.Status().Update(ctx, odoo)
		if err != nil {
			return err, false, true
		}
		return nil, true, true
	}
	return nil, false, false
}

func (r *OdooReconciler) cleanOperationsSpec(ctx context.Context, odoo *appsv1alpha1.Odoo) error {
	patch := client.MergeFrom(odoo.DeepCopy())
	odoo.Status.RunningOperationJob = ""
	err := r.Status().Patch(ctx, odoo, patch)
	if err != nil {
		return err
	}
	if odoo.Spec.Operations.Install == operationInProgress {
		odoo.Spec.Operations.Install = ""
	}
	if odoo.Spec.Operations.Update == operationInProgress {
		odoo.Spec.Operations.Update = ""
	}
	return r.Patch(ctx, odoo, patch)
}

func (r *OdooReconciler) odooOperationJobInstall(odoo *appsv1alpha1.Odoo) (runtime.Object, reconciler.DesiredState, error) {
	job, err := r.odooOperationJobCommon(odoo, false)
	return job, reconciler.StateCreated, err
}

func (r *OdooReconciler) odooOperationJobUpdate(odoo *appsv1alpha1.Odoo) (runtime.Object, reconciler.DesiredState, error) {
	job, err := r.odooOperationJobCommon(odoo, true)
	return job, reconciler.StateCreated, err
}

func (r *OdooReconciler) odooOperationJobCommon(odoo *appsv1alpha1.Odoo, update bool) (*batchv1.Job, error) {
	values := getOdooValues(odoo)
	env := odooJobEnv(odoo)
	var jobName string
	switch update {
	case false:
		jobName = values.jobInstall
		env = append(env, corev1.EnvVar{
			Name:  "INSTALL",
			Value: odoo.Spec.Operations.Install,
		})
	case true:
		jobName = values.jobUpdate
		env = append(env, corev1.EnvVar{
			Name:  "UPDATE",
			Value: odoo.Spec.Operations.Update,
		})
	}
	labels := values.labels
	labels["app"] = "odoo-job"
	var zero int32

	job := &batchv1.Job{
		ObjectMeta: metav1.ObjectMeta{
			Name:      jobName,
			Namespace: odoo.Namespace,
			Labels:    labels,
		},
		Spec: batchv1.JobSpec{
			BackoffLimit: &zero,
			Template: corev1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Name:      jobName,
					Namespace: odoo.Namespace,
					Labels:    labels,
				},
				Spec: corev1.PodSpec{
					Containers: []corev1.Container{{
						Image:           fmt.Sprintf("%s:%s", odoo.Spec.DockerImage, odoo.Spec.Version),
						Name:            jobName,
						ImagePullPolicy: corev1.PullAlways,
						Env:             env,
						Resources: corev1.ResourceRequirements{
							Limits: corev1.ResourceList{
								corev1.ResourceCPU:    odoo.Spec.Resources.ResourceLimits.CPU,
								corev1.ResourceMemory: odoo.Spec.Resources.ResourceLimits.Memory,
							},
							Requests: corev1.ResourceList{
								corev1.ResourceCPU:    odoo.Spec.Resources.ResourceRequests.CPU,
								corev1.ResourceMemory: odoo.Spec.Resources.ResourceRequests.Memory,
							},
						},
						VolumeMounts: []corev1.VolumeMount{
							{
								Name:      "odoo-filestore",
								MountPath: "/var/lib/odoo/filestore",
							},
							{
								Name:      "odoo-sources",
								MountPath: "/opt",
							},
						},
					}},
					RestartPolicy: corev1.RestartPolicyNever,

					Volumes: []corev1.Volume{
						odooFilestoreVolume(odoo, values.name),
						{
							Name: "odoo-sources",
							VolumeSource: corev1.VolumeSource{
								EmptyDir: &corev1.EmptyDirVolumeSource{},
							},
						},
					},
				},
			},
		},
	}
	err := ctrl.SetControllerReference(odoo, job, r.Scheme)
	return job, err
}

func odooJobEnv(odoo *appsv1alpha1.Odoo) []corev1.EnvVar {
	envVars, _, modules := odooBaseEnv(odoo)
	envVars = append(envVars,
		corev1.EnvVar{
			Name:  "LOAD",
			Value: modules,
		},
		corev1.EnvVar{
			Name:  "STOP_AFTER_INIT",
			Value: "True",
		},
		corev1.EnvVar{
			Name:  "WORKERS",
			Value: "1",
		},
	)
	return envVars
}
