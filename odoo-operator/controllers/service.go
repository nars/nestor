// Copyright 2020 NDP Systèmes. All Rights Reserved.
// See LICENSE file for full licensing details.

package controllers

import (
	"github.com/banzaicloud/operator-tools/pkg/reconciler"
	appsv1alpha1 "gitlab.com/nars/odoo-operator/api/apps/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/intstr"
	ctrl "sigs.k8s.io/controller-runtime"
)

func (r *OdooReconciler) odooService(odoo *appsv1alpha1.Odoo) (runtime.Object, reconciler.DesiredState, error) {
	values := getOdooValues(odoo)

	service := &corev1.Service{
		TypeMeta: metav1.TypeMeta{},
		ObjectMeta: metav1.ObjectMeta{
			Name:      values.name,
			Namespace: odoo.Namespace,
			Labels:    values.labels,
		},
		Spec: corev1.ServiceSpec{
			Type: corev1.ServiceTypeClusterIP,
			Ports: []corev1.ServicePort{
				{
					Name:       "http",
					Port:       8069,
					TargetPort: intstr.Parse("http"),
				},
				{
					Name:       "longpolling",
					Port:       8072,
					TargetPort: intstr.Parse("longpolling"),
				},
			},
			Selector: values.labels,
		},
	}
	err := ctrl.SetControllerReference(odoo, service, r.Scheme)
	return service, reconciler.StateCreated, err
}

func (r *OdooReconciler) odooJobWorkerService(odoo *appsv1alpha1.Odoo) (runtime.Object, reconciler.DesiredState, error) {
	values := getOdooValues(odoo)
	labels := values.labels
	labels["app"] = "odoo-jobworker"

	service := &corev1.Service{
		TypeMeta: metav1.TypeMeta{},
		ObjectMeta: metav1.ObjectMeta{
			Name:      values.jobWorker,
			Namespace: odoo.Namespace,
			Labels:    labels,
		},
		Spec: corev1.ServiceSpec{
			Type: corev1.ServiceTypeClusterIP,
			Ports: []corev1.ServicePort{
				{
					Name:       "http",
					Port:       8069,
					TargetPort: intstr.Parse("http"),
				},
				{
					Name:       "longpolling",
					Port:       8072,
					TargetPort: intstr.Parse("longpolling"),
				},
			},
			Selector: labels,
		},
	}
	err := ctrl.SetControllerReference(odoo, service, r.Scheme)
	return service, getJobDeploymentState(odoo, false), err
}
