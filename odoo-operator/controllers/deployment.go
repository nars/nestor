// Copyright 2020 NDP Systèmes. All Rights Reserved.
// See LICENSE file for full licensing details.

package controllers

import (
	"fmt"
	"sort"
	"strings"

	"github.com/banzaicloud/operator-tools/pkg/reconciler"
	appsv1alpha1 "gitlab.com/nars/odoo-operator/api/apps/v1alpha1"
	appsv1 "k8s.io/api/apps/v1"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/util/intstr"
	ctrl "sigs.k8s.io/controller-runtime"
)

const (
	defaultJobRunnerMemoryRequest = "300Mi"
	defaultJobRunnerMemoryLimit   = "800Mi"
	defaultJobRunnerCPURequest    = "25m"
	defaultJobRunnerCPULimit      = "250m"
)

func getJobDeploymentState(odoo *appsv1alpha1.Odoo, update bool) reconciler.DesiredState {
	if *odoo.Spec.Options.QueueJobs.Replicas > 0 {
		if update {
			return reconciler.StatePresent
		}
		return reconciler.StateCreated
	}
	return reconciler.StateAbsent
}

func (r *OdooReconciler) odooMainDeployment(odoo *appsv1alpha1.Odoo) (runtime.Object, reconciler.DesiredState, error) {
	values := getOdooValues(odoo)
	replicas := *odoo.Spec.Replicas
	switch {
	case odoo.Spec.Disabled:
		replicas = 0
	case odoo.Status.RunningOperationJob != "":
		replicas = 0
	case !odoo.Spec.Redis.Enabled:
		replicas = 1
	}
	dep, err := r.odooCommonDeployment(odoo, values.name, values.labels, replicas, odooMainDeploymentEnv(odoo))
	return dep, reconciler.StatePresent, err
}

func (r *OdooReconciler) odooJobRunnerDeployment(odoo *appsv1alpha1.Odoo) (runtime.Object, reconciler.DesiredState, error) {
	values := getOdooValues(odoo)
	replicas := int32(1)
	switch {
	case odoo.Spec.Disabled:
		replicas = 0
	case odoo.Status.RunningOperationJob != "":
		replicas = 0
	}
	labels := values.labels
	labels["app"] = "odoo-jobrunner"
	dep, err := r.odooCommonDeployment(odoo, values.jobRunner, labels, replicas, odooJobRunnerDeploymentEnv(odoo))
	if err != nil {
		return nil, nil, err
	}
	dep.Spec.Template.Spec.Containers[0].Resources = corev1.ResourceRequirements{
		Limits: corev1.ResourceList{
			corev1.ResourceCPU:    resource.MustParse(defaultJobRunnerCPULimit),
			corev1.ResourceMemory: resource.MustParse(defaultJobRunnerMemoryLimit),
		},
		Requests: corev1.ResourceList{
			corev1.ResourceCPU:    resource.MustParse(defaultJobRunnerCPURequest),
			corev1.ResourceMemory: resource.MustParse(defaultJobRunnerMemoryRequest),
		},
	}
	return dep, getJobDeploymentState(odoo, true), nil
}

func (r *OdooReconciler) odooJobWorkerDeployment(odoo *appsv1alpha1.Odoo) (runtime.Object, reconciler.DesiredState, error) {
	values := getOdooValues(odoo)
	replicas := *odoo.Spec.Options.QueueJobs.Replicas
	switch {
	case odoo.Spec.Disabled:
		replicas = 0
	case odoo.Status.RunningOperationJob != "":
		replicas = 0
	}
	labels := values.labels
	labels["app"] = "odoo-jobworker"
	dep, err := r.odooCommonDeployment(odoo, values.jobWorker, labels, replicas, odooJobWorkerDeploymentEnv(odoo))
	return dep, getJobDeploymentState(odoo, true), err
}

func (r *OdooReconciler) odooCommonDeployment(odoo *appsv1alpha1.Odoo, name string, labels map[string]string, replicas int32, env []corev1.EnvVar) (*appsv1.Deployment, error) {
	dep := &appsv1.Deployment{
		ObjectMeta: metav1.ObjectMeta{
			Name:      name,
			Namespace: odoo.Namespace,
			Labels:    labels,
		},
		Spec: appsv1.DeploymentSpec{
			Replicas: &replicas,
			Selector: &metav1.LabelSelector{
				MatchLabels: labels,
			},
			Template: corev1.PodTemplateSpec{
				ObjectMeta: metav1.ObjectMeta{
					Labels: labels,
				},
				Spec: corev1.PodSpec{
					Affinity: &corev1.Affinity{
						PodAntiAffinity: &corev1.PodAntiAffinity{
							PreferredDuringSchedulingIgnoredDuringExecution: []corev1.WeightedPodAffinityTerm{
								{
									Weight: 10,
									PodAffinityTerm: corev1.PodAffinityTerm{
										LabelSelector: &metav1.LabelSelector{
											MatchExpressions: []metav1.LabelSelectorRequirement{
												{
													Key:      "release",
													Operator: metav1.LabelSelectorOpIn,
													Values:   []string{labels["release"]},
												},
												{
													Key:      "app",
													Operator: metav1.LabelSelectorOpIn,
													Values:   []string{labels["app"]},
												},
											},
										},
										TopologyKey: "kubernetes.io/hostname",
									},
								},
							},
						},
					},
					Containers: []corev1.Container{{
						Image:           fmt.Sprintf("%s:%s", odoo.Spec.DockerImage, odoo.Spec.Version),
						Name:            name,
						ImagePullPolicy: corev1.PullAlways,
						Env:             env,
						Ports: []corev1.ContainerPort{
							{
								ContainerPort: 8069,
								Name:          "http",
							},
							{
								ContainerPort: 8072,
								Name:          "longpolling",
							},
						},
						LivenessProbe: &corev1.Probe{
							Handler: corev1.Handler{
								HTTPGet: &corev1.HTTPGetAction{
									Path: "/",
									Port: intstr.FromString("http"),
								},
							},
							InitialDelaySeconds: 1800,
							TimeoutSeconds:      5,
							PeriodSeconds:       30,
							SuccessThreshold:    1,
							FailureThreshold:    5,
						},
						ReadinessProbe: &corev1.Probe{
							Handler: corev1.Handler{
								HTTPGet: &corev1.HTTPGetAction{
									Path: "/",
									Port: intstr.FromString("http"),
								},
							},
							InitialDelaySeconds: 15,
							TimeoutSeconds:      5,
							PeriodSeconds:       10,
							SuccessThreshold:    1,
							FailureThreshold:    6,
						},
						Resources: corev1.ResourceRequirements{
							Limits: corev1.ResourceList{
								corev1.ResourceCPU:    odoo.Spec.Resources.ResourceLimits.CPU,
								corev1.ResourceMemory: odoo.Spec.Resources.ResourceLimits.Memory,
							},
							Requests: corev1.ResourceList{
								corev1.ResourceCPU:    odoo.Spec.Resources.ResourceRequests.CPU,
								corev1.ResourceMemory: odoo.Spec.Resources.ResourceRequests.Memory,
							},
						},
						VolumeMounts: []corev1.VolumeMount{
							{
								Name:      "odoo-filestore",
								MountPath: "/var/lib/odoo/filestore",
							},
							{
								Name:      "odoo-sessions",
								MountPath: "/var/lib/odoo/sessions",
							},
							{
								Name:      "odoo-sources",
								MountPath: "/opt",
							},
						},
					}},
					Volumes: []corev1.Volume{
						odooFilestoreVolume(odoo, getOdooValues(odoo).name),
						{
							Name: "odoo-sessions",
							VolumeSource: corev1.VolumeSource{
								EmptyDir: &corev1.EmptyDirVolumeSource{},
							},
						},
						{
							Name: "odoo-sources",
							VolumeSource: corev1.VolumeSource{
								EmptyDir: &corev1.EmptyDirVolumeSource{},
							},
						},
					},
				},
			},
		},
	}
	if len(odoo.Spec.NodeSelectorTerms) != 0 {
		dep.Spec.Template.Spec.Affinity.NodeAffinity = &corev1.NodeAffinity{
			RequiredDuringSchedulingIgnoredDuringExecution: &corev1.NodeSelector{
				NodeSelectorTerms: odoo.Spec.NodeSelectorTerms,
			},
		}
	}
	err := ctrl.SetControllerReference(odoo, dep, r.Scheme)
	return dep, err
}

func appendQueueJobVars(odoo *appsv1alpha1.Odoo, envVars []corev1.EnvVar, version string, modules string) ([]corev1.EnvVar, string) {
	varName := "ODOO_QUEUE_JOB_CHANNELS"
	hostVarName := "ODOO_QUEUE_JOB_HOST"
	connectorModule := "queue_job"
	if version == "8.0" || version == "9.0" {
		varName = "ODOO_CONNECTOR_CHANNELS"
		hostVarName = "ODOO_CONNECTOR_HOST"
		connectorModule = "connector"
	}
	modules += "," + connectorModule
	host := "localhost"
	if odoo.Spec.Options.QueueJobs.Enabled && *odoo.Spec.Options.QueueJobs.Replicas > 0 {
		host = getOdooValues(odoo).jobWorker
	}
	envVars = append(envVars,
		corev1.EnvVar{
			Name:  varName,
			Value: odoo.Spec.Options.QueueJobs.Channels,
		},
		corev1.EnvVar{
			Name:  hostVarName,
			Value: host,
		})
	return envVars, modules
}

func odooJobRunnerDeploymentEnv(odoo *appsv1alpha1.Odoo) []corev1.EnvVar {
	envVars, version, modules := odooCommonDeploymentEnv(odoo)
	envVars, modules = appendQueueJobVars(odoo, envVars, version, modules)
	envVars = append(envVars,
		corev1.EnvVar{
			Name:  "LOAD",
			Value: modules,
		},
		corev1.EnvVar{
			Name:  "WORKERS",
			Value: "1",
		},
		corev1.EnvVar{
			Name:  "MAX_CRON_THREADS",
			Value: "0",
		})
	return envVars
}

func odooJobWorkerDeploymentEnv(odoo *appsv1alpha1.Odoo) []corev1.EnvVar {
	envVars, _, modules := odooCommonDeploymentEnv(odoo)
	envVars = append(envVars,
		corev1.EnvVar{
			Name:  "LOAD",
			Value: modules,
		},
		corev1.EnvVar{
			Name:  "WORKERS",
			Value: fmt.Sprintf("%d", odoo.Spec.Workers),
		},
		corev1.EnvVar{
			Name:  "MAX_CRON_THREADS",
			Value: "0",
		})
	return envVars
}

func odooMainDeploymentEnv(odoo *appsv1alpha1.Odoo) []corev1.EnvVar {
	envVars, version, modules := odooCommonDeploymentEnv(odoo)
	if odoo.Spec.Options.QueueJobs.Enabled && *odoo.Spec.Options.QueueJobs.Replicas == 0 {
		envVars, modules = appendQueueJobVars(odoo, envVars, version, modules)
	}
	envVars = append(envVars,
		corev1.EnvVar{
			Name:  "LOAD",
			Value: modules,
		},
		corev1.EnvVar{
			Name:  "WORKERS",
			Value: fmt.Sprintf("%d", odoo.Spec.Workers),
		})
	return envVars
}

func odooCommonDeploymentEnv(odoo *appsv1alpha1.Odoo) ([]corev1.EnvVar, string, string) {
	values := getOdooValues(odoo)
	envVars, version, modules := odooBaseEnv(odoo)
	if odoo.Spec.Redis.Enabled {
		modules += ",redis_session_store"
		envVars = append(envVars,
			corev1.EnvVar{
				Name:  "ENABLE_REDIS",
				Value: "true",
			},
			corev1.EnvVar{
				Name:  "REDIS_HOST",
				Value: values.redisHost,
			},
			corev1.EnvVar{
				Name: "REDIS_PASS",
				ValueFrom: &corev1.EnvVarSource{
					SecretKeyRef: &corev1.SecretKeySelector{
						LocalObjectReference: corev1.LocalObjectReference{
							Name: values.redisName,
						},
						Key: "redis-password",
					},
				},
			},
		)
	}
	return envVars, version, modules
}

func odooBaseEnv(odoo *appsv1alpha1.Odoo) ([]corev1.EnvVar, string, string) {
	values := getOdooValues(odoo)
	var pgVars []corev1.EnvVar
	switch odoo.Spec.Postgresql.Secret {
	case "":
		pgVars = []corev1.EnvVar{
			{Name: "DB_HOST", Value: values.postgresName},
			{Name: "DB_PORT", Value: "5432"},
			{Name: "DB_USER", ValueFrom: &corev1.EnvVarSource{
				SecretKeyRef: &corev1.SecretKeySelector{
					LocalObjectReference: corev1.LocalObjectReference{
						Name: values.postgresSecret,
					},
					Key: "username",
				}},
			},
			{Name: "DB_PASSWORD", ValueFrom: &corev1.EnvVarSource{
				SecretKeyRef: &corev1.SecretKeySelector{
					LocalObjectReference: corev1.LocalObjectReference{
						Name: values.postgresSecret,
					},
					Key: "password",
				}},
			},
		}
	default:
		pgVars = []corev1.EnvVar{
			{Name: "DB_HOST", ValueFrom: &corev1.EnvVarSource{
				SecretKeyRef: &corev1.SecretKeySelector{
					LocalObjectReference: corev1.LocalObjectReference{
						Name: odoo.Spec.Postgresql.Secret,
					},
					Key: "DB_HOST",
				}},
			},
			{Name: "DB_PORT", ValueFrom: &corev1.EnvVarSource{
				SecretKeyRef: &corev1.SecretKeySelector{
					LocalObjectReference: corev1.LocalObjectReference{
						Name: odoo.Spec.Postgresql.Secret,
					},
					Key: "DB_PORT",
				}},
			},
			{Name: "DB_USER", ValueFrom: &corev1.EnvVarSource{
				SecretKeyRef: &corev1.SecretKeySelector{
					LocalObjectReference: corev1.LocalObjectReference{
						Name: odoo.Spec.Postgresql.Secret,
					},
					Key: "DB_USER",
				}},
			},
			{Name: "DB_PASSWORD", ValueFrom: &corev1.EnvVarSource{
				SecretKeyRef: &corev1.SecretKeySelector{
					LocalObjectReference: corev1.LocalObjectReference{
						Name: odoo.Spec.Postgresql.Secret,
					},
					Key: "DB_PASSWORD",
				}},
			},
		}
	}
	envVars := append(pgVars, []corev1.EnvVar{
		{Name: "ADMIN_PASSWD", ValueFrom: &corev1.EnvVarSource{
			SecretKeyRef: &corev1.SecretKeySelector{
				LocalObjectReference: corev1.LocalObjectReference{
					Name: values.name,
				},
				Key: "odoo-admin-password",
			}},
		},
		{Name: "DATABASE", Value: odoo.Name},
		{Name: "SSH_PRIVATE_KEY", ValueFrom: &corev1.EnvVarSource{
			SecretKeyRef: &corev1.SecretKeySelector{
				LocalObjectReference: corev1.LocalObjectReference{
					Name: odoo.Spec.Sources.PullSecret,
				},
				Key: "deploy_key_NDP",
			}},
		},
		{Name: "SERVEUR_SSH", Value: odoo.Spec.Sources.Server},
		{Name: "WITHOUT_DEMO", Value: odoo.Spec.Options.WithoutDemo},
		{Name: "CMD_LINE_PARAMS", Value: odoo.Spec.Options.CommandLineParams},
		{Name: "DB_FILTER", Value: odoo.Spec.Options.DBFilter},
		{Name: "LOG_LEVEL", Value: odoo.Spec.Options.LogLevel},
	}...)
	if odoo.Spec.Options.LimitTimeCPU != 0 {
		envVars = append(envVars, corev1.EnvVar{Name: "LIMIT_TIME_CPU", Value: fmt.Sprintf("%d", odoo.Spec.Options.LimitTimeCPU)})
	}
	if odoo.Spec.Options.LimitTimeReal != 0 {
		envVars = append(envVars, corev1.EnvVar{Name: "LIMIT_TIME_REAL", Value: fmt.Sprintf("%d", odoo.Spec.Options.LimitTimeReal)})
	}
	if odoo.Spec.Options.LimitMemorySoft != 0 {
		envVars = append(envVars, corev1.EnvVar{Name: "LIMIT_MEMORY_SOFT", Value: fmt.Sprintf("%d", odoo.Spec.Options.LimitMemorySoft)})
	}
	if odoo.Spec.Options.LimitMemoryHard != 0 {
		envVars = append(envVars, corev1.EnvVar{Name: "LIMIT_MEMORY_HARD", Value: fmt.Sprintf("%d", odoo.Spec.Options.LimitMemoryHard)})
	}
	modules := "web"
	version := strings.Split(odoo.Spec.Version, "-")[0]
	if version == "7.0" || version == "8.0" || version == "9.0" || version == "10.0" {
		modules = "web,web_kanban"
	}
	if odoo.Spec.Options.Load != "" {
		modules += "," + odoo.Spec.Options.Load
	}
	if odoo.Spec.Options.Unaccent != nil && *odoo.Spec.Options.Unaccent {
		envVars = append(envVars, corev1.EnvVar{Name: "UNACCENT", Value: "1"})
	}
	if odoo.Spec.Persistence.S3.Enabled {
		modules += ",odoo_s3"
		envVars = append(envVars,
			corev1.EnvVar{
				Name: "ODOO_S3_HOST",
				ValueFrom: &corev1.EnvVarSource{
					SecretKeyRef: &corev1.SecretKeySelector{
						LocalObjectReference: corev1.LocalObjectReference{
							Name: odoo.Spec.Persistence.S3.Secret,
						},
						Key: "S3_HOST",
					},
				},
			},
			corev1.EnvVar{
				Name: "ODOO_S3_REGION",
				ValueFrom: &corev1.EnvVarSource{
					SecretKeyRef: &corev1.SecretKeySelector{
						LocalObjectReference: corev1.LocalObjectReference{
							Name: odoo.Spec.Persistence.S3.Secret,
						},
						Key: "S3_REGION",
					},
				},
			},
			corev1.EnvVar{
				Name: "ODOO_S3_ACCESS_KEY",
				ValueFrom: &corev1.EnvVarSource{
					SecretKeyRef: &corev1.SecretKeySelector{
						LocalObjectReference: corev1.LocalObjectReference{
							Name: odoo.Spec.Persistence.S3.Secret,
						},
						Key: "S3_ACCESS_KEY",
					},
				},
			},
			corev1.EnvVar{
				Name: "ODOO_S3_SECRET_KEY",
				ValueFrom: &corev1.EnvVarSource{
					SecretKeyRef: &corev1.SecretKeySelector{
						LocalObjectReference: corev1.LocalObjectReference{
							Name: odoo.Spec.Persistence.S3.Secret,
						},
						Key: "S3_SECRET_KEY",
					},
				},
			},
			corev1.EnvVar{
				Name:  "ODOO_S3_BUCKET",
				Value: odoo.Spec.Persistence.S3.Bucket,
			},
		)
		if *odoo.Spec.Persistence.S3.AllowWrite {
			envVars = append(envVars, corev1.EnvVar{
				Name:  "ODOO_S3_READWRITE",
				Value: "1",
			})
		}
	}
	repoNames := make([]string, len(odoo.Spec.Sources.Repositories))
	var i int
	for repoName := range odoo.Spec.Sources.Repositories {
		repoNames[i] = repoName
		i++
	}
	sort.Strings(repoNames)
	for _, repoName := range repoNames {
		repo := odoo.Spec.Sources.Repositories[repoName]
		if repo.Server != "" || repo.Secret != "" {
			repo.Method = "https"
		}
		branch := coalesce(repo.Branch, odoo.Spec.Sources.Branch, odoo.Spec.Version)
		server := coalesce(repo.Server, odoo.Spec.Sources.Server)
		switch repo.Method {
		case "https":
			switch repo.Secret {
			case "":
				envVars = append(envVars, corev1.EnvVar{
					Name:  repoName,
					Value: fmt.Sprintf("-b %s --single-branch --depth=1 https://%s/%s.git", branch, server, repo.Path),
				})
			default:
				envVars = append(envVars,
					corev1.EnvVar{
						Name: fmt.Sprintf("HTTPS_USER_%s", strings.ToUpper(repoName)),
						ValueFrom: &corev1.EnvVarSource{
							SecretKeyRef: &corev1.SecretKeySelector{
								LocalObjectReference: corev1.LocalObjectReference{
									Name: repo.Secret,
								},
								Key: "username",
							},
						},
					},
					corev1.EnvVar{
						Name: fmt.Sprintf("HTTPS_PASSWORD_%s", strings.ToUpper(repoName)),
						ValueFrom: &corev1.EnvVarSource{
							SecretKeyRef: &corev1.SecretKeySelector{
								LocalObjectReference: corev1.LocalObjectReference{
									Name: repo.Secret,
								},
								Key: "password",
							},
						},
					},
					corev1.EnvVar{
						Name: repoName,
						Value: fmt.Sprintf("-b %s --single-branch --depth=1 https://$(HTTPS_USER_%s):$(HTTPS_PASSWORD_%s)@%s/%s.git",
							branch, strings.ToUpper(repoName), strings.ToUpper(repoName), server, repo.Path),
					},
				)
			}
		case "ssh":
			envVars = append(envVars, corev1.EnvVar{
				Name:  repoName,
				Value: fmt.Sprintf("-b %s --single-branch --depth=1 git@%s:%s.git", branch, server, repo.Path),
			})
		}
	}
	return envVars, version, modules
}

func odooFilestoreVolume(odoo *appsv1alpha1.Odoo, name string) corev1.Volume {
	filestoreVolume := corev1.Volume{Name: "odoo-filestore"}
	switch odoo.Spec.Persistence.Disabled {
	case true:
		filestoreVolume.EmptyDir = &corev1.EmptyDirVolumeSource{}
	case false:
		filestoreVolume.PersistentVolumeClaim = &corev1.PersistentVolumeClaimVolumeSource{
			ClaimName: name,
		}
	}
	return filestoreVolume
}
