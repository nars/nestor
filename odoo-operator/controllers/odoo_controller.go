/*


Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package controllers

import (
	"context"
	"fmt"

	"github.com/banzaicloud/operator-tools/pkg/reconciler"
	"github.com/go-logr/logr"
	postgresv1 "github.com/zalando/postgres-operator/pkg/apis/acid.zalan.do/v1"
	appsv1 "k8s.io/api/apps/v1"
	batchv1 "k8s.io/api/batch/v1"
	corev1 "k8s.io/api/core/v1"
	networkingv1 "k8s.io/api/networking/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	"k8s.io/apimachinery/pkg/labels"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	ctrl "sigs.k8s.io/controller-runtime"
	"sigs.k8s.io/controller-runtime/pkg/client"

	appsv1alpha1 "gitlab.com/nars/odoo-operator/api/apps/v1alpha1"
)

// OdooReconciler reconciles a Odoo object
type OdooReconciler struct {
	client.Client
	Log    logr.Logger
	Scheme *runtime.Scheme
}

// +kubebuilder:rbac:groups=apps.ndp-systemes.fr,resources=odooes,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=apps.ndp-systemes.fr,resources=odooes/status,verbs=get;update;patch
// +kubebuilder:rbac:groups=apps.ndp-systemes.fr,resources=odoooperatorconfigurations,verbs=get;list;watch
// +kubebuilder:rbac:groups=acid.zalan.do,resources=postgresqls,verbs=get;list;watch;create;update;patch
// +kubebuilder:rbac:groups=apps,resources=deployments,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=apps,resources=statefulsets,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=networking.k8s.io,resources=ingresses,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=batch,resources=jobs,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=batch,resources=cronjobs,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=core,resources=services,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=core,resources=secrets,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=core,resources=configmaps,verbs=get;list;watch;create;update;patch;delete
// +kubebuilder:rbac:groups=core,resources=persistentvolumeclaims,verbs=get;list;watch;create;update;patch
// +kubebuilder:rbac:groups=core,resources=pods,verbs=get;list;

func (r *OdooReconciler) Reconcile(req ctrl.Request) (ctrl.Result, error) {

	ctx := context.Background()
	log := r.Log.WithValues("odoo", req.NamespacedName)
	odoo := new(appsv1alpha1.Odoo)
	err := r.Get(ctx, req.NamespacedName, odoo)
	if err != nil {
		if errors.IsNotFound(err) {
			// Request object not found, could have been deleted after reconcile request.
			// Owned objects are automatically garbage collected. For additional cleanup logic use finalizers.
			// Return and don't requeue
			log.Info("Odoo resource not found. Ignoring since object must be deleted")
			return ctrl.Result{}, nil
		}
		// Error reading the object - requeue the request.
		log.Error(err, "Failed to get Odoo")
		return ctrl.Result{}, err
	}
	odoo.SetDefaults(r)
	rec := reconciler.NewReconcilerWith(r.Client, reconciler.WithLog(r.Log))
	for _, factory := range []func(*appsv1alpha1.Odoo) (runtime.Object, reconciler.DesiredState, error){
		r.odooSecret,
		r.odooPostgresql,
		r.odooRedisSecret,
		r.odooRedisHealthConfigMap,
		r.odooRedisConfigConfigMap,
		r.odooRedisStatefulSet,
		r.odooRedisService,
		r.odooRedisServiceHeadless,
		r.odooPVC,
		r.odooMainDeployment,
		r.odooJobRunnerDeployment,
		r.odooJobWorkerDeployment,
		r.odooService,
		r.odooJobWorkerService,
		r.odooIngress,
		r.odooBackupCronJob,
	} {
		o, state, err := factory(odoo)
		if err != nil {
			return ctrl.Result{}, fmt.Errorf("failed to create desired object: %s", err)
		}
		if o == nil {
			return ctrl.Result{}, fmt.Errorf("reconcile error! Resource %T returns with nil object", factory)
		}
		result, err := rec.ReconcileResource(o, state)
		if err != nil {
			return ctrl.Result{}, fmt.Errorf("failed to reconcile resource %s: %s", o.GetObjectKind().GroupVersionKind(), err)
		}
		if result != nil {
			return *result, nil
		}
	}
	if err, requeue := r.reconcileOperations(ctx, rec, odoo); err != nil || requeue {
		return ctrl.Result{Requeue: requeue}, err
	}
	if err := r.updateStatus(ctx, odoo); err != nil {
		return ctrl.Result{}, err
	}
	return ctrl.Result{}, nil
}

func (r *OdooReconciler) updateStatus(ctx context.Context, odoo *appsv1alpha1.Odoo) error {
	values := getOdooValues(odoo)
	odooNS := types.NamespacedName{Name: values.name, Namespace: odoo.Namespace}
	dep := appsv1.Deployment{}
	err := r.Get(ctx, odooNS, &dep)
	if err != nil {
		return err
	}
	ingress := networkingv1.Ingress{}
	err = r.Get(ctx, odooNS, &ingress)
	if err != nil {
		return err
	}
	odoo.Status.Replicas = dep.Status.Replicas
	odoo.Status.Selector = labels.SelectorFromSet(values.labels).String()
	odoo.Status.URL = ingress.Spec.Rules[0].Host
	err = r.Status().Update(ctx, odoo)
	if err != nil {
		return err
	}
	return nil
}

func (r *OdooReconciler) SetupWithManager(mgr ctrl.Manager) error {
	return ctrl.NewControllerManagedBy(mgr).
		For(&appsv1alpha1.Odoo{}).
		Owns(&appsv1.Deployment{}).
		Owns(&appsv1.StatefulSet{}).
		Owns(&batchv1.Job{}).
		Owns(&corev1.Secret{}).
		Owns(&postgresv1.Postgresql{}).
		Complete(r)
}

type odooValues struct {
	name                 string
	postgresName         string
	postgresCluster      string
	postgresSecret       string
	labels               map[string]string
	redisHost            string
	redisName            string
	redisHealthCM        string
	redisServiceHeadless string
	jobInstall           string
	jobUpdate            string
	jobRunner            string
	jobWorker            string
	tlsSecret            string
	backupName           string
}

func getOdooValues(odoo *appsv1alpha1.Odoo) *odooValues {
	return &odooValues{
		name:                 fmt.Sprintf("%s-odoo", odoo.Name),
		postgresName:         fmt.Sprintf("ndp-%s-postgresql", odoo.Name),
		postgresCluster:      fmt.Sprintf("%s-postgresql", odoo.Name),
		postgresSecret:       fmt.Sprintf("odoo.ndp-%s-postgresql.credentials.postgresql.acid.zalan.do", odoo.Name),
		labels:               map[string]string{"operator": "odoo", "app": "odoo", "release": odoo.Name},
		redisHost:            fmt.Sprintf("%s-redis-master", odoo.Name),
		redisServiceHeadless: fmt.Sprintf("%s-redis-headless", odoo.Name),
		redisName:            fmt.Sprintf("%s-redis", odoo.Name),
		redisHealthCM:        fmt.Sprintf("%s-redis-health", odoo.Name),
		jobInstall:           fmt.Sprintf("%s-job-install", odoo.Name),
		jobUpdate:            fmt.Sprintf("%s-job-update", odoo.Name),
		jobRunner:            fmt.Sprintf("%s-odoo-job-runner", odoo.Name),
		jobWorker:            fmt.Sprintf("%s-odoo-job-worker", odoo.Name),
		tlsSecret:            fmt.Sprintf("%s-tls-secret", odoo.Name),
		backupName:           fmt.Sprintf("%s-backup", odoo.Name),
	}
}

// coalesce returns the first non empty string
func coalesce(val string, vals ...string) string {
	if val != "" {
		return val
	}
	for _, v := range vals {
		if v != "" {
			return v
		}
	}
	return ""
}
