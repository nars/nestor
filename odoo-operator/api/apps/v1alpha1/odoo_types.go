/*


   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.

*/

// +kubebuilder:validation:Optional

package v1alpha1

import (
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/resource"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// OdooSpec defines the desired state of Odoo
type OdooSpec struct {
	// Options defines the desired configuration of Odoo
	Options OdooOptions `json:"options"`
	// Sources defines the Odoo modules source paths
	Sources OdooSources `json:"sources"`
	// DockerImage is the name of the Docker image to use (without tag)
	DockerImage string `json:"dockerImage"`
	// Version is the Odoo version to setup. It must be a valid Odoo Docker image tag.
	// +kubebuilder:validation:Required
	Version string `json:"version"`
	// Replicas defines the number of Odoo pods to setup
	Replicas *int32 `json:"replicas"`
	// Disabled defines if the Odoo server should be stopped.
	// When true, the Odoo deployment is scaled to 0.
	Disabled bool `json:"disabled"`
	// Workers is the number of Odoo worker process in each pod
	// +kubebuilder:validation:Minimum=2
	Workers int32 `json:"workers"`
	// Persistence defines persistence options for Odoo filestore
	Persistence OdooPersistence `json:"persistence"`
	// Redis defines options for session management with redis
	Redis OdooRedis `json:"redis"`
	// Resources defines resource requests and limits for Odoo
	Resources OdooResources `json:"resources"`
	// Postgresql defines options for the Postgresql database
	Postgresql OdooPostgresql `json:"postgresql"`
	// Ingress defines options for the Odoo ingress
	Ingress OdooIngress `json:"ingress"`
	// Operations defines install and update operations to run.
	// The fields are automatically emptied when the operation is done.
	Operations OdooOperations `json:"operations"`
	// NodeSelectorTerms defines the node selector terms for node affinity
	NodeSelectorTerms []corev1.NodeSelectorTerm `json:"nodeSelectorTerms"`
	// Backup defines the backup options for the instance
	Backup OdooBackup `json:"backup"`
}

// OdooOptions defines the desired configuration of Odoo
type OdooOptions struct {
	// AdminPassword is the Odoo admin password. Leave empty to have a generated password.
	AdminPassword string `json:"adminPassword"`
	// LimitTimeCPU prevents the worker from using more than <limit> CPU seconds
	// for each request. If the limit is exceeded, the worker is killed.
	LimitTimeCPU int32 `json:"limitTimeCpu"`
	// LimitTimeReal prevents the worker from taking longer than <limit> seconds
	// to process a request. If the limit is exceeded, the worker is killed.
	// It differs from --limit-time-cpu in that this is a "wall time" limit including e.g. SQL queries.
	LimitTimeReal int32 `json:"limitTimeReal"`
	// LimitMemorySoft is the maximum allowed virtual memory per worker. If the limit is exceeded,
	// the worker is killed and recycled at the end of the current request.
	LimitMemorySoft int64 `json:"limitMemorySoft"`
	// LimitMemoryHard is the hard limit on virtual memory, any worker exceeding the limit
	// will be immediately killed without waiting for the end of the current request processing.
	LimitMemoryHard int64 `json:"limitMemoryHard"`
	// WithoutDemo specifies the modules for which not to load demo data.
	WithoutDemo string `json:"withoutDemo"`
	// DBFilter hides databases that do not match <filter>
	DBFilter string `json:"dbFilter"`
	// Unaccent enables unaccented searches
	Unaccent *bool `json:"unaccent"`
	// LogLevel defines the log level of Odoo
	LogLevel string `json:"logLevel"`
	// Load is a comma separated list of modules to load at server startup (server-wide modules).
	// You do not need to specify 'web', 'web_kanban' or modules started with queue jobs.
	Load string `json:"load"`
	// QueueJobs defines options for setting up queue jobs
	QueueJobs OdooQueueJobs `json:"queueJobs"`
	// CommandLineParams will be appended to the Odoo script as command line parameters
	CommandLineParams string `json:"commandLineParams"`
}

// OdooQueueJobs defines options for setting up queue jobs
type OdooQueueJobs struct {
	// Enabled is set to true to enable queue jobs
	Enabled bool `json:"enabled"`
	// Channels is a comma separated list of channels with their capacity (eg: root:1,root.sale:1)
	Channels string `json:"channels"`
	// Replicas is the number of replicas for queue job pods.
	// If null or not specified, it defaults to zero if the main replicas is 0 or 1 and 1 otherwise.
	Replicas *int32 `json:"replicas"`
}

// OdooSources defines Odoo modules source paths
type OdooSources struct {
	// Server is the git server to fetch sources from, if not overridden in the repositories.
	Server string `json:"server"`
	// Branch is the branch to fetch sources from, if not overridden in the repositories.
	// It defaults to the Odoo version.
	Branch       string                    `json:"branch"`
	PullSecret   string                    `json:"pullSecret"`
	Repositories map[string]OdooRepository `json:"repositories"`
}

// OdooRepository defines a git repository with Odoo modules
type OdooRepository struct {
	// +kubebuilder:validation:required
	Path string `json:"path"`
	// +kubebuilder:validation:Enum="ssh";"https"
	Method string `json:"method"`
	Branch string `json:"branch"`
	Server string `json:"server"`
	Secret string `json:"secret"`
}

// OdooS3Targets defines S3 targets for database and filestore
type OdooS3Targets struct {
	Database  OdooS3Target `json:"database"`
	Filestore OdooS3Target `json:"filestore"`
}

// OdooS3Target defines an S3 bucket on a s3 service
type OdooS3Target struct {
	Secret string `json:"secret"`
	Bucket string `json:"bucket"`
}

// OdooS3 defines the options for the S3 persistence
type OdooS3 struct {
	Enabled    bool   `json:"enabled"`
	Secret     string `json:"secret"`
	Bucket     string `json:"bucket"`
	AllowWrite *bool  `json:"allowWrite"`
}

// OdooPersistence defines the options for a persistent disk
type OdooPersistence struct {
	// Set Disabled to true to disable persistence for Odoo filestore.
	Disabled bool `json:"disabled"`
	// Size of the persistence disk for Odoo filestore. Defaulte to 8Gi.
	Size resource.Quantity `json:"size"`
	// StorageClassName defines the name of the storage class to use.
	// Set to "-" to disable dynamic provisioning.
	// Leave empty to use the cluster default storage class.
	StorageClassName string `json:"storageClassName"`
	// S3 defines the options for the S3 filestore storage
	S3 OdooS3 `json:"s3"`
}

// OdooRedis defines the options for a redis cache
type OdooRedis struct {
	Enabled bool `json:"enabled"`
}

// OdooPostgresql defines Postgresql database options
type OdooPostgresql struct {
	// Replicas for postgresql. Default is 2.
	Replicas             *int32               `json:"replicas"`
	Parameters           map[string]string    `json:"parameters"`
	Resources            OdooResources        `json:"resources"`
	Volume               OdooPostgresqlVolume `json:"volume"`
	EnableLogicalBackups bool                 `json:"enableLogicalBackups"`
	// Secret, if specified, gives the name of a secret with PG host and credentials.
	// Leave empty for the operator to setup a PG instance automatically
	Secret string `json:"secret"`
}

// OdooPostgresqlVolume defines volume options for postgresql database
type OdooPostgresqlVolume struct {
	Size resource.Quantity `json:"size"`
}

// OdooResources describes requests and limits for the cluster resources.
type OdooResources struct {
	ResourceRequests OdooResourceDescription `json:"requests,omitempty"`
	ResourceLimits   OdooResourceDescription `json:"limits,omitempty"`
}

// OdooResourceDescription describes CPU and memory resources defined for a cluster.
type OdooResourceDescription struct {
	CPU    resource.Quantity `json:"cpu"`
	Memory resource.Quantity `json:"memory"`
}

// OdooIngress describes an ingress for Odoo
type OdooIngress struct {
	// Enabled defines whether to create an ingress or not for this service
	Enabled *bool `json:"enabled"`
	// Host defines the hostname for Odoo
	Host string `json:"host"`
	// DefaultDomain defines a default domain that is used to compute
	// a hostname when Host is not defined.
	DefaultDomain string `json:"defaultDomain"`
}

// OdooOperations defines module install and update operations to run.
// The fields are emptied when the operation is over.
type OdooOperations struct {
	Install string `json:"install"`
	Update  string `json:"update"`
}

// OdooScript defines a script on a git repository
type OdooScript struct {
	Repository string `json:"repository"`
	Branch     string `json:"branch"`
	Script     string `json:"script"`
}

// OdooBackup defines a backup strategy for an odoo instance
type OdooBackup struct {
	Enabled *bool `json:"enabled"`
	// Script used for backup
	Script *OdooScript `json:"script"`
	// Schedule of the backup as a cron string
	Schedule string `json:"schedule"`
	// Image is the name of the docker image to use with Script
	Image string `json:"image"`
	// S3Targets for the backup
	S3Targets []OdooS3Targets `json:"s3Targets"`
	// ServiceAccountName to execute the backup job
	ServiceAccountName string `json:"serviceAccountName"`
}

// OdooStatus defines the observed state of Odoo
type OdooStatus struct {
	Replicas            int32  `json:"replicas"`
	Selector            string `json:"selector"`
	RunningOperationJob string `json:"runningOperationJob"`
	URL                 string `json:"url"`
}

// +kubebuilder:object:root=true
// +kubebuilder:subresource:status
// +kubebuilder:subresource:scale:specpath=.spec.replicas,statuspath=.status.replicas,selectorpath=.status.selector

// Odoo is the Schema for the odooes API
type Odoo struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Spec   OdooSpec   `json:"spec,omitempty"`
	Status OdooStatus `json:"status,omitempty"`
}

// OdooList contains a list of Odoo
// +kubebuilder:object:root=true
type OdooList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []Odoo `json:"items"`
}

func init() {
	SchemeBuilder.Register(&Odoo{}, &OdooList{})
}
