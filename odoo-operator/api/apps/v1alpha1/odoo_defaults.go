// Copyright 2020 NDP Systèmes. All Rights Reserved.
// See LICENSE file for full licensing details.

package v1alpha1

import (
	"context"

	"github.com/zalando/postgres-operator/pkg/util"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/types"
	"sigs.k8s.io/controller-runtime/pkg/client"
)

const (
	odooSourcesMethodSSH      = "ssh"
	odooOperatorConfiguration = "odoo-operator-configuration"

	defaultOdooImage                      = "docker.io/ndpsystemes/odoo"
	defaultOdooVersion                    = "12.0"
	defaultOdooReplicas             int32 = 1
	defaultOdooWorkers              int32 = 3
	defaultOdooLimitTimeCPU         int32 = 1800
	defaultOdooLimitTimeReal        int32 = 3600
	defaultOdooWithoutDemo                = "all"
	defaultOdooDBFilter                   = ".*"
	defaultOdooLogLevel                   = "info"
	defaultOdooQueueJobChannels           = "root:1"
	defaultOdooSourcesServer              = "gitlab.ndp-systemes.fr"
	defaultOdooSourcesPullSecret          = "ssh-private-key-deploy"
	defaultPostgresqlReplicas       int32 = 2
	defaultOdooDomain                     = "tests.ndp-systemes.fr"
	defaultOdooUnaccent                   = true
	defaultOdooQueueJobReplicas     int32 = 1
	defaultIngressEnabled                 = true
	defaultBackupSchedule                 = "13 04 * * *"
	defaultBackupImage                    = "registry.ndp-systemes.fr/dockers/nestor-automation"
	defaultBackupServiceAccountName       = "backup-bot"
)

var (
	defaultOdooRepositories = map[string]OdooRepository{
		"DEPOT_GIT":  {},
		"DEPOT_GIT2": {Path: "odoo-addons/common-modules"},
		"DEPOT_GIT3": {Path: "odoo-addons/community-addons"},
	}
)

func (in *Odoo) SetDefaults(r client.Reader) {
	ctx := context.TODO()
	var conf OdooOperatorConfiguration
	_ = r.Get(ctx, types.NamespacedName{Name: odooOperatorConfiguration, Namespace: in.Namespace}, &conf)

	defaults := conf.Configuration.Defaults
	if in.Spec.DockerImage == "" {
		in.Spec.DockerImage = util.Coalesce(defaults.Image, defaultOdooImage)
	}
	if in.Spec.Version == "" {
		in.Spec.Version = util.Coalesce(defaults.Version, defaultOdooVersion)
	}
	if in.Spec.Replicas == nil {
		defaultReplicas := defaultOdooReplicas
		in.Spec.Replicas = util.CoalesceInt32(&defaults.Replicas, &defaultReplicas)
	}
	if in.Spec.Workers == 0 {
		in.Spec.Workers = coalesceInt32(defaults.Workers, defaultOdooWorkers)
	}
	if in.Spec.Options.LimitTimeCPU == 0 {
		in.Spec.Options.LimitTimeCPU = coalesceInt32(defaults.LimitTimeCPU, defaultOdooLimitTimeCPU)
	}
	if in.Spec.Options.LimitTimeReal == 0 {
		in.Spec.Options.LimitTimeReal = coalesceInt32(defaults.LimitTimeReal, defaultOdooLimitTimeReal)
	}
	if in.Spec.Options.WithoutDemo == "" {
		in.Spec.Options.WithoutDemo = util.Coalesce(defaults.WithoutDemo, defaultOdooWithoutDemo)
	}
	if in.Spec.Options.DBFilter == "" {
		in.Spec.Options.DBFilter = util.Coalesce(defaults.DBFilter, defaultOdooDBFilter)
	}
	if in.Spec.Options.LogLevel == "" {
		in.Spec.Options.LogLevel = util.Coalesce(defaults.LogLevel, defaultOdooLogLevel)
	}
	if in.Spec.Options.Unaccent == nil {
		unaccent := defaultOdooUnaccent
		in.Spec.Options.Unaccent = &unaccent
	}
	if in.Spec.Options.QueueJobs.Channels == "" {
		in.Spec.Options.QueueJobs.Channels = util.Coalesce(defaults.QueueJobChannels, defaultOdooQueueJobChannels)
	}
	if in.Spec.Options.QueueJobs.Replicas == nil {
		zero := int32(0)
		in.Spec.Options.QueueJobs.Replicas = &zero
		if in.Spec.Options.QueueJobs.Enabled && *in.Spec.Replicas > 1 && in.Spec.Redis.Enabled {
			defaultReplicas := defaultOdooQueueJobReplicas
			in.Spec.Options.QueueJobs.Replicas = util.CoalesceInt32(defaults.QueueJobReplicas, &defaultReplicas)
		}
	}
	if in.Spec.Sources.Server == "" {
		in.Spec.Sources.Server = util.Coalesce(defaults.SourcesServer, defaultOdooSourcesServer)
	}
	if in.Spec.Sources.PullSecret == "" {
		in.Spec.Sources.PullSecret = util.Coalesce(defaults.SourcesPullRequest, defaultOdooSourcesPullSecret)
	}
	if in.Spec.Sources.Repositories == nil {
		in.Spec.Sources.Repositories = make(map[string]OdooRepository)
	}
	defOdooRepositories := defaults.SourceRepositories
	if defOdooRepositories == nil {
		defOdooRepositories = defaultOdooRepositories
	}
	for repoName, repo := range defOdooRepositories {
		if _, ok := in.Spec.Sources.Repositories[repoName]; !ok {
			in.Spec.Sources.Repositories[repoName] = repo
		}
	}
	for repoName, repo := range in.Spec.Sources.Repositories {
		if repo.Method == "" {
			repo.Method = odooSourcesMethodSSH
			in.Spec.Sources.Repositories[repoName] = repo
		}
	}
	if in.Spec.Postgresql.Parameters == nil {
		in.Spec.Postgresql.Parameters = make(map[string]string)
	}
	if in.Spec.Postgresql.Replicas == nil {
		replicas := coalesceInt32(defaults.PostgresqlReplicas, defaultPostgresqlReplicas)
		in.Spec.Postgresql.Replicas = &replicas
	}
	if !in.Spec.Postgresql.EnableLogicalBackups && defaults.EnableDBBackups {
		in.Spec.Postgresql.EnableLogicalBackups = true
		in.Spec.Postgresql.Parameters["hot_standby_feedback"] = "on"
	}
	if in.Spec.Ingress.DefaultDomain == "" {
		in.Spec.Ingress.DefaultDomain = util.Coalesce(defaults.DefaultDomain, defaultOdooDomain)
	}
	if in.Spec.Persistence.StorageClassName == "" && defaults.StorageClassName != "" {
		in.Spec.Persistence.StorageClassName = defaults.StorageClassName
	}
	if in.Spec.Persistence.S3.Enabled {
		in.Spec.Persistence.Disabled = true
		if in.Spec.Persistence.S3.Secret == "" && defaults.S3Secret != "" {
			in.Spec.Persistence.S3.Secret = defaults.S3Secret
		}
	}
	switch in.Spec.Persistence.S3.AllowWrite {
	case nil:
		in.Spec.Persistence.S3.AllowWrite = &defaults.S3AllowWrite
	default:
		in.Spec.Persistence.S3.AllowWrite = new(bool)
	}
	if !in.Spec.Redis.Enabled && defaults.RedisEnabled {
		in.Spec.Redis.Enabled = true
	}
	if in.Spec.NodeSelectorTerms == nil {
		switch defaults.NodeSelectorTerms {
		case nil:
			in.Spec.NodeSelectorTerms = []corev1.NodeSelectorTerm{}
		default:
			in.Spec.NodeSelectorTerms = defaults.NodeSelectorTerms
		}
	}
	if in.Spec.Ingress.Enabled == nil {
		enabled := defaultIngressEnabled
		in.Spec.Ingress.Enabled = &enabled
	}
	if in.Spec.Backup.Enabled == nil {
		in.Spec.Backup.Enabled = &defaults.BackupEnable
	}
	if in.Spec.Backup.Script == nil {
		switch defaults.BackupScript {
		case nil:
			in.Spec.Backup.Script = &OdooScript{}
			in.Spec.Backup.Enabled = new(bool)
		default:
			in.Spec.Backup.Script = defaults.BackupScript
		}
	}
	if in.Spec.Backup.Image == "" {
		in.Spec.Backup.Image = util.Coalesce(defaults.BackupImage, defaultBackupImage)
	}
	if in.Spec.Backup.ServiceAccountName == "" {
		in.Spec.Backup.ServiceAccountName = util.Coalesce(defaults.BackupServicaAccountName, defaultBackupServiceAccountName)
	}
	if in.Spec.Backup.Schedule == "" {
		in.Spec.Backup.Schedule = util.Coalesce(defaults.BackupSchedule, defaultBackupSchedule)
	}
	if in.Spec.Backup.S3Targets == nil {
		switch defaults.BackupS3Targets {
		case nil:
			in.Spec.Backup.S3Targets = []OdooS3Targets{}
			in.Spec.Backup.Enabled = new(bool)
		default:
			in.Spec.Backup.S3Targets = defaults.BackupS3Targets
		}
	}
}

// coalesceInt32 works like coalesce but for *int32
func coalesceInt32(val, defaultVal int32) int32 {
	if val == 0 {
		return defaultVal
	}
	return val
}
