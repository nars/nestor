/*


Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package v1alpha1

import (
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// OdooOperatorConfigurationConf defines the desired configuration of OdooOperatorConfiguration
type OdooOperatorConfigurationConf struct {
	// Defaults defines default values for Odoo custom ressources
	Defaults OdooOperatorConfigurationDefaults `json:"defaults"`
}

// OdooOperatorConfigurationDefaults defines defaults for Odoo custom resources
type OdooOperatorConfigurationDefaults struct {
	Image                    string                    `json:"image"`
	Version                  string                    `json:"version"`
	Replicas                 int32                     `json:"replicas"`
	Workers                  int32                     `json:"workers"`
	LimitTimeCPU             int32                     `json:"limitTimeCPU"`
	LimitTimeReal            int32                     `json:"limitTimeReal"`
	WithoutDemo              string                    `json:"withoutDemo"`
	DBFilter                 string                    `json:"dbFilter"`
	LogLevel                 string                    `json:"logLevel"`
	QueueJobChannels         string                    `json:"queueJobChannels"`
	QueueJobReplicas         *int32                    `json:"queueJobReplicas"`
	SourcesServer            string                    `json:"sourcesServer"`
	SourcesPullRequest       string                    `json:"sourcesPullRequest"`
	PostgresqlReplicas       int32                     `json:"postgresqlReplicas"`
	DefaultDomain            string                    `json:"odooDomain"`
	StorageClassName         string                    `json:"storageClassName"`
	RedisEnabled             bool                      `json:"redisEnabled"`
	SourceRepositories       map[string]OdooRepository `json:"sourceRepositories"`
	NodeSelectorTerms        []corev1.NodeSelectorTerm `json:"nodeSelectorTerms"`
	EnableDBBackups          bool                      `json:"enableDBBackups"`
	S3Secret                 string                    `json:"s3Secret"`
	S3AllowWrite             bool                      `json:"s3AllowWrite"`
	BackupEnable             bool                      `json:"backupEnable"`
	BackupScript             *OdooScript               `json:"backupScript"`
	BackupSchedule           string                    `json:"backupSchedule"`
	BackupImage              string                    `json:"backupImage"`
	BackupS3Targets          []OdooS3Targets           `json:"backupS3Targets"`
	BackupServicaAccountName string                    `json:"backupServicaAccountName"`
}

// +kubebuilder:object:root=true

// OdooOperatorConfiguration is the Schema for the odoooperatorconfigurations API
type OdooOperatorConfiguration struct {
	metav1.TypeMeta   `json:",inline"`
	metav1.ObjectMeta `json:"metadata,omitempty"`

	Configuration OdooOperatorConfigurationConf `json:"configuration,omitempty"`
}

// +kubebuilder:object:root=true

// OdooOperatorConfigurationList contains a list of OdooOperatorConfiguration
type OdooOperatorConfigurationList struct {
	metav1.TypeMeta `json:",inline"`
	metav1.ListMeta `json:"metadata,omitempty"`
	Items           []OdooOperatorConfiguration `json:"items"`
}

func init() {
	SchemeBuilder.Register(&OdooOperatorConfiguration{}, &OdooOperatorConfigurationList{})
}
