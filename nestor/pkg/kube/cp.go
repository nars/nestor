// Copyright 2020 NDP Systèmes. All Rights Reserved.
// See LICENSE file for full licensing details.

package kube

import (
	"archive/tar"
	"errors"
	"fmt"
	"io"
	"io/ioutil"
	"os"
	"path"
	"path/filepath"
	"strings"

	v1 "k8s.io/api/core/v1"
	"k8s.io/client-go/rest"
)

var (
	errFileCannotBeEmpty = errors.New("filepath can not be empty")
)

func CopyToPod(config *rest.Config, pod *v1.Pod, src, dst string) error {
	return copyToPod(config, pod, src, dst, true)
}

func CopyDirToPod(config *rest.Config, pod *v1.Pod, src, dst string) error {
	return copyToPod(config, pod, src, dst, false)
}

func copyToPod(config *rest.Config, pod *v1.Pod, src string, dst string, destDirCheck bool) error {
	if src == "" || dst == "" {
		return errFileCannotBeEmpty
	}
	reader, writer := io.Pipe()

	// strip trailing slash (if any)
	if dst != "/" && strings.HasSuffix(string(dst[len(dst)-1]), "/") {
		dst = dst[:len(dst)-1]
	}

	if destDirCheck {
		if err := Exec(config, pod, fmt.Sprintf("test -d %s", dst), false); err == nil {
			// If no error, dst was found to be a directory.
			// Copy specified src into it
			dst = dst + "/" + path.Base(src)
		}
	}

	go func() {
		defer writer.Close()
		err := makeTar(src, dst, writer)
		if err != nil {
			fmt.Println(err)
			os.Exit(1)
		}
	}()

	cmdArr := []string{"tar", "-xf", "-"}

	destDir := path.Dir(dst)
	if len(destDir) > 0 {
		cmdArr = append(cmdArr, "-C", destDir)
	}
	return execPipe(config, pod, strings.Join(cmdArr, " "), reader, os.Stdout, os.Stderr, false)
}

func CopyFromPod(config *rest.Config, pod *v1.Pod, src, dst string) error {
	if src == "" || dst == "" {
		return errFileCannotBeEmpty
	}

	reader, outStream := io.Pipe()
	go func() {
		defer outStream.Close()
		execPipe(config, pod, fmt.Sprintf("tar cf - %s", src), nil, outStream, os.Stderr, false)
	}()

	prefix := getPrefix(src)
	prefix = path.Clean(prefix)
	// remove extraneous path shortcuts - these could occur if a path contained extra "../"
	// and attempted to navigate beyond "/" in a remote filesystem
	prefix = stripPathShortcuts(prefix)
	return untarAll(reader, dst, prefix)
}

// stripPathShortcuts removes any leading or trailing "../" from a given path
func stripPathShortcuts(p string) string {
	newPath := path.Clean(p)
	trimmed := strings.TrimPrefix(newPath, "../")

	for trimmed != newPath {
		newPath = trimmed
		trimmed = strings.TrimPrefix(newPath, "../")
	}

	// trim leftover ".."
	if newPath == ".." {
		newPath = ""
	}

	if len(newPath) > 0 && string(newPath[0]) == "/" {
		return newPath[1:]
	}

	return newPath
}

func makeTar(srcPath, destPath string, writer io.Writer) error {
	tarWriter := tar.NewWriter(writer)
	defer tarWriter.Close()

	srcPath = path.Clean(srcPath)
	destPath = path.Clean(destPath)
	return recursiveTar(path.Dir(srcPath), path.Base(srcPath), path.Dir(destPath), path.Base(destPath), tarWriter)
}

func recursiveTar(srcBase, srcFile, destBase, destFile string, tw *tar.Writer) error {
	fPath := path.Join(srcBase, srcFile)
	stat, err := os.Lstat(fPath)
	if err != nil {
		return err
	}
	if stat.IsDir() {
		files, err := ioutil.ReadDir(fPath)
		if err != nil {
			return err
		}
		if len(files) == 0 {
			// case empty directory
			hdr, _ := tar.FileInfoHeader(stat, fPath)
			hdr.Name = destFile
			if err := tw.WriteHeader(hdr); err != nil {
				return err
			}
		}
		for _, f := range files {
			if err := recursiveTar(srcBase, path.Join(srcFile, f.Name()), destBase, path.Join(destFile, f.Name()), tw); err != nil {
				return err
			}
		}
		return nil
	} else if stat.Mode()&os.ModeSymlink != 0 {
		// case soft link
		hdr, _ := tar.FileInfoHeader(stat, fPath)
		target, err := os.Readlink(fPath)
		if err != nil {
			return err
		}

		hdr.Linkname = target
		hdr.Name = destFile
		if err := tw.WriteHeader(hdr); err != nil {
			return err
		}
	} else {
		// case regular file or other file type like pipe
		hdr, err := tar.FileInfoHeader(stat, fPath)
		if err != nil {
			return err
		}
		hdr.Name = destFile

		if err := tw.WriteHeader(hdr); err != nil {
			return err
		}

		f, err := os.Open(fPath)
		if err != nil {
			return err
		}
		defer f.Close()

		if _, err := io.Copy(tw, f); err != nil {
			return err
		}
		return f.Close()
	}
	return nil
}

// clean prevents path traversals by stripping them out.
// This is adapted from https://golang.org/src/net/http/fs.go#L74
func clean(fileName string) string {
	return path.Clean(string(os.PathSeparator) + fileName)
}

func untarAll(reader io.Reader, destFile, prefix string) error {
	entrySeq := -1
	tarReader := tar.NewReader(reader)
	for {
		header, err := tarReader.Next()
		if err != nil {
			if err != io.EOF {
				return err
			}
			break
		}
		entrySeq++
		mode := header.FileInfo().Mode()
		outFileName := path.Join(destFile, clean(header.Name[len(prefix):]))
		baseName := path.Dir(outFileName)
		if err := os.MkdirAll(baseName, 0755); err != nil {
			return err
		}
		if header.FileInfo().IsDir() {
			if err := os.MkdirAll(outFileName, 0755); err != nil {
				return err
			}
			continue
		}

		// handle coping remote file into local directory
		if entrySeq == 0 && !header.FileInfo().IsDir() {
			exists, err := dirExists(outFileName)
			if err != nil {
				return err
			}
			if exists {
				outFileName = filepath.Join(outFileName, path.Base(clean(header.Name)))
			}
		}

		if mode&os.ModeSymlink != 0 {
			err := os.Symlink(header.Linkname, outFileName)
			if err != nil {
				return err
			}
		} else {
			outFile, err := os.Create(outFileName)
			if err != nil {
				return err
			}
			defer outFile.Close()
			if _, err := io.Copy(outFile, tarReader); err != nil {
				return err
			}
			if err := outFile.Close(); err != nil {
				return err
			}
		}
	}

	if entrySeq == -1 {
		// if no file was copied
		errInfo := fmt.Sprintf("error: %s no such file or directory", prefix)
		return errors.New(errInfo)
	}
	return nil
}

func getPrefix(file string) string {
	// tar strips the leading '/' if it's there, so we will too
	return strings.TrimLeft(file, "/")
}

// dirExists checks if a path exists and is a directory.
func dirExists(path string) (bool, error) {
	fi, err := os.Stat(path)
	if err == nil && fi.IsDir() {
		return true, nil
	}
	if os.IsNotExist(err) {
		return false, nil
	}
	return false, err
}
