// Copyright 2021 NDP Systèmes. All Rights Reserved.
// See LICENSE file for full licensing details.

package kube

import (
	"context"
	"errors"
	"fmt"
	"strings"

	corev1 "k8s.io/api/core/v1"
	rbacv1 "k8s.io/api/rbac/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	clientcmdapi "k8s.io/client-go/tools/clientcmd/api"
)

type UserData struct {
	Name  string
	Token string
}

// ListUsers lists the defined users in this namespace.
func ListUsers(config *Config) ([]string, error) {
	ctx := context.TODO()
	sas, err := config.GetKubeClient().CoreV1().ServiceAccounts(config.Namespace).List(ctx, metav1.ListOptions{})
	if err != nil {
		return nil, err
	}
	res := make([]string, len(sas.Items))
	for i, sa := range sas.Items {
		res[i] = sa.Name
	}
	return res, nil
}

// GetUserKubeConfig returns a kubeconfig for the given username
func GetUserKubeConfig(config *Config, username, clusterName string, production bool) (*clientcmdapi.Config, error) {
	ctx := context.TODO()
	sa, err := config.GetKubeClient().CoreV1().ServiceAccounts(config.Namespace).Get(ctx, username, metav1.GetOptions{})
	if err != nil {
		return nil, err
	}
	res := clientcmdapi.NewConfig()
	res.APIVersion = "v1"
	auth := clientcmdapi.NewAuthInfo()
	cluster := clientcmdapi.NewCluster()
	cluster.Server = config.Host
	kubeContext := clientcmdapi.NewContext()
	kcName := fmt.Sprintf("%s@%s", username, clusterName)
	kubeContext.Cluster = clusterName
	kubeContext.AuthInfo = username
	kubeContext.Namespace = sa.Namespace
	if len(sa.Secrets) > 0 {
		ref := sa.Secrets[0]
		secret, err := config.GetKubeClient().CoreV1().Secrets(config.Namespace).Get(ctx, ref.Name, metav1.GetOptions{})
		if err != nil {
			return nil, err
		}
		auth.Token = string(secret.Data["token"])
		cluster.CertificateAuthorityData = secret.Data["ca.crt"]
		kubeContext.Namespace = string(secret.Data["namespace"])
	}
	setContextWithTypeProduction(kubeContext, production)
	res.AuthInfos[username] = auth
	res.Clusters[clusterName] = cluster
	res.Contexts[kcName] = kubeContext
	res.CurrentContext = kcName
	return res, nil
}

// AddUser adds a user with the given name
func AddUser(config *Config, username string, roleBindings []corev1.ObjectReference) error {
	ctx := context.TODO()
	sa := corev1.ServiceAccount{}
	sa.Name = username
	csa, err := config.GetKubeClient().CoreV1().ServiceAccounts(config.Namespace).Create(ctx, &sa, metav1.CreateOptions{})
	if err != nil {
		return err
	}
	for _, rbRef := range roleBindings {
		switch rbRef.Kind {
		case "ClusterRoleBinding":
			binding, err := config.GetKubeClient().RbacV1().ClusterRoleBindings().Get(ctx, rbRef.Name, metav1.GetOptions{})
			if err != nil {
				return err
			}
			binding.Subjects = append(binding.Subjects, rbacv1.Subject{
				Kind:      "ServiceAccount",
				Name:      csa.Name,
				Namespace: csa.Namespace,
			})
			if _, err = config.GetKubeClient().RbacV1().ClusterRoleBindings().Update(ctx, binding, metav1.UpdateOptions{}); err != nil {
				return err
			}
		case "RoleBinding":
			binding, err := config.GetKubeClient().RbacV1().RoleBindings(config.Namespace).Get(ctx, rbRef.Name, metav1.GetOptions{})
			if err != nil {
				return err
			}
			binding.Subjects = append(binding.Subjects, rbacv1.Subject{
				Kind:      "ServiceAccount",
				Name:      csa.Name,
				Namespace: csa.Namespace,
			})
			if _, err = config.GetKubeClient().RbacV1().RoleBindings(config.Namespace).Update(ctx, binding, metav1.UpdateOptions{}); err != nil {
				return err
			}
		default:
			return fmt.Errorf("unknown kind '%s'", rbRef.Kind)
		}
	}
	return nil
}

// DeleteUser deletes the user with the given name
// The given role bindings will be checked for cleaning
func DeleteUser(config *Config, username string, roleBindings []corev1.ObjectReference) error {
	ctx := context.TODO()
	err := config.GetKubeClient().CoreV1().ServiceAccounts(config.Namespace).Delete(ctx, username, metav1.DeleteOptions{})
	if err != nil {
		return err
	}
	var errs []string
	for _, rbRef := range roleBindings {
		switch rbRef.Kind {
		case "ClusterRoleBinding":
			binding, err := config.GetKubeClient().RbacV1().ClusterRoleBindings().Get(ctx, rbRef.Name, metav1.GetOptions{})
			if err != nil {
				errs = append(errs, err.Error())
				continue
			}
			var newSubjects []rbacv1.Subject
			for _, sub := range binding.Subjects {
				if sub.Name != username || sub.Namespace != config.Namespace {
					newSubjects = append(newSubjects, sub)
				}
			}
			binding.Subjects = newSubjects
			if _, err = config.GetKubeClient().RbacV1().ClusterRoleBindings().Update(ctx, binding, metav1.UpdateOptions{}); err != nil {
				errs = append(errs, err.Error())
			}
		case "RoleBinding":
			binding, err := config.GetKubeClient().RbacV1().RoleBindings(config.Namespace).Get(ctx, rbRef.Name, metav1.GetOptions{})
			if err != nil {
				errs = append(errs, err.Error())
				continue
			}
			var newSubjects []rbacv1.Subject
			for _, sub := range binding.Subjects {
				if sub.Name != username || sub.Namespace != config.Namespace {
					newSubjects = append(newSubjects, sub)
				}
			}
			binding.Subjects = newSubjects
			if _, err = config.GetKubeClient().RbacV1().RoleBindings(config.Namespace).Update(ctx, binding, metav1.UpdateOptions{}); err != nil {
				errs = append(errs, err.Error())
			}
		default:
			return fmt.Errorf("unknown kind '%s'", rbRef.Kind)
		}
	}
	if len(errs) > 0 {
		return errors.New(strings.Join(errs, ", "))
	}
	return nil
}
