// Copyright 2021 NDP Systèmes. All Rights Reserved.
// See LICENSE file for full licensing details.

package kube

import (
	"fmt"
	"strings"

	"k8s.io/apimachinery/pkg/api/meta"
	"k8s.io/client-go/discovery"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	runtimeclient "sigs.k8s.io/controller-runtime/pkg/client"
)

// A Config to create kubernetes clients
type Config struct {
	*rest.Config
	ClusterName string
	Namespace   string
}

func (c *Config) String() string {
	type Config struct {
		Config      string
		ClusterName string
		Namespace   string
	}
	aux := &Config{
		Config:      "CONFIG-CHANGE-ME",
		ClusterName: c.ClusterName,
		Namespace:   c.Namespace,
	}
	return strings.Replace(fmt.Sprintf("%#v", aux), `"CONFIG-CHANGE-ME"`, c.Config.String(), 1)
}

func (c *Config) ToRESTConfig() (*rest.Config, error) {
	return c.Config, nil
}

func (c *Config) ToDiscoveryClient() (discovery.CachedDiscoveryInterface, error) {
	panic("Not implemented")
}

func (c *Config) ToRESTMapper() (meta.RESTMapper, error) {
	panic("Not implemented")
}

// ToRawKubeConfigLoader return kubeconfig loader as-is
func (c *Config) ToRawKubeConfigLoader() clientcmd.ClientConfig {
	panic("Not implemented")
}

// GetRuntimeClient returns a runtime client to access kubernetes objects
func (c *Config) GetRuntimeClient() runtimeclient.Client {
	restCfg, err := c.ToRESTConfig()
	if err != nil {
		panic(err)
	}
	rtClient, err := runtimeclient.New(restCfg, runtimeclient.Options{})
	if err != nil {
		panic(err)
	}
	return rtClient
}

// GetKubeClient returns a kubernetes client to access kubernetes objects
// in a type safe way.
func (c *Config) GetKubeClient() kubernetes.Interface {
	client, err := kubernetes.NewForConfig(c.Config)
	if err != nil {
		panic(err)
	}
	return client
}
