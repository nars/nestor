// Copyright 2020 NDP Systèmes. All Rights Reserved.
// See LICENSE file for full licensing details.

package kube

import (
	"context"
	"errors"
	"fmt"
	"io"
	"net"
	"net/http"
	"os"
	"sync"
	"time"

	"github.com/imdario/mergo"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/serializer"
	"k8s.io/cli-runtime/pkg/genericclioptions"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	"k8s.io/client-go/tools/clientcmd/api"
	"k8s.io/client-go/tools/portforward"
	"k8s.io/client-go/tools/remotecommand"
	"k8s.io/client-go/transport/spdy"
	"k8s.io/kubectl/pkg/cmd/logs"
	"k8s.io/kubectl/pkg/polymorphichelpers"
	"k8s.io/kubectl/pkg/util/term"
)

const (
	productionType   = "production"
	nestorOptionsKey = "nestor"
	contextTypeKey   = "type"
)

// A Context defines a context in the .kube/config file
type Context struct {
	*api.Context
	Name    string
	Current bool
}

// Production returns true if this is a production context
func (c Context) Production() bool {
	return IsProductionContext(c.Context)
}

// IsProductionContext returns true if the given context is a production context
func IsProductionContext(ctx *api.Context) bool {
	nEntry, ok := ctx.Extensions[nestorOptionsKey]
	if !ok {
		return false
	}
	nExt := new(v1.ConfigMap)
	switch nEntryT := nEntry.(type) {
	case *v1.ConfigMap:
		nExt = nEntryT
	case *runtime.Unknown:
		codecs := serializer.NewCodecFactory(runtime.NewScheme())
		deserializer := codecs.UniversalDeserializer()
		obj, _, err := deserializer.Decode(nEntryT.Raw, nil, nExt)
		if err != nil {
			return false
		}
		nExt = obj.(*v1.ConfigMap)
	default:
		return false
	}
	return nExt.Data[contextTypeKey] == productionType
}

// GetFreePort asks the kernel for a free open port that is ready to use.
func GetFreePort() (int, error) {
	addr, err := net.ResolveTCPAddr("tcp", "localhost:0")
	if err != nil {
		return 0, err
	}
	l, err := net.ListenTCP("tcp", addr)
	if err != nil {
		return 0, err
	}
	defer l.Close()
	return l.Addr().(*net.TCPAddr).Port, nil
}

// GetSecretValue returns the value of key in secret
func GetSecretValue(c *Config, secret, key string) (string, error) {
	ctx := context.TODO()
	s, err := c.GetKubeClient().CoreV1().Secrets(c.Namespace).Get(ctx, secret, metav1.GetOptions{})
	if err != nil {
		return "", err
	}
	return string(s.Data[key]), nil
}

// PodReady returns true if the pod is ready, i.e. all its containers are ready
func PodReady(pod *v1.Pod) bool {
	if pod == nil {
		return false
	}
	for _, cs := range pod.Status.ContainerStatuses {
		if !cs.Ready {
			return false
		}
	}
	return true
}

// FirstRunningPod returns the first running pod of the list or nil.
func FirstRunningPod(pl *v1.PodList) *v1.Pod {
	var pod *v1.Pod
	for _, p := range pl.Items {
		if PodReady(&p) {
			pod = &p
			break
		}
	}
	return pod
}

// ContainersInfos returns the number of ready containers, the total number of containers and the number of restarts
func ContainersInfos(pod *v1.Pod) (int, int, int) {
	total := len(pod.Status.ContainerStatuses)
	var ready, restarts int
	for _, c := range pod.Status.ContainerStatuses {
		if c.Ready {
			ready++
		}
		if int(c.RestartCount) > restarts {
			restarts = int(c.RestartCount)
		}
	}
	return ready, total, restarts
}

// Exec executes the given command on the given pod
func Exec(config *rest.Config, pod *v1.Pod, cmd string, tty bool) error {
	return execPipe(config, pod, cmd, os.Stdin, os.Stdout, os.Stderr, tty)
}

// ExecTo executes the given command in the given pod and writes the output to w.
func ExecTo(config *rest.Config, pod *v1.Pod, w io.Writer, cmd string) error {
	return execPipe(config, pod, cmd, os.Stdin, w, os.Stderr, false)
}

func execPipe(config *rest.Config, pod *v1.Pod, cmd string, in io.Reader, out, errOut io.Writer, inTty bool) error {
	if pod == nil {
		return errors.New("trying to exec on a nil pod")
	}
	client, err := kubernetes.NewForConfig(config)
	if err != nil {
		return err
	}
	req := client.CoreV1().RESTClient().Post().
		Namespace(pod.Namespace).
		Resource("pods").
		Name(pod.Name).
		SubResource("exec")
	scheme := runtime.NewScheme()
	if err := v1.AddToScheme(scheme); err != nil {
		return fmt.Errorf("error adding to scheme: %v", err)
	}
	parameterCodec := runtime.NewParameterCodec(scheme)

	tty := term.TTY{
		In:  in,
		Out: out,
		Raw: true,
	}
	var sizeQueue remotecommand.TerminalSizeQueue
	if inTty {
		sizeQueue = tty.MonitorSize(tty.GetSize())
	}
	var hasIn, hasOut, hasErr bool
	if in != nil {
		hasIn = true
	}
	if out != nil {
		hasOut = true
	}
	if errOut != nil {
		hasErr = true
	}
	fn := func() error {
		req.VersionedParams(&v1.PodExecOptions{
			Command: []string{"/bin/sh", "-c", cmd},
			Stdin:   hasIn,
			Stdout:  hasOut,
			Stderr:  hasErr,
			TTY:     inTty,
		}, parameterCodec)
		exec, err := remotecommand.NewSPDYExecutor(config, "POST", req.URL())
		if err != nil {
			return fmt.Errorf("error while creating Executor: %v", err)
		}
		return exec.Stream(remotecommand.StreamOptions{
			Stdin:             in,
			Stdout:            out,
			Stderr:            errOut,
			Tty:               inTty,
			TerminalSizeQueue: sizeQueue,
		})
	}
	if inTty {
		if err := tty.Safe(fn); err != nil {
			return fmt.Errorf("error in exec: %v", err)
		}
		return nil
	}
	err = fn()
	if err != nil {
		return fmt.Errorf("error in exec: %v", err)
	}
	return nil
}

// PortForward forwards the given port configuration 'localPort:remotePort' for the given pod.
func PortForward(config *rest.Config, pod *v1.Pod, port string, stopChan, readyChan chan struct{}) error {
	if pod.Status.Phase != v1.PodRunning {
		return fmt.Errorf("unable to forward port because pod is not running. Current status=%v", pod.Status.Phase)
	}

	client, err := kubernetes.NewForConfig(config)
	if err != nil {
		return err
	}
	req := client.CoreV1().RESTClient().Post().
		Resource("pods").
		Namespace(pod.Namespace).
		Name(pod.Name).
		SubResource("portforward")

	transport, upgrader, err := spdy.RoundTripperFor(config)
	if err != nil {
		return err
	}
	dialer := spdy.NewDialer(upgrader, &http.Client{Transport: transport}, "POST", req.URL())
	fw, err := portforward.NewOnAddresses(dialer, []string{"localhost"}, []string{port}, stopChan, readyChan, os.Stdout, os.Stderr)
	if err != nil {
		return err
	}
	return fw.ForwardPorts()
}

// ListConfigs returns the names of all kubernetes contexts in .kube/config file
func ListConfigs() ([]Context, error) {
	pathOption := clientcmd.NewDefaultPathOptions()
	config, err := pathOption.GetStartingConfig()
	if err != nil {
		return nil, err
	}
	res := make([]Context, len(config.Contexts))
	var i int
	for ctxName, ctx := range config.Contexts {
		res[i] = Context{
			Context: ctx,
			Name:    ctxName,
			Current: ctxName == config.CurrentContext,
		}
		i++
	}
	return res, nil
}

// SelectConfig selects the given kubernetes context as current context
// This command fails if the given context is a production context
func SelectConfig(contextName string) error {
	pathOption := clientcmd.NewDefaultPathOptions()
	config, err := pathOption.GetStartingConfig()
	if err != nil {
		return err
	}
	for ctx := range config.Contexts {
		if ctx == contextName {
			config.CurrentContext = contextName
			if IsProductionContext(config.Contexts[config.CurrentContext]) {
				return errors.New("vous ne pouvez pas sélectionner une configuration de production. Utilisez le flag --kube à la place")
			}
			return clientcmd.ModifyConfig(pathOption, *config, true)
		}
	}
	return fmt.Errorf("contexte kubernetes '%s' inconnu", contextName)
}

// AddConfig creates (or update) a .kube/config file from the given config data.
// data must be a valid kubeconfig yaml.
func AddConfig(data []byte) error {
	config, err := clientcmd.Load(data)
	if err != nil {
		return err
	}
	for ctxName, ctx := range config.DeepCopy().Contexts {
		name := fmt.Sprintf("%s@%s", ctx.AuthInfo, ctx.Cluster)
		config.Contexts[name] = ctx
		if ctxName != name {
			delete(config.Contexts, ctxName)
		}
		if config.CurrentContext == ctxName {
			config.CurrentContext = name
		}
	}
	if IsProductionContext(config.Contexts[config.CurrentContext]) {
		// Do not switch to our new context if it is a production context
		config.CurrentContext = ""
	}
	// Apply new config
	pathOption := clientcmd.NewDefaultPathOptions()
	currentConfig, err := pathOption.GetStartingConfig()
	err = mergo.Merge(currentConfig, config, mergo.WithOverride)
	if err != nil {
		return err
	}
	return clientcmd.ModifyConfig(pathOption, *currentConfig, true)
}

// RemoveConfig removes the given context from the configuration
func RemoveConfig(contextName string) error {
	pathOption := clientcmd.NewDefaultPathOptions()
	config, err := pathOption.GetStartingConfig()
	if err != nil {
		return err
	}
	for ctx := range config.Contexts {
		if ctx == contextName {
			delete(config.Contexts, ctx)
			return clientcmd.ModifyConfig(pathOption, *config, true)
		}
	}
	return fmt.Errorf("configuration inconnue: %s", contextName)
}

// RenameConfig renames the given context with the new name
func RenameConfig(contextName, newName string) error {
	if contextName == newName {
		return nil
	}
	pathOption := clientcmd.NewDefaultPathOptions()
	config, err := pathOption.GetStartingConfig()
	if err != nil {
		return err
	}
	ctx, ok := config.Contexts[contextName]
	if !ok {
		return fmt.Errorf("la configuration '%s' n'existe pas", contextName)
	}
	config.Contexts[newName] = ctx
	delete(config.Contexts, contextName)
	if config.CurrentContext == contextName {
		config.CurrentContext = newName
	}
	return clientcmd.ModifyConfig(pathOption, *config, true)
}

// SetContextTypeProduction sets/unsets the type 'production' on the context with the given name
func SetContextTypeProduction(contextName string, value bool) error {
	pathOption := clientcmd.NewDefaultPathOptions()
	config, err := pathOption.GetStartingConfig()
	if err != nil {
		return err
	}
	ctx, ok := config.Contexts[contextName]
	if !ok {
		return fmt.Errorf("la configuration '%s' n'existe pas", contextName)
	}
	setContextWithTypeProduction(ctx, value)
	return clientcmd.ModifyConfig(pathOption, *config, true)
}

func setContextWithTypeProduction(ctx *api.Context, value bool) {
	switch value {
	case true:
		ctx.Extensions[nestorOptionsKey] = &v1.ConfigMap{
			Data: map[string]string{
				contextTypeKey: productionType,
			},
		}
	case false:
		delete(ctx.Extensions, nestorOptionsKey)
	}
}

// Logs the given pods
func Logs(rcg genericclioptions.RESTClientGetter, pods *v1.PodList, follow bool, tail int64) error {
	options := v1.PodLogOptions{
		Follow: follow,
	}
	if tail > 0 {
		options.TailLines = &tail
	}
	requests, err := polymorphichelpers.LogsForObjectFn(rcg, pods, &options, 10*time.Second, true)
	if err != nil {
		return err
	}

	if follow && len(requests) > 1 {
		reader, writer := io.Pipe()
		wg := &sync.WaitGroup{}
		wg.Add(len(requests))
		for objRef, request := range requests {
			go func(objRef v1.ObjectReference, request rest.ResponseWrapper) {
				defer wg.Done()
				if err := logs.DefaultConsumeRequest(request, writer); err != nil {
					writer.CloseWithError(err)
					// It's important to return here to propagate the error via the pipe
					return
				}
			}(objRef, request)
		}

		go func() {
			wg.Wait()
			writer.Close()
		}()

		_, err := io.Copy(os.Stdout, reader)
		return err
	}
	for _, request := range requests {
		if err := logs.DefaultConsumeRequest(request, os.Stdout); err != nil {
			return err
		}
	}
	return nil
}

// DeletePod deletes the given pod
func DeletePod(client kubernetes.Interface, pod *v1.Pod) error {
	ctx := context.TODO()
	return client.CoreV1().Pods(pod.Namespace).Delete(ctx, pod.Name, metav1.DeleteOptions{})
}
