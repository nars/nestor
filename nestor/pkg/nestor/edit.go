// Copyright 2020 NDP Systèmes. All Rights Reserved.
// See LICENSE file for full licensing details.

package nestor

import (
	"bytes"
	"errors"
	"os"

	"k8s.io/kubectl/pkg/cmd/util/editor"
	"sigs.k8s.io/yaml"
)

var (
	// unchangedValsError is emitted when the values are unchanged in the editor
	unchangedValsError = errors.New("no modification to configuration file")
	// emptyValsError is emitted when there is no text in the editor
	emptyValsError = errors.New("empty editor")
)

// editValues opens the editor for the user to modify given vals and returns the modified vals
func editValues(vals map[string]interface{}) (map[string]interface{}, error) {
	data, err := yaml.Marshal(vals)
	if err != nil {
		return nil, err
	}
	buf := bytes.NewBuffer(data)
	e := editor.NewDefaultEditor([]string{"EDITOR"})
	edited, file, err := e.LaunchTempFile("nestor_edit", ".txt", buf)
	defer os.Remove(file)
	if err != nil {
		return nil, err
	}
	if bytes.Equal(data, edited) {
		return vals, unchangedValsError
	}
	if len(bytes.TrimSpace(edited)) == 0 {
		return nil, emptyValsError
	}

	var res map[string]interface{}
	err = yaml.Unmarshal(edited, &res)
	if err != nil {
		return nil, err
	}
	return res, nil
}
