// Copyright 2020 NDP Systèmes. All Rights Reserved.
// See LICENSE file for full licensing details.

package nestor

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"os"
	"os/exec"
	"strconv"
	"strings"
	"sync"
	"time"

	"gitlab.com/nars/nestor/pkg/clean"
	"gitlab.com/nars/nestor/pkg/kube"
	"gitlab.com/nars/nestor/pkg/s3"
	v1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

// pgPods returns the postgresql pods of this instance
func (i *Instance) pgMasterPod() *v1.Pod {
	pod, _ := i.findPGMasterPod()
	return pod
}

// pgPods returns the postgresql pods of this instance
// Second returned argument is true if postgresql is managed by the postgres-operator (spilo)
func (i *Instance) findPGMasterPod() (*v1.Pod, bool) {
	ctx := context.TODO()
	// Check pod from operator first
	pods, err := i.config.GetKubeClient().CoreV1().Pods(i.Namespace()).List(ctx, metav1.ListOptions{
		LabelSelector: fmt.Sprintf("release=%s,application=spilo,spilo-role=master", i.Name()),
	})
	if err != nil {
		panic(err)
	}
	pod := kube.FirstRunningPod(pods)
	return pod, true
}

// pgPassword retrieves the postgresql password of this instance
func (i *Instance) pgPassword() string {
	var secretName, secretField string
	switch i.CRD.Spec.Postgresql.Secret {
	case "":
		secretName = fmt.Sprintf("odoo.ndp-%s-postgresql.credentials.postgresql.acid.zalan.do", i.Name())
		secretField = "password"
	default:
		secretName = i.CRD.Spec.Postgresql.Secret
		secretField = "DB_PASSWORD"
	}
	pw, err := kube.GetSecretValue(i.config, secretName, secretField)
	if err != nil {
		return ""
	}
	return pw
}

func (i *Instance) pgVersion() string {
	if i.pgMasterPod() == nil {
		return ""
	}
	image := i.pgMasterPod().Spec.Containers[0].Image
	toks := strings.Split(image, "/")
	return toks[len(toks)-1]
}

// PgActivity executes pg_activity on the postgresql master pod
func (i *Instance) PgActivity() error {
	if i.CRD.Spec.Postgresql.Secret != "" {
		host, _ := kube.GetSecretValue(i.config, i.CRD.Spec.Postgresql.Secret, "DB_HOST")
		port, _ := kube.GetSecretValue(i.config, i.CRD.Spec.Postgresql.Secret, "DB_PORT")
		user, _ := kube.GetSecretValue(i.config, i.CRD.Spec.Postgresql.Secret, "DB_USER")
		cmd := exec.Command("pg_activity", "-d", "postgres", "-U", user, "-h", host, "-p", port)
		cmd.Env = append(os.Environ(), fmt.Sprintf("PGPASSWORD=%s", i.pgPassword()))
		cmd.Stdin = os.Stdin
		cmd.Stdout = os.Stdout
		cmd.Stderr = os.Stderr
		if err := cmd.Run(); err != nil {
			return err
		}
		return nil
	}
	return i.execLocalPgCommand([]string{"pg_activity", "-d", "postgres", "-U", "odoo", "-h", "localhost", "-p"})
}

func (i *Instance) execLocalPgCommand(command []string) error {
	pod := i.pgMasterPod()
	if pod == nil {
		return fmt.Errorf("l'instance '%s' n'a pas de pod postgresql en fonctionnement", i.Name())
	}
	stopChan := make(chan struct{})
	readyChan := make(chan struct{})

	localPort, err := kube.GetFreePort()
	if err != nil {
		return err
	}
	port := fmt.Sprintf("%d:5432", localPort)

	var wg sync.WaitGroup
	wg.Add(1)
	defer func() {
		close(stopChan)
		wg.Wait()
	}()
	go func() {
		defer wg.Done()
		if err := kube.PortForward(i.ToRESTConfig(), pod, port, stopChan, readyChan); err != nil {
			fmt.Printf("Erreur réseau: %s\n", err)
			os.Exit(1)
		}
	}()

	<-readyChan
	command = append(command, strconv.Itoa(localPort))
	cmd := exec.Command(command[0], command[1:]...)
	cmd.Env = append(os.Environ(), fmt.Sprintf("PGPASSWORD=%s", i.pgPassword()))
	cmd.Stdin = os.Stdin
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr
	if err := cmd.Run(); err != nil {
		return err
	}
	return nil
}

// remoteDump dumps the database inside the pg pod and returns the file name.
func (i *Instance) remoteDump(db string, verbose bool) (string, error) {
	if db == "" {
		db = i.Name()
	}
	tmpFile := fmt.Sprintf("/tmp/dump-%s_%s", i.Name(), time.Now().Format("2006-01-02_15-04-05"))
	err := i.tmpDump(db, tmpFile, verbose)
	if err != nil {
		return "", err
	}
	return tmpFile, nil
}

// PgLocalDump executes a local pg_dump on the given db, saving the dump to file
func (i *Instance) PgLocalDump(db, file string, verbose bool) error {
	tmpFile, err := i.remoteDump(db, verbose)
	if err != nil {
		return err
	}
	defer func() {
		if err := i.deleteTmpDump(tmpFile); err != nil {
			fmt.Println("warning: erreur lors de la suppression du dump dans le pod:", err)
		}
	}()
	fmt.Println("Dump terminé, début du téléchargement...")
	return kube.CopyFromPod(i.ToRESTConfig(), i.pgMasterPod(), tmpFile, file)
}

// PgS3Dump dumps the given database in the given S3 bucket with the given file path
func (i *Instance) PgS3Dump(service *s3.ServiceData, db, bucket, file string, verbose bool) error {
	tmpFile, err := i.remoteDump(db, verbose)
	if err != nil {
		return err
	}
	defer func() {
		if err := i.deleteTmpDump(tmpFile); err != nil {
			fmt.Println("warning: erreur lors de la suppression du dump dans le pod:", err)
		}
	}()
	fmt.Println("Dump terminé, début de l'upload sur S3...")
	s3Script := fmt.Sprintf("echo '%s'|bash", s3.PutFileScript(service, tmpFile, bucket, file))
	return i.Exec(s3Script, true, false)
}

// remoteRestore restores the given file dump into the given db
func (i *Instance) remoteRestore(db string, file string, clean bool, options clean.DBCleanOptions, verbose bool) error {
	fmt.Println("Upload terminé. En attente de l'arrêt d'Odoo...")
	err := i.waitForOdooStop(time.Minute)
	if err != nil {
		return err
	}
	err = i.tmpRestore(db, file, verbose)
	fmt.Println(err)
	// We don't stop on errors because pg_restore might have only warnings
	if clean {
		err = i.CleanDB(db, options)
		if err != nil {
			return err
		}
	}
	return i.ScaleOdooUp()
}

// PgLocalRestore executes a local pg_restore on the given db, using the given dump file
func (i *Instance) PgLocalRestore(db, file string, clean bool, options clean.DBCleanOptions, verbose bool) error {
	if _, err := os.Stat(file); err != nil {
		return err
	}
	if db == "" {
		db = i.Name()
	}
	err := i.ScaleOdooToZero()
	if err != nil {
		return err
	}
	fmt.Println("Upload du fichier dump en cours...")
	tmpFile := fmt.Sprintf("/tmp/restore-%s_%s", i.Name(), time.Now().Format("2006-01-02_15-04-05"))
	err = kube.CopyToPod(i.ToRESTConfig(), i.pgMasterPod(), file, tmpFile)
	if err != nil {
		return err
	}
	defer func() {
		if err := i.deleteTmpDump(tmpFile); err != nil {
			fmt.Println("warning: erreur lors de la suppression du dump dans le pod:", err)
		}
	}()
	return i.remoteRestore(db, tmpFile, clean, options, verbose)
}

// PgS3Restore restores the given db, using the given dump file in the given s3 bucket
func (i *Instance) PgS3Restore(service *s3.ServiceData, db, bucket, file string, clean bool, options clean.DBCleanOptions, verbose bool) error {
	if exists, err := s3.ExistsOnS3(service, bucket, file); err != nil || !exists {
		if err != nil {
			return err
		}
		return fmt.Errorf("le fichier %s dans le bucket %s:%s n'existe pas", file, service.Name, bucket)
	}
	if db == "" {
		db = i.Name()
	}
	err := i.ScaleOdooToZero()
	if err != nil {
		return err
	}
	fmt.Println("Upload du fichier dump depuis S3 en cours...")
	tmpFile := fmt.Sprintf("/tmp/restore-%s_%s", i.Name(), time.Now().Format("2006-01-02_15-04-05"))
	s3Script := fmt.Sprintf("echo '%s'|bash", s3.GetFileScript(service, bucket, file, tmpFile))
	err = i.Exec(s3Script, true, false)
	if err != nil {
		return err
	}
	defer func() {
		if err := i.deleteTmpDump(tmpFile); err != nil {
			fmt.Println("warning: erreur lors de la suppression du dump dans le pod:", err)
		}
	}()
	return i.remoteRestore(db, tmpFile, clean, options, verbose)
}

// tmpDump dumps the given db in the tmp directory of the postgresql pod
func (i *Instance) tmpDump(db, file string, verbose bool) error {
	var vStr string
	if verbose {
		vStr = "-v"
	}
	cmd := fmt.Sprintf("PGPASSWORD=%s pg_dump -F c -f %s %s -U odoo -h localhost -p 5432 %s", i.pgPassword(), file, vStr, db)
	return i.Exec(cmd, true, false)
}

// tmpRestore resstores the given db from the given file on the postgresql master pod
// This function drops and recreate the data base
func (i *Instance) tmpRestore(db, file string, verbose bool) error {
	err := i.DropDB(db)
	if err != nil {
		return err
	}
	err = i.createDBIfNotExists(db)
	if err != nil {
		return err
	}
	var vStr string
	if verbose {
		vStr = "-v"
	}
	cmd := fmt.Sprintf("PGPASSWORD=%s pg_restore -d %s -j 4 %s -O -U odoo -h localhost -p 5432 %s", i.pgPassword(), db, vStr, file)
	return i.Exec(cmd, true, false)
}

// deleteTmpDump deletes the given temp dump on the postgres master pod
func (i *Instance) deleteTmpDump(file string) error {
	cmd := fmt.Sprintf("rm %s", file)
	return i.Exec(cmd, true, false)
}

func (i *Instance) createDBIfNotExists(db string) error {
	if i.dbExists(db) {
		return nil
	}
	fmt.Printf("Création de la base de données '%s'\n", db)
	return i.execPsql("postgres", fmt.Sprintf(`CREATE DATABASE \"%s\";`, db))
}

// dbExists returns true if the given db exists
func (i *Instance) dbExists(db string) bool {
	if err := i.execPsql("postgres", "", fmt.Sprintf("-l -A|grep ^%s\\| >/dev/null", db)); err != nil {
		return false
	}
	return true
}

// Psql executes psql on the given database.
// If db is empty, it defaults to the instance name.
func (i *Instance) Psql(db string) error {
	return i.execPsql(db, "")
}

// execPsql executes the given psql command.
// If cmd and extraArgs are empty, launches psql interactively.
func (i *Instance) execPsql(db string, cmd string, extraArgs ...string) error {
	if db == "" {
		db = i.Name()
	}
	var (
		pod              *v1.Pod
		host, user, port string
	)
	switch i.CRD.Spec.Postgresql.Secret {
	case "":
		pod = i.pgMasterPod()
		host = "localhost"
		user = "odoo"
		port = "5432"
	default:
		pod = kube.FirstRunningPod(i.odooPods())
		host = "$DB_HOST"
		user = "$DB_USER"
		port = "$DB_PORT"
	}
	if pod == nil {
		return fmt.Errorf("l'instance '%s' n'a pas de pod postgresql en fonctionnement", i.Name())
	}
	command := fmt.Sprintf("PGPASSWORD=\"%s\" psql -h %s -U %s -p %s %s", i.pgPassword(), host, user, port, db)
	tty := true
	for _, arg := range extraArgs {
		tty = false
		command = fmt.Sprint(command, " ", arg)
	}
	if cmd != "" {
		command += fmt.Sprintf(" -c \"%s\"", cmd)
		tty = false
	}
	return kube.Exec(i.ToRESTConfig(), pod, command, tty)
}

// DBPort opens a port forward between the postgresql port and the given localPort
// This function blocks while the port is open.
func (i *Instance) DBPort(localPort int) error {
	port := fmt.Sprintf("%d:5432", localPort)
	return kube.PortForward(i.ToRESTConfig(), i.pgMasterPod(), port, nil, nil)
}

// CleanDB cleans the given db directly on the postgresql pod.
func (i *Instance) CleanDB(db string, options clean.DBCleanOptions) error {
	fmt.Println("Execution du script principal de nettoyage de la bdd")
	options.AdminPassword = i.OdooAdminPassword()
	mainScript, err := clean.Script(i.odooVersion(), options)
	if err != nil {
		return err
	}
	err = i.execPsql(db, mainScript)
	if err != nil {
		return err
	}
	fmt.Println("Execution du script de nettoyage du projet")
	err = i.ScaleOdooUp()
	if err != nil {
		return err
	}
	err = i.waitForOdooStart(podDefaultTimeout)
	if err != nil {
		return err
	}
	var buf bytes.Buffer
	err = kube.ExecTo(i.ToRESTConfig(), kube.FirstRunningPod(i.odooPods()), &buf, "cat /opt/odoo-addons/dumps_cleaner.sql")
	if err != nil {
		fmt.Println("Erreur lors de la récupération du script du projet:", err)
		return nil
	}
	err = i.ScaleOdooToZero()
	if err != nil {
		return err
	}
	return i.execPsql(db, buf.String())
}

// waitForOdooStop blocks until the postgresql master pod is down
func (i *Instance) waitForPGStop(timeout time.Duration) error {
	startTime := time.Now()
	for time.Since(startTime) < timeout {
		if i.pgMasterPod() == nil {
			return nil
		}
		time.Sleep(time.Second)
	}
	return errors.New("dépassement du timeout lors de l'attente de l'arrêt de postgresql")
}

// waitForOdooStart blocks until the postgresql master pod is up
func (i *Instance) waitForPGStart(timeout time.Duration) error {
	startTime := time.Now()
	for time.Since(startTime) < timeout {
		if kube.PodReady(i.pgMasterPod()) {
			return nil
		}
		time.Sleep(time.Second)
	}
	return errors.New("dépassement du timeout lors de l'attente du démarrage de postgresql")
}

// DropDB drops the database with the given name.
// If the database doesn't exist, DropDB is a no op.
func (i *Instance) DropDB(db string) error {
	if db == "" {
		db = i.Name()
	}
	fmt.Printf("Suppression de la base de données '%s' demandée\n", db)
	if !i.dbExists(db) {
		// DB doesn't exist
		fmt.Printf("Base de données '%s' non trouvée\n", db)
		return nil
	}

	if i.CRD.Spec.Postgresql.Secret == "" {
		fmt.Println("En attente de l'arrêt d'Odoo")
		err := i.ScaleOdooToZero()
		if err != nil {
			return err
		}
		err = i.waitForOdooStop(podDefaultTimeout)
		if err != nil {
			return err
		}
	}

	fmt.Printf("Deconnexion des sessions actives sur la base de données '%s'\n", db)
	err := i.execPsql("postgres", fmt.Sprintf("SELECT pg_terminate_backend(pid) FROM pg_stat_activity WHERE datname = '%s';", db), " > /dev/null")
	if err != nil {
		return err
	}
	fmt.Printf("Suppression de la base de données '%s'\n", db)
	return i.execPsql("postgres", fmt.Sprintf(`DROP DATABASE \"%s\";`, db))
}
