// Copyright 2020 NDP Systèmes. All Rights Reserved.
// See LICENSE file for full licensing details.

package nestor

import (
	"bytes"
	"context"
	"errors"
	"fmt"
	"io"
	"strings"
	"time"

	"github.com/gosuri/uitable"
	postgresv1 "github.com/zalando/postgres-operator/pkg/apis/acid.zalan.do/v1"
	"gitlab.com/nars/nestor/pkg/kube"
	appsv1alpha1 "gitlab.com/nars/odoo-operator/api/apps/v1alpha1"
	corev1 "k8s.io/api/core/v1"
	apierrors "k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/apimachinery/pkg/util/duration"
	"k8s.io/client-go/rest"
	runtimeclient "sigs.k8s.io/controller-runtime/pkg/client"
	"sigs.k8s.io/yaml"
)

const (
	// podDefaultTimeout is the maximum time to wait for a pod to come up or down
	podDefaultTimeout = 2 * time.Minute
	odooSuffix        = "odoo"
	crdSpecKey        = "spec"
)

var userValueKeys = []string{"spec.dockerImage", "spec.ingress", "spec.options", "spec.persistence", "spec.postgresql",
	"spec.redis", "spec.replicas", "spec.resources", "spec.sources", "spec.version", "spec.workers", "spec.backup"}

// An Instance of Odoo on the kubernetes cluster is a Helm release
type Instance struct {
	CRD    *appsv1alpha1.Odoo
	config *kube.Config
}

// Name of the instance
func (i *Instance) Name() string {
	return i.CRD.Name
}

// Namespace of the instance
func (i *Instance) Namespace() string {
	return i.CRD.Namespace
}

// odooPods returns the Odoo pods of this instance
func (i *Instance) odooPods() *corev1.PodList {
	ctx := context.TODO()
	pods, err := i.config.GetKubeClient().CoreV1().Pods(i.Namespace()).List(ctx, metav1.ListOptions{
		LabelSelector: fmt.Sprintf("release=%s,app=odoo", i.Name()),
	})
	if err != nil {
		panic(err)
	}
	return pods
}

// allOdooPods returns all the Odoo pods of this instance, including jobrunner and jobworker
func (i *Instance) allOdooPods() *corev1.PodList {
	ctx := context.TODO()
	pods, err := i.config.GetKubeClient().CoreV1().Pods(i.Namespace()).List(ctx, metav1.ListOptions{
		LabelSelector: fmt.Sprintf("release=%s,app in (odoo,odoo-jobrunner,odoo-jobworker)", i.Name()),
	})
	if err != nil {
		panic(err)
	}
	return pods
}

// allPods returns all the pods of this instance, except job pods
func (i *Instance) allPods() *corev1.PodList {
	ctx := context.TODO()
	pods, err := i.config.GetKubeClient().CoreV1().Pods(i.Namespace()).List(ctx, metav1.ListOptions{
		LabelSelector: fmt.Sprintf("release=%s,app!=odoo-job", i.Name()),
	})
	if err != nil {
		panic(err)
	}
	return pods
}

// jobPods returns the job pods of this instance
func (i *Instance) jobPods() *corev1.PodList {
	ctx := context.TODO()
	pods, err := i.config.GetKubeClient().CoreV1().Pods(i.Namespace()).List(ctx, metav1.ListOptions{
		LabelSelector: fmt.Sprintf("release=%s,app=odoo-job", i.Name()),
	})
	if err != nil {
		panic(err)
	}
	return pods
}

func (i *Instance) odooVersion() string {
	return i.CRD.Spec.Version
}

// OdooAdminPassword returns the db manager password, which is also the admin password
// for cleaned databases.
func (i *Instance) OdooAdminPassword() string {
	ctx := context.TODO()
	secretName := fmt.Sprintf("%s-%s", i.Name(), odooSuffix)
	secret, err := i.config.GetKubeClient().CoreV1().Secrets(i.Namespace()).Get(ctx, secretName, metav1.GetOptions{})
	if err != nil {
		panic(err)
	}
	return string(secret.Data["odoo-admin-password"])
}

func (i *Instance) waitForOdooStop(timeout time.Duration) error {
	startTime := time.Now()
	for time.Since(startTime) < timeout {
		if kube.FirstRunningPod(i.allOdooPods()) == nil {
			return nil
		}
		time.Sleep(time.Second)
	}
	return errors.New("dépassement du timeout lors de l'attente de l'arrêt d'Odoo")
}

func (i *Instance) waitForOdooStart(timeout time.Duration) error {
	startTime := time.Now()
	for time.Since(startTime) < timeout {
		if kube.FirstRunningPod(i.odooPods()) != nil {
			return nil
		}
		time.Sleep(time.Second)
	}
	return errors.New("dépassement du timeout lors de l'attente du démarrage d'Odoo")
}

// WaitForUp waits for the Odoo pods (or the postgresql pod if postgresql is true) to be up
func (i *Instance) WaitForUp(timeout time.Duration, postgresql bool) error {
	if postgresql {
		return i.waitForPGStart(timeout)
	}
	return i.waitForOdooStart(timeout)
}

// WaitForDown waits for the Odoo pods (or the postgresql pod if postgresql is true) to be down
func (i *Instance) WaitForDown(timeout time.Duration, postgresql bool) error {
	if postgresql {
		return i.waitForPGStop(timeout)
	}
	return i.waitForOdooStop(timeout)
}

// UserValues are the values of the instance CRD as a table.
func (i *Instance) UserValues() map[string]interface{} {
	iYAML, err := yaml.Marshal(i.CRD)
	if err != nil {
		panic(err)
	}
	var iVals map[string]interface{}
	err = yaml.Unmarshal(iYAML, &iVals)
	if err != nil {
		panic(err)
	}
	return iVals
}

func defaultUserValues(config *kube.Config) map[string]interface{} {
	odoo := &appsv1alpha1.Odoo{
		ObjectMeta: metav1.ObjectMeta{
			Namespace: config.Namespace,
		},
	}
	odoo.SetDefaults(config.GetRuntimeClient())
	data, err := yaml.Marshal(odoo)
	if err != nil {
		panic(err)
	}
	var vals map[string]interface{}
	err = yaml.Unmarshal(data, &vals)
	return vals
}

// filterValues returns a new map with only the given keys
// Keys must be given as path: eg.: postgresql.persistance
func filterValues(vals map[string]interface{}, keys ...string) map[string]interface{} {
	res := make(map[string]interface{})
	for vKey, vVal := range vals {
		for _, k := range keys {
			toks := strings.Split(k, ".")
			rVal := vVal
			if toks[0] == vKey {
				if len(toks) > 1 {
					vVals, ok := vVal.(map[string]interface{})
					if ok {
						rVal = filterValues(vVals, strings.Join(toks[1:], "."))
						resVal, ok := res[vKey].(map[string]interface{})
						if ok {
							rVal = CoalesceTables(resVal, rVal.(map[string]interface{}))
						}
					}
				}
				res[vKey] = rVal
			}
		}
	}
	return res
}

// consoleCmd returns the remote command to get the odoo console
// depending on the Odoo version.
func (i *Instance) consoleCmd(db string) string {
	var odooCmd, extraOpts string
	switch i.odooVersion() {
	case "8.0":
		odooCmd = "odoo.py"
		extraOpts = "--addons-path=/opt/odoo-addons,/odoo/addons,/odoo/openerp/addons"
	case "9.0":
		odooCmd = "odoo.py"
	default:
		odooCmd = "odoo-bin"
	}
	return fmt.Sprintf("/odoo/%s %s shell -d %s -c /odoo/openerp-server.conf --no-xmlrpc --load=$LOAD", odooCmd, extraOpts, db)
}

// Console launches an interactive Odoo console on this instance.
// If db is empty, defaults to the instance name
func (i *Instance) Console(db string) error {
	if db == "" {
		db = i.Name()
	}
	podCmd := fmt.Sprintf("su odoo -s /bin/bash -c \"%s\"", i.consoleCmd(db))
	return i.Exec(podCmd, false, true)
}

// Exec executes a command on an Odoo pod of the instance.
func (i *Instance) Exec(cmd string, postgresql bool, tty bool) error {
	pod := kube.FirstRunningPod(i.odooPods())
	if postgresql {
		pod = i.pgMasterPod()
	}
	if pod == nil {
		return fmt.Errorf("l'instance '%s' n'a pas de pod en fonctionnement", i.Name())
	}
	return kube.Exec(i.ToRESTConfig(), pod, cmd, tty)
}

// Logs Prints the logs of the instance on stdout.
// If postgresql is true, logs the db pod, otherwise the Odoo pods
func (i *Instance) Logs(postgresql bool, follow bool, tail int64) error {
	pods := i.odooPods()
	if postgresql {
		pods = &corev1.PodList{Items: []corev1.Pod{*i.pgMasterPod()}}
	}
	return kube.Logs(i.config, pods, follow, tail)
}

// Edit opens the default editor to edit the instance's variables
func (i *Instance) Edit(values map[string]interface{}, openEditor bool) error {
	ctx := context.TODO()
	var abortIfUnchanged bool
	if values == nil || len(values) == 0 {
		values = make(map[string]interface{})
		openEditor = true
		abortIfUnchanged = true
	}
	values = CoalesceTables(values, filterValues(i.UserValues(), userValueKeys...))
	if openEditor {
		var err error
		values, err = editValues(values)
		if err != nil {
			if !errors.Is(err, unchangedValsError) || abortIfUnchanged {
				return err
			}
		}
	}
	yData, err := yaml.Marshal(values[crdSpecKey])
	if err != nil {
		return err
	}
	var odooSpec appsv1alpha1.OdooSpec
	err = yaml.Unmarshal(yData, &odooSpec)
	if err != nil {
		return err
	}
	p := runtimeclient.MergeFrom(i.CRD.DeepCopy())
	i.CRD.Spec = odooSpec
	return i.config.GetRuntimeClient().Patch(ctx, i.CRD, p)
}

// YAML prints the instance YAML to the given writer with the given indent
func (i *Instance) YAML(out io.Writer, indent int) error {
	var res bytes.Buffer
	data, err := yaml.Marshal(filterValues(i.UserValues(), userValueKeys...))
	if err != nil {
		return err
	}
	for _, line := range bytes.Split(data, []byte{'\n'}) {
		if len(line) == 0 {
			continue
		}
		for i := 0; i < indent; i++ {
			res.WriteString(" ")
		}
		res.Write(line)
		res.WriteByte('\n')
	}
	_, err = res.WriteTo(out)
	return err
}

// Spec returns the instance as a comma separated list of path.to.config.param=value
func (i *Instance) Spec(out io.Writer) error {
	var res bytes.Buffer
	vals := FlattenTable(filterValues(i.UserValues(), userValueKeys...))
	for k, v := range vals {
		if v == nil {
			continue
		}
		res.WriteString(k)
		res.WriteByte('=')
		res.WriteString(fmt.Sprintf("%#v", v))
		res.WriteByte(',')
	}
	res.Truncate(res.Len() - 1)
	_, err := res.WriteTo(out)
	return err
}

// Details sends a human readable text with the instance data on out
func (i *Instance) Details(out io.Writer) error {
	var res bytes.Buffer
	res.WriteString("NAME: ")
	res.WriteString(i.Name())
	res.WriteByte('\n')
	res.WriteString("URL: ")
	res.WriteString("https://" + i.URL())
	res.WriteByte('\n')
	res.WriteString("NAMESPACE: ")
	res.WriteString(i.Namespace())
	res.WriteByte('\n')

	res.WriteString("VALUES:")
	res.WriteByte('\n')
	err := i.YAML(&res, 2)
	if err != nil {
		return err
	}

	res.WriteString("VERSIONS:")
	res.WriteByte('\n')
	res.WriteString("  Odoo: ")
	res.WriteString(i.odooVersion())
	res.WriteByte('\n')
	res.WriteString("  PostgreSQL: ")
	res.WriteString(i.pgVersion())
	res.WriteByte('\n')

	res.WriteString("SECRETS:")
	res.WriteByte('\n')
	res.WriteString("  Odoo admin / DB Manager password: ")
	res.WriteString(i.OdooAdminPassword())
	res.WriteByte('\n')
	res.WriteString("  PostgreSQL password: ")
	res.WriteString(i.pgPassword())
	res.WriteByte('\n')

	res.WriteString("JOBS:")
	res.WriteByte('\n')
	table := uitable.New()
	table.AddRow("    ", "NAME", "READY", "STATUS", "RESTARTS", "AGE", "NODE")
	for _, p := range i.jobPods().Items {
		ready, total, restarts, age, status := podData(p)
		table.AddRow("", p.Name, fmt.Sprintf("%d/%d", ready, total), status, restarts, age, p.Spec.NodeName)
	}
	res.Write(table.Bytes())
	res.WriteByte('\n')

	res.WriteString("PODS:")
	res.WriteByte('\n')
	table = uitable.New()
	table.AddRow("    ", "NAME", "READY", "STATUS", "RESTARTS", "AGE", "NODE")
	for _, p := range i.allPods().Items {
		ready, total, restarts, age, status := podData(p)
		table.AddRow("", p.Name, fmt.Sprintf("%d/%d", ready, total), status, restarts, age, p.Spec.NodeName)
	}
	res.Write(table.Bytes())
	res.WriteByte('\n')
	_, err = res.WriteTo(out)
	return err
}

func podData(p corev1.Pod) (int, int, int, string, corev1.PodPhase) {
	ready, total, restarts := kube.ContainersInfos(&p)
	var age string
	if p.Status.StartTime != nil {
		age = duration.HumanDuration(time.Since(p.Status.StartTime.Time))
	}
	status := p.Status.Phase
	if p.DeletionTimestamp != nil {
		status = "Terminating"
	}
	return ready, total, restarts, age, status
}

// Delete this instance
func (i *Instance) Delete() error {
	ctx := context.TODO()

	client := i.config.GetRuntimeClient()
	var (
		hasPostgres bool
		postgresql  postgresv1.Postgresql
	)
	if i.CRD.Spec.Postgresql.Secret == "" {
		hasPostgres = true
		err := client.Get(ctx, types.NamespacedName{Name: fmt.Sprintf("ndp-%s-postgresql", i.Name()), Namespace: i.Namespace()}, &postgresql)
		if err != nil {
			return err
		}
	}
	err := client.Delete(ctx, i.CRD)
	if err != nil {
		return err
	}
	// Delete PVC
	var pvc corev1.PersistentVolumeClaim
	err = client.Get(ctx, types.NamespacedName{Name: fmt.Sprintf("%s-odoo", i.Name()), Namespace: i.Namespace()}, &pvc)
	if err != nil {
		if !apierrors.IsNotFound(err) {
			return err
		}
	} else {
		err = client.Delete(ctx, &pvc)
		if err != nil {
			return err
		}
	}
	// Delete remaining job pods if any
	labels := map[string]string{
		"app":      "odoo-job",
		"release":  i.Name(),
		"operator": "odoo",
	}
	var pod corev1.Pod
	err = client.DeleteAllOf(ctx, &pod, runtimeclient.MatchingLabels(labels), runtimeclient.InNamespace(i.Namespace()))
	if err != nil {
		return err
	}
	// Delete pg too
	if hasPostgres {
		return client.Delete(ctx, &postgresql)
	}
	return nil
}

// Copy src file to dst file.
// If toPod is true, dst is in the pod and src is local, else src is in the pod and dst is local.
// If postgresql is true, the the pod is the PG pod, else it is the first Odoo pod.
func (i *Instance) Copy(src, dst string, toPod bool, postgresql bool) error {
	pod := kube.FirstRunningPod(i.odooPods())
	conf, err := i.config.ToRESTConfig()
	if err != nil {
		return err
	}
	if postgresql {
		pod = i.pgMasterPod()
	}
	if toPod {
		return kube.CopyToPod(conf, pod, src, dst)
	}
	return kube.CopyFromPod(conf, pod, src, dst)
}

// ScaleOdooToZero scales the odoo deployment to zero
func (i *Instance) ScaleOdooToZero() error {
	ctx := context.TODO()
	p := runtimeclient.MergeFrom(i.CRD.DeepCopy())
	i.CRD.Spec.Disabled = true
	return i.config.GetRuntimeClient().Patch(ctx, i.CRD, p)
}

// ScaleOdooUp scales the odoo deployment to the number of
// desired instance in the chart values. It also scales back the
// required number of workers.
func (i *Instance) ScaleOdooUp() error {
	ctx := context.TODO()
	p := runtimeclient.MergeFrom(i.CRD.DeepCopy())
	i.CRD.Spec.Disabled = false
	return i.config.GetRuntimeClient().Patch(ctx, i.CRD, p)
}

// followJobLogs follows the logs of the install/update job.
func (i *Instance) followJobLogs() error {
	ctx := context.TODO()
	time.Sleep(time.Second)
	startTime := time.Now()
	var (
		pods *corev1.PodList
		err  error
		ok   bool
	)
	for time.Since(startTime) < podDefaultTimeout {
		pods, err = i.config.GetKubeClient().CoreV1().Pods(i.Namespace()).List(ctx, metav1.ListOptions{
			LabelSelector: fmt.Sprintf("release=%s,app=odoo-job", i.Name()),
		})
		if err != nil {
			return err
		}
		if kube.FirstRunningPod(pods) != nil {
			ok = true
			break
		}
		time.Sleep(time.Second)
	}
	if !ok {
		return errors.New("dépassement du timeout lors de l'attente du démarrage du job")
	}
	return kube.Logs(i.config, &corev1.PodList{Items: []corev1.Pod{*kube.FirstRunningPod(pods)}}, true, 0)
}

// InstallModule installs the given Odoo module, creating the DB if it doesn't exist yet
func (i *Instance) InstallModule(db, module string) error {
	ctx := context.TODO()
	fmt.Println("Module à installer:", module)
	if db == "" {
		db = i.Name()
	}
	p := runtimeclient.MergeFrom(i.CRD.DeepCopy())
	i.CRD.Spec.Operations.Install = module
	err := i.config.GetRuntimeClient().Patch(ctx, i.CRD, p)
	if err != nil {
		return err
	}
	return i.followJobLogs()
}

// UpdateModule updates the given Odoo module
func (i *Instance) UpdateModule(module string) error {
	ctx := context.TODO()
	p := runtimeclient.MergeFrom(i.CRD.DeepCopy())
	i.CRD.Spec.Operations.Update = module
	err := i.config.GetRuntimeClient().Patch(ctx, i.CRD, p)
	if err != nil {
		return err
	}
	return i.followJobLogs()
}

// Restart the odoo pods of the instance, setting back nb of workers and instances.
// If postgresql is true, restarts postgresql pods instead.
func (i *Instance) Restart(postgresql bool) error {
	client := i.config.GetKubeClient()
	if postgresql {
		return kube.DeletePod(client, i.pgMasterPod())
	}
	var errs []string
	for _, pod := range i.allOdooPods().Items {
		err := kube.DeletePod(client, &pod)
		if err != nil {
			errs = append(errs, err.Error())
		}
	}
	if len(errs) > 0 {
		return errors.New(strings.Join(errs, "\n"))
	}
	// Wait for delete order to be taken into account by Kubernetes
	// Otherwiser ScaleOdooUp will fail because the deployment has been changed.
	time.Sleep(time.Second)
	return i.ScaleOdooUp()
}

// URL returns the ingress URL to access this instance
func (i *Instance) URL() string {
	return i.CRD.Status.URL
}

// OdooAge returns the age of the oldest Odoo Pod
func (i *Instance) OdooAge() time.Duration {
	var age time.Duration
	for _, pod := range i.odooPods().Items {
		if pod.Status.Phase != "Running" {
			continue
		}
		podAge := time.Since(pod.Status.StartTime.Time)
		if podAge > age {
			age = podAge
		}
	}
	return age
}

// ToRESTConfig returns a new rest.Config from this instance configuration
func (i *Instance) ToRESTConfig() *rest.Config {
	conf, err := i.config.ToRESTConfig()
	if err != nil {
		panic(err)
	}
	return conf
}

// Instances returns a list of all Odoo instances
func Instances(config *kube.Config) ([]*Instance, error) {
	ctx := context.TODO()
	client := config.GetRuntimeClient()
	var odooes appsv1alpha1.OdooList
	err := client.List(ctx, &odooes, &runtimeclient.ListOptions{Namespace: config.Namespace})
	if err != nil {
		return nil, err
	}
	res := make([]*Instance, len(odooes.Items))
	for i := range odooes.Items {
		res[i] = &Instance{
			CRD:    &odooes.Items[i],
			config: config,
		}
	}
	return res, nil
}

// InstanceNames returns the list of all instance names.
//
// This methods is much faster than getting instances and iterate over names.
func InstanceNames(config *kube.Config) ([]string, error) {
	instances, err := Instances(config)
	if err != nil {
		return nil, err
	}
	res := make([]string, len(instances))
	for i, instance := range instances {
		res[i] = instance.CRD.Name
	}
	return res, nil
}

// InstanceByName returns the instance with the given name or nil if it doesn't exist
func InstanceByName(config *kube.Config, name string) (*Instance, error) {
	ctx := context.TODO()
	client := config.GetRuntimeClient()
	var odoo appsv1alpha1.Odoo
	err := client.Get(ctx, runtimeclient.ObjectKey{Name: name, Namespace: config.Namespace}, &odoo)
	if err != nil {
		return nil, err
	}
	return &Instance{
		CRD:    &odoo,
		config: config,
	}, nil
}

// NewInstance creates a new instance with the given name, asking the user for values
func NewInstance(config *kube.Config, name string, values map[string]interface{}, openEditor bool) (*Instance, error) {
	if values == nil {
		values = make(map[string]interface{})
		openEditor = true
	}
	values = CoalesceTables(values, filterValues(defaultUserValues(config), userValueKeys...))
	if openEditor {
		var err error
		values, err = editValues(values)
		if err != nil && !errors.Is(err, unchangedValsError) {
			return nil, err
		}
	}
	ctx := context.TODO()
	data, err := yaml.Marshal(values)
	if err != nil {
		return nil, err
	}
	var odoo appsv1alpha1.Odoo
	err = yaml.Unmarshal(data, &odoo)
	if err != nil {
		return nil, err
	}
	odoo.ObjectMeta.Namespace = config.Namespace
	client := config.GetRuntimeClient()
	odoo.SetDefaults(client)
	odoo.ObjectMeta.Name = name
	odoo.ObjectMeta.Namespace = config.Namespace
	err = client.Create(ctx, &odoo)
	if err != nil {
		return nil, err
	}
	return &Instance{
		CRD:    &odoo,
		config: config,
	}, nil
}
