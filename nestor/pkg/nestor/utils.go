// Copyright 2020 NDP Systèmes. All Rights Reserved.
// See LICENSE file for full licensing details.

package nestor

import (
	"fmt"
	"strings"

	"k8s.io/apimachinery/pkg/util/duration"
)

// CoalesceTables merges a source map into a destination map.
//
// dest is considered authoritative.
func CoalesceTables(dst, src map[string]interface{}) map[string]interface{} {
	if src == nil {
		return dst
	}
	if dst == nil {
		return src
	}
	// Because dest has higher precedence than src, dest values override src
	// values.
	for key, val := range src {
		if dv, ok := dst[key]; ok && dv == nil {
			delete(dst, key)
		} else if !ok {
			dst[key] = val
		} else if isTable(val) {
			if isTable(dv) {
				CoalesceTables(dv.(map[string]interface{}), val.(map[string]interface{}))
			} else {
				fmt.Printf("warning: cannot overwrite table with non table for %s (%v)", key, val)
			}
		} else if isTable(dv) {
			fmt.Printf("warning: destination for %s is a table. Ignoring non-table value %v", key, val)
		}
	}
	return dst
}

// FlattenTable returns a map with only a single level and path-like keys
func FlattenTable(table map[string]interface{}) map[string]interface{} {
	res := make(map[string]interface{})
	flattenTable(table, &res, "")
	return res
}

func flattenTable(src map[string]interface{}, dst *map[string]interface{}, prefix string) {
	for k, v := range src {
		key := fmt.Sprintf("%s.%s", prefix, k)
		if prefix == "" {
			key = k
		}
		switch val := v.(type) {
		case map[string]interface{}:
			flattenTable(val, dst, key)
		default:
			(*dst)[key] = v
		}
	}
}

// GetValueInTable returns the given key in the table.
// Key can be a nested path
func GetValueInTable(table map[string]interface{}, key string) interface{} {
	toks := strings.Split(key, ".")
	if len(toks) == 1 {
		return table[key]
	}
	targetTable, ok := table[toks[0]].(map[string]interface{})
	if !ok {
		return nil
	}
	return GetValueInTable(targetTable, strings.Join(toks[1:], "."))
}

// SetValueInTable adds the given key with the given value to the table.
// key can be a nested path.
func SetValueInTable(key string, value interface{}, table *map[string]interface{}) {
	toks := strings.Split(key, ".")
	if len(toks) == 1 {
		(*table)[key] = value
		return
	}
	tt, exists := (*table)[toks[0]]
	targetTable, ok := tt.(map[string]interface{})
	if !exists || !ok {
		(*table)[toks[0]] = make(map[string]interface{})
	}
	targetTable = (*table)[toks[0]].(map[string]interface{})
	SetValueInTable(strings.Join(toks[1:], "."), value, &targetTable)
}

// istable is a special-purpose function to see if the present thing matches the definition of a YAML table.
func isTable(v interface{}) bool {
	_, ok := v.(map[string]interface{})
	return ok
}

// FormatInstances returns a table with each instance data defined in the format string.
// If header is true, a header line is inserted at the top.
func FormatInstances(instances []*Instance, format string, header bool) [][]interface{} {
	var res [][]interface{}
	fields := strings.Split(format, ",")
	if header {
		header := make([]interface{}, len(fields))
		for i, field := range fields {
			header[i] = strings.ToUpper(field)
		}
		res = append(res, header)
	}
	for _, instance := range instances {
		line := make([]interface{}, len(fields))
		for i, field := range fields {
			var value interface{}
			switch field {
			case "name":
				value = instance.Name()
			case "url":
				value = "https://" + instance.URL()
			case "namespace":
				value = instance.Namespace()
			case "age":
				value = duration.HumanDuration(instance.OdooAge())
			case "password":
				value = instance.OdooAdminPassword()
			case "pg-password":
				value = instance.pgPassword()
			default:
				value = GetValueInTable(instance.UserValues(), field)
			}
			line[i] = value
		}
		res = append(res, line)
	}
	return res
}
