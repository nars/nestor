// Copyright 2021 NDP Systèmes. All Rights Reserved.
// See LICENSE file for full licensing details.

package nestor

import (
	"context"

	"gitlab.com/nars/nestor/pkg/kube"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var dbLabels = map[string]string{
	"manager": "nestor",
	"type":    "db",
}

// DatabaseConnexion holds the information to connect to a database
type DatabaseConnexion struct {
	Name     string
	Host     string
	Port     string
	User     string
	Password string
}

// DBSecretList all db secrets defined in this cluster
func DBSecretList(config *kube.Config) ([]string, error) {
	ctx := context.TODO()
	labelSelector := metav1.LabelSelector{
		MatchLabels: dbLabels,
	}
	secrets, err := config.GetKubeClient().CoreV1().Secrets(config.Namespace).List(ctx, metav1.ListOptions{
		LabelSelector: metav1.FormatLabelSelector(&labelSelector),
	})
	if err != nil {
		return nil, err
	}
	secretNames := make([]string, len(secrets.Items))
	for i, secret := range secrets.Items {
		secretNames[i] = secret.Name
	}
	return secretNames, nil
}

// DBSecretNew creates a new db secret with the given ServiceData
func DBSecretNew(config *kube.Config, data *DatabaseConnexion) error {
	ctx := context.TODO()
	secret := corev1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Namespace: config.Namespace,
			Name:      data.Name,
			Labels:    dbLabels,
		},
		StringData: map[string]string{
			"DB_HOST":     data.Host,
			"DB_PORT":     data.Port,
			"DB_USER":     data.User,
			"DB_PASSWORD": data.Password,
		},
	}
	_, err := config.GetKubeClient().CoreV1().Secrets(config.Namespace).Create(ctx, &secret, metav1.CreateOptions{})
	return err
}

// DBSecretGet returns the given db secret as a ServiceData
func DBSecretGet(config *kube.Config, name string) (*DatabaseConnexion, error) {
	ctx := context.TODO()
	secret, err := config.GetKubeClient().CoreV1().Secrets(config.Namespace).Get(ctx, name, metav1.GetOptions{})
	if err != nil {
		return nil, err
	}
	return &DatabaseConnexion{
		Name:     name,
		Host:     string(secret.Data["DB_HOST"]),
		Port:     string(secret.Data["DB_PORT"]),
		User:     string(secret.Data["DB_USER"]),
		Password: string(secret.Data["DB_PASSWORD"]),
	}, nil
}

// DBSecretDelete the db secret with the given name
func DBSecretDelete(config *kube.Config, name string) error {
	ctx := context.TODO()
	return config.GetKubeClient().CoreV1().Secrets(config.Namespace).Delete(ctx, name, metav1.DeleteOptions{})
}
