// Copyright 2021 NDP Systèmes. All Rights Reserved.
// See LICENSE file for full licensing details.

package nestor

import (
	"fmt"
	"os"
	"path/filepath"

	"gitlab.com/nars/nestor/pkg/kube"
	"gitlab.com/nars/nestor/pkg/s3"
	v1 "k8s.io/api/core/v1"
)

const odooFilestoreDir = "/var/lib/odoo/filestore"

// FSLocalDump dumps the filestore to the local computer into the given destination directory
func (i *Instance) FSLocalDump(destDir string, progress bool) error {
	if i.CRD.Spec.Persistence.S3.Enabled {
		fsService, err := s3.Get(i.config, i.CRD.Spec.Persistence.S3.Secret)
		if err != nil {
			return err
		}
		return s3.CopyS3ToLocal(fsService, i.CRD.Spec.Persistence.S3.Bucket, "", destDir, progress)
	}
	return kube.CopyFromPod(i.ToRESTConfig(), kube.FirstRunningPod(i.odooPods()), odooFilestoreDir, destDir)
}

// FSS3Dump dumps the filestore into the given S3 bucket in the given destination directory
func (i *Instance) FSS3Dump(service *s3.ServiceData, bucket, destDir string, progress bool) error {
	if i.CRD.Spec.Persistence.S3.Enabled {
		fsService, err := s3.Get(i.config, i.CRD.Spec.Persistence.S3.Secret)
		if err != nil {
			return err
		}
		return s3.CopyS3ToS3(fsService, i.CRD.Spec.Persistence.S3.Bucket, "", service, bucket, destDir, progress)
	}
	tmpDir, err := os.MkdirTemp("", "nestor")
	if err != nil {
		return err
	}
	defer os.RemoveAll(tmpDir)
	err = kube.CopyFromPod(i.ToRESTConfig(), kube.FirstRunningPod(i.odooPods()), odooFilestoreDir, tmpDir)
	if err != nil {
		return err
	}
	return s3.CopyLocalToS3(service, tmpDir, bucket, destDir, progress)
}

// copyLocalDirContentToPod copies the content of the given srcDir inside destDir on the given pod
func (i *Instance) copyLocalDirContentToPod(srcDir string, pod *v1.Pod, destDir string) error {
	err := kube.CopyDirToPod(i.ToRESTConfig(), pod, srcDir, destDir)
	if err != nil {
		return err
	}
	return kube.Exec(i.ToRESTConfig(), pod, fmt.Sprintf("chown -R odoo:odoo %s", destDir), false)
}

// FSLocalRestore restores the filestore from the given source directory on the local computer
func (i *Instance) FSLocalRestore(srcDir, db string, noRename, progress bool) error {
	if db == "" {
		db = i.Name()
	}
	// Check if srcDir exists
	if _, err := os.Stat(srcDir); err != nil {
		return err
	}
	// Check srcDir content and define what to copy
	dirs, err := os.ReadDir(srcDir)
	if err != nil {
		return err
	}
	if len(dirs) != 1 {
		fmt.Println(`Le dossier de filestore source contient plusieurs dossiers correspondants à des bases de données.`)
		noRename = true
	}
	srcDBDir := filepath.Join(srcDir, dirs[0].Name())
	destDir := db
	if noRename {
		fmt.Println("Le dossier de la base de données ne sera pas renommé.")
		// We copy the root folder instead of the db folder
		srcDBDir = srcDir
		destDir = ""
	}
	// Start copy
	if i.CRD.Spec.Persistence.S3.Enabled {
		fsService, err := s3.Get(i.config, i.CRD.Spec.Persistence.S3.Secret)
		if err != nil {
			return err
		}
		return s3.CopyLocalToS3(fsService, srcDBDir, i.CRD.Spec.Persistence.S3.Bucket, destDir, progress)
	}
	return i.copyLocalDirContentToPod(srcDBDir, kube.FirstRunningPod(i.odooPods()), filepath.Join(odooFilestoreDir, destDir))
}

// FSS3Restore restores the filestore from given source directory in the given bucket of the given S3 service
func (i *Instance) FSS3Restore(service *s3.ServiceData, bucket, srcDir, db string, noRename, progress bool) error {
	if db == "" {
		db = i.Name()
	}
	// Check if srcDir exists on S3
	if exists, err := s3.ExistsOnS3(service, bucket, srcDir); err != nil || !exists {
		if err != nil {
			return err
		}
		return fmt.Errorf("le dossier %s dans le bucket %s:%s n'existe pas", srcDir, service.Name, bucket)
	}
	// Check srcDir content and define what to copy
	dirs, err := s3.ListDir(service, bucket, srcDir)
	if err != nil {
		return err
	}
	if len(dirs) != 1 {
		fmt.Println(`Le dossier de filestore source contient plusieurs dossiers correspondants à des bases de données.`)
		noRename = true
	}
	srcDBDir := filepath.Join(srcDir, dirs[0])
	destDir := db
	if noRename {
		fmt.Println("Le dossier de la base de données ne sera pas renommé.")
		// We copy the root folder instead of the db folder
		srcDBDir = srcDir
		destDir = ""
	}
	// Start copy
	if i.CRD.Spec.Persistence.S3.Enabled {
		fsService, err := s3.Get(i.config, i.CRD.Spec.Persistence.S3.Secret)
		if err != nil {
			return err
		}
		return s3.CopyS3ToS3(service, bucket, srcDBDir, fsService, i.CRD.Spec.Persistence.S3.Bucket, destDir, progress)
	}
	tmpDir, err := os.MkdirTemp("", "nestor")
	if err != nil {
		return err
	}
	defer os.RemoveAll(tmpDir)
	err = s3.CopyS3ToLocal(service, bucket, srcDBDir, filepath.Join(tmpDir, destDir), progress)
	if err != nil {
		return err
	}
	return i.copyLocalDirContentToPod(tmpDir, kube.FirstRunningPod(i.odooPods()), odooFilestoreDir)
}
