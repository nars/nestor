// Copyright 2020 NDP Systèmes. All Rights Reserved.
// See LICENSE file for full licensing details.

package clean

import (
	"bytes"
	"crypto/rand"
	"crypto/sha512"
	"encoding/base64"
	"fmt"
	"strconv"
	"strings"
	"text/template"

	"golang.org/x/crypto/pbkdf2"
)

var cleanScriptTmpl = template.Must(template.New("").Parse(`
DELETE FROM ir_mail_server;
DELETE FROM fetchmail_server;
UPDATE ir_cron SET active=FALSE;
DELETE FROM ir_attachment WHERE {{ .MimeTypeField }} in ('text/css', 'application/javascript');
UPDATE ir_config_parameter SET VALUE='TEST </br> ' || CURRENT_DATE WHERE KEY='ribbon.name';
DELETE FROM ir_config_parameter WHERE KEY='web.base.url';
`))

var passwordScriptTmpl = template.Must(template.New("").Parse(`
UPDATE res_users SET {{ .PasswordField }}='{{ .Password }}' WHERE login='admin';
UPDATE res_users SET {{ .PasswordField }}={{ .UserPassword }} WHERE login!='admin';
`))

// DBCleanOptions are the options that can be passed to the cleaner
type DBCleanOptions struct {
	// AdminPassword is the password to apply to the admin user
	AdminPassword string
	// NoResetPassword tells the cleaner not to touch the user passwords
	NoResetPassword bool
	// AllUsers telles the cleaner to set the password to all users and
	// not only the admin.
	AllUsers bool
}

type scriptData struct {
	PasswordField string
	Password      string
	UserPassword  string
	MimeTypeField string
}

const (
	// iterations is the number of iterations used when hashing a password
	iterations = 25000
	// saltLen is the length of the randomly generated salt
	saltLen = 16
	// keyLen is the length of the pbkdf2 key to generate
	keyLen = 64
)

// b64EncodeToString encodes the given values as base64 used by pbkdf2 in python passlib
func b64EncodeToString(in []byte) string {
	res := base64.RawStdEncoding.EncodeToString(in)
	res = strings.ReplaceAll(res, "+", ".")
	return res
}

// hash returns a hashed string password in PBKDF2/SHA256 format
func hash(password string) (string, error) {
	salt := make([]byte, saltLen)
	_, err := rand.Read(salt)
	if err != nil {
		return "", err
	}
	dk := pbkdf2.Key([]byte(password), salt, iterations, keyLen, sha512.New)

	encSalt := b64EncodeToString(salt)
	hashedPW := fmt.Sprintf("$pbkdf2-sha512$%d$%s$%s", iterations, encSalt, b64EncodeToString(dk))
	return hashedPW, nil
}

// Script returns the SQL cleaning script for the given odoo version and admin password
func Script(version string, options DBCleanOptions) (string, error) {
	toks := strings.Split(version, "-")
	v, err := strconv.ParseFloat(toks[0], 64)
	if err != nil {
		return "", err
	}
	pwdField := "password_crypt"
	if v == 7.0 || v >= 12.0 {
		pwdField = "password"
	}
	mimeTypeField := "file_type"
	if v >= 9.0 {
		mimeTypeField = "mimetype"
	}
	var buf bytes.Buffer
	err = cleanScriptTmpl.Execute(&buf, scriptData{
		MimeTypeField: mimeTypeField,
	})
	if err != nil {
		return "", err
	}
	if options.NoResetPassword {
		return buf.String(), nil
	}
	pwdHash, err := hash(options.AdminPassword)
	if v == 7.0 {
		pwdHash = options.AdminPassword
	}
	if err != nil {
		return "", nil
	}
	userPassword := "NULL"
	if options.AllUsers {
		userPassword = fmt.Sprintf("'%s'", pwdHash)
	}
	err = passwordScriptTmpl.Execute(&buf, scriptData{
		PasswordField: pwdField,
		Password:      pwdHash,
		UserPassword:  userPassword,
	})
	if err != nil {
		return "", err
	}
	return buf.String(), nil
}
