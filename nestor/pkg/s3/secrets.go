// Copyright 2021 NDP Systèmes. All Rights Reserved.
// See LICENSE file for full licensing details.

package s3

import (
	"context"

	"gitlab.com/nars/nestor/pkg/kube"
	corev1 "k8s.io/api/core/v1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
)

var s3Labels = map[string]string{
	"manager": "nestor",
	"type":    "s3",
}

// ServiceData holds the information to connect to an S3 service
type ServiceData struct {
	Name      string
	AccessKey string
	SecretKey string
	Host      string
	Region    string
}

// List all s3 secrets defined in this cluster
func List(config *kube.Config) ([]string, error) {
	ctx := context.TODO()
	labelSelector := metav1.LabelSelector{
		MatchLabels: s3Labels,
	}
	secrets, err := config.GetKubeClient().CoreV1().Secrets(config.Namespace).List(ctx, metav1.ListOptions{
		LabelSelector: metav1.FormatLabelSelector(&labelSelector),
	})
	if err != nil {
		return nil, err
	}
	secretNames := make([]string, len(secrets.Items))
	for i, secret := range secrets.Items {
		secretNames[i] = secret.Name
	}
	return secretNames, nil
}

// New creates a new S3 secret with the given ServiceData
func New(config *kube.Config, data *ServiceData) error {
	ctx := context.TODO()
	secret := corev1.Secret{
		ObjectMeta: metav1.ObjectMeta{
			Namespace: config.Namespace,
			Name:      data.Name,
			Labels:    s3Labels,
		},
		StringData: map[string]string{
			"S3_ACCESS_KEY": data.AccessKey,
			"S3_SECRET_KEY": data.SecretKey,
			"S3_HOST":       data.Host,
			"S3_REGION":     data.Region,
		},
	}
	_, err := config.GetKubeClient().CoreV1().Secrets(config.Namespace).Create(ctx, &secret, metav1.CreateOptions{})
	return err
}

// Get returns the given S3 secret as a ServiceData
func Get(config *kube.Config, name string) (*ServiceData, error) {
	ctx := context.TODO()
	secret, err := config.GetKubeClient().CoreV1().Secrets(config.Namespace).Get(ctx, name, metav1.GetOptions{})
	if err != nil {
		return nil, err
	}
	return &ServiceData{
		Name:      name,
		AccessKey: string(secret.Data["S3_ACCESS_KEY"]),
		SecretKey: string(secret.Data["S3_SECRET_KEY"]),
		Host:      string(secret.Data["S3_HOST"]),
		Region:    string(secret.Data["S3_REGION"]),
	}, nil
}

// Delete the s3 secret with the given name
func Delete(config *kube.Config, name string) error {
	ctx := context.TODO()
	return config.GetKubeClient().CoreV1().Secrets(config.Namespace).Delete(ctx, name, metav1.DeleteOptions{})
}
