// Copyright 2020 NDP Systèmes. All Rights Reserved.
// See LICENSE file for full licensing details.

package s3

import (
	"bytes"
	"text/template"
)

var s3CurlScriptTmpl = template.Must(template.New("").Parse(`
function hmac_sha256 {
  key="$1"
  data="$2"
  echo -n "$data" | openssl dgst -sha256 -mac HMAC -macopt "$key" | sed "s/^.* //"
}

function hash {
  data="$1"
  echo -n "$data" | openssl dgst -sha256 | sed "s/^.* //"
}

timestamp=$(date -u +%Y%m%dT%H%M%SZ)
dateShort=$(date -u +%Y%m%d)
s3Bucket="{{ .BucketName }}"
fileName="{{ .FileName }}"
s3AccessKey="{{ .S3.AccessKey }}"
s3SecretKey="{{ .S3.SecretKey }}"
region="{{ .S3.Region }}"
host="${s3Bucket}.{{ .S3.Host }}"
HTTPMethod="{{ .Method }}"
canonicalURI="/${fileName}"
canonicalQueryString=""
payloadPath="{{ .Payload }}"
hashedPayload=$(openssl dgst -sha256 "$payloadPath" | sed "s/^.* //")
canonicalHeaders="host:${host}
x-amz-content-sha256:${hashedPayload}
x-amz-date:${timestamp}"
signedHeaders="host;x-amz-content-sha256;x-amz-date"
canonicalRequest="${HTTPMethod}
${canonicalURI}
${canonicalQueryString}
${canonicalHeaders}

${signedHeaders}
${hashedPayload}"

canonicalRequestHash=$(hash "$canonicalRequest")

stringToSign="AWS4-HMAC-SHA256
${timestamp}
${dateShort}/${region}/s3/aws4_request
${canonicalRequestHash}"

dateKey=$(hmac_sha256 key:"AWS4${s3SecretKey}" "$dateShort")
dateRegionKey=$(hmac_sha256 "hexkey:$dateKey" "$region")
dateRegionServiceKey=$(hmac_sha256 "hexkey:$dateRegionKey" "s3")
signingKey=$(hmac_sha256 "hexkey:$dateRegionServiceKey" "aws4_request")

signature=$(hmac_sha256 "hexkey:${signingKey}" "$stringToSign")

authHeader="Authorization: AWS4-HMAC-SHA256 Credential=${s3AccessKey}/${dateShort}/${region}/s3/aws4_request, SignedHeaders=${signedHeaders}, Signature=${signature}"

curl -X "$HTTPMethod" \
-T "$payloadPath" \
-H "$authHeader" \
-H "Host: ${host}" \
-H "x-amz-content-sha256: $hashedPayload" \
-H "x-amz-date: $timestamp" \
{{ if ne .Output "" }}--output {{ .Output }}{{ end }} \
https://${host}${canonicalURI}
`))

type scriptData struct {
	BucketName string
	FileName   string
	Payload    string
	Method     string
	Output     string
	S3         *ServiceData
}

// GetFileScript returns a curl script to retrieve the given
// file from the given bucket.
func GetFileScript(service *ServiceData, bucket, file, dest string) string {
	var buf bytes.Buffer
	err := s3CurlScriptTmpl.Execute(&buf, scriptData{
		BucketName: bucket,
		FileName:   file,
		Payload:    "/dev/null",
		Method:     "GET",
		Output:     dest,
		S3:         service,
	})
	if err != nil {
		panic(err)
	}
	return buf.String()
}

// PutFileScript returns a curl script to upload the given
// file to the given bucket and destination.
func PutFileScript(service *ServiceData, file, bucket, dest string) string {
	var buf bytes.Buffer
	err := s3CurlScriptTmpl.Execute(&buf, scriptData{
		BucketName: bucket,
		FileName:   dest,
		Payload:    file,
		Method:     "PUT",
		Output:     "",
		S3:         service,
	})
	if err != nil {
		panic(err)
	}
	return buf.String()
}
