// Copyright 2021 NDP Systèmes. All Rights Reserved.
// See LICENSE file for full licensing details.

package s3

import (
	"context"
	"fmt"
	"path"

	_ "github.com/rclone/rclone/backend/local"
	_ "github.com/rclone/rclone/backend/s3"
	rcmd "github.com/rclone/rclone/cmd"
	"github.com/rclone/rclone/fs"
	"github.com/rclone/rclone/fs/config/configfile"
	"github.com/rclone/rclone/fs/operations"
	rsync "github.com/rclone/rclone/fs/sync"
)

const (
	s3Checkers  = 40
	s3Transfers = 40
)

// s3ConnectionString returns a connection string to an s3 service valid for rclone NewFS
func s3ConnectionString(service *ServiceData, bucket, src string) []string {
	res := fmt.Sprintf(":s3,endpoint='%s',region='%s',access_key_id='%s',secret_access_key='%s':%s",
		service.Host, service.Region, service.AccessKey, service.SecretKey, path.Join(bucket, src),
	)
	return []string{res}
}

// s3Copy copies src to dest. Arguments should be valid rclone args
func s3Copy(src, dest []string, progress bool) error {
	ctx := context.Background()
	configfile.LoadConfig(ctx)
	cfg := fs.GetConfig(ctx)
	cfg.Checkers = s3Checkers
	cfg.Transfers = s3Transfers
	srcFS := rcmd.NewFsDir(src)
	dstFs := rcmd.NewFsDir(dest)
	stopProgress := func() {}
	if progress {
		stopProgress = startProgress()
	}
	defer stopProgress()
	return rsync.CopyDir(ctx, dstFs, srcFS, true)
}

// ExistsOnS3 checks whether the given file exists on the S3 server
func ExistsOnS3(service *ServiceData, bucket, file string) (bool, error) {
	ctx := context.Background()
	configfile.LoadConfig(ctx)
	f := rcmd.NewFsSrc(s3ConnectionString(service, bucket, file))
	var exists bool
	err := operations.ListFn(ctx, f, func(object fs.Object) {
		exists = true
	})
	if err != nil {
		return false, err
	}
	return exists, nil
}

// ListDir lists the directories inside the given dir
func ListDir(service *ServiceData, bucket, dir string) ([]string, error) {
	ctx := context.Background()
	configfile.LoadConfig(ctx)
	f := rcmd.NewFsDir(s3ConnectionString(service, bucket, dir))
	dirs, err := f.List(ctx, "")
	if err != nil {
		return nil, err
	}
	var res []string
	for _, dir := range dirs {
		if _, ok := dir.(*fs.Dir); ok {
			res = append(res, dir.Remote())
		}
	}
	return res, nil
}

// CopyLocalToS3 copies the local src folder to the remote dest folder in the given bucket
func CopyLocalToS3(service *ServiceData, src, bucket, dest string, progress bool) error {
	return s3Copy([]string{src}, s3ConnectionString(service, bucket, dest), progress)
}

// CopyS3ToLocal copies the src file (or folder) in the given S3 bucket to the local dest
func CopyS3ToLocal(service *ServiceData, bucket, src, dest string, progress bool) error {
	return s3Copy(s3ConnectionString(service, bucket, src), []string{dest}, progress)
}

// CopyS3ToS3 copies data from src S3 to dest S3
func CopyS3ToS3(srcService *ServiceData, srcBucket, srcPath string, destService *ServiceData, destBucket, destPath string, progress bool) error {
	return s3Copy(s3ConnectionString(srcService, srcBucket, srcPath), s3ConnectionString(destService, destBucket, destPath), progress)
}
