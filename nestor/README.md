# Nestor

`nestor` est l'utilitaire de gestion des instances Odoo de NDP Systèmes.

## Installation depuis les sources

- Installer l'environnement Go: https://golang.org/dl/

- Cloner ce projet:
```shell script
$ git clone git@gitlab.ndp-systemes.fr:kubernetes/nestor.git
```

- Compiler nestor:
```shell script
$ go build
```
