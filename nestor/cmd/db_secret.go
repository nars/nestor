// Copyright 2021 NDP Systèmes. All Rights Reserved.
// See LICENSE file for full licensing details.

package cmd

import (
	"bufio"
	"fmt"
	"os"
	"strings"

	"gitlab.com/nars/nestor/pkg/nestor"

	"github.com/spf13/cobra"
)

var (
	dbHost     string
	dbPort     string
	dbUser     string
	dbPassword string
)

var dbSecretCmd = &cobra.Command{
	Use:   "secret",
	Short: "Gestion des secrets de connexion aux bases de données",
}

var dbSecretListCmd = &cobra.Command{
	Use:     "list",
	Aliases: []string{"ls"},
	Short:   "Liste les secrets de connexion aux bases de données",
	Long:    `Liste les différents secrets de connexion aux bases de données configurés dans ce cluster.`,
	Args:    cobra.NoArgs,
	Run: func(cmd *cobra.Command, args []string) {
		cfg, _ := getConfig()
		secrets, err := nestor.DBSecretList(cfg)
		checkError(err)
		for _, secret := range secrets {
			fmt.Println(secret)
		}
	},
}

var dbSecretNewCmd = &cobra.Command{
	Use:     "new <secret>",
	Aliases: []string{"add", "create"},
	Short:   "Crée un nouveau secret de connexion à une base de données",
	Long: `Crée la configuration d'accès à une nouvelle base de données. Vous devez spécifier les flags de configuration:

	nestor database secret new my_database --host=myhost.example.com --port=5433 --user=myuser --password=secret`,
	Args: cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		cfg, _ := getConfig()
		data := nestor.DatabaseConnexion{
			Name:     args[0],
			Host:     dbHost,
			Port:     dbPort,
			User:     dbUser,
			Password: dbPassword,
		}
		checkError(nestor.DBSecretNew(cfg, &data))
		fmt.Printf("Secret de base de données '%s' configuré.\n", args[0])
	},
}

var dbSecretDeleteCmd = &cobra.Command{
	Use:     "delete <secret>",
	Aliases: []string{"remove", "rm"},
	Short:   "Supprime le secret d'une connexion à une base de données",
	Long: `Supprime la configuration  d'une connexion à une base de données.
Cette commande ne supprime pas la base de données.`,
	Args: cobra.ExactArgs(1),
	ValidArgsFunction: func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
		var res []string
		cfg, _ := getConfig()
		secrets, err := nestor.DBSecretList(cfg)
		checkError(err)
		for _, secret := range secrets {
			if strings.HasPrefix(secret, toComplete) {
				res = append(res, secret)
			}
		}
		return res, cobra.ShellCompDirectiveNoFileComp
	},
	Run: func(cmd *cobra.Command, args []string) {
		cfg, _ := getConfig()
		_, err := nestor.DBSecretGet(cfg, args[0])
		checkError(err)
		fmt.Println("La suppression du secret de base de données ne supprime pas la base de données.")
		fmt.Printf("Êtes-vous sûr de vouloir supprimer le secret de base de données '%s' ? [o/N] ", args[0])
		scanner := bufio.NewScanner(os.Stdin)
		scanner.Scan()
		if strings.ToUpper(scanner.Text()) == "O" || strings.ToUpper(scanner.Text()) == "Y" {
			checkError(nestor.DBSecretDelete(cfg, args[0]))
			fmt.Printf("Secret de base de données '%s' supprimée.\n", args[0])
			fmt.Println("La base de données n'a pas été supprimée.")
			os.Exit(0)
		}
		fmt.Println("Suppression annulée.")
	},
}

func init() {
	dbSecretNewCmd.Flags().StringVar(&dbHost, "host", "", "Serveur de la base de données")
	checkError(dbSecretNewCmd.MarkFlagRequired("host"))
	dbSecretNewCmd.Flags().StringVar(&dbPort, "port", "5432", "Port de la base de données")
	dbSecretNewCmd.Flags().StringVar(&dbUser, "user", "", "Nom d'utilisateur de la base de données")
	checkError(dbSecretNewCmd.MarkFlagRequired("user"))
	dbSecretNewCmd.Flags().StringVar(&dbPassword, "password", "", "Mot de passe de connexion à la base de données")
	checkError(dbSecretNewCmd.MarkFlagRequired("password"))

	dbSecretCmd.AddCommand(dbSecretListCmd)
	dbSecretCmd.AddCommand(dbSecretNewCmd)
	dbSecretCmd.AddCommand(dbSecretDeleteCmd)
}
