// Copyright 2021 NDP Systèmes. All Rights Reserved.
// See LICENSE file for full licensing details.

package cmd

import (
	"bufio"
	"fmt"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/nars/nestor/pkg/kube"
	corev1 "k8s.io/api/core/v1"
	"k8s.io/cli-runtime/pkg/genericclioptions"
	"k8s.io/client-go/kubernetes/scheme"
	"k8s.io/client-go/tools/clientcmd/api/latest"
)

var (
	standardUserBindings = []corev1.ObjectReference{
		{
			Kind: "RoleBinding",
			Name: "odoo-manager-role",
		},
		{
			Kind: "ClusterRoleBinding",
			Name: "odoo-manager-clusterrole",
		},
		{
			Kind: "ClusterRoleBinding",
			Name: "lens-read-metrics",
		},
		{
			Kind: "ClusterRoleBinding",
			Name: "lens-prometheus",
		},
	}
)

var userCmd = &cobra.Command{
	Use:     "user",
	Aliases: []string{"users"},
	Short:   "Gérer les utilisateurs",
}

var userListCmd = &cobra.Command{
	Use:     "list",
	Aliases: []string{"ls"},
	Short:   "Liste les utilisateurs",
	Long:    `Liste les utilisateurs du namespace`,
	Args:    cobra.NoArgs,
	Run: func(cmd *cobra.Command, args []string) {
		cfg, _ := getConfig()
		users, err := kube.ListUsers(cfg)
		checkError(err)
		for _, user := range users {
			fmt.Println(user)
		}
	},
}

var userShowCmd = &cobra.Command{
	Use:     "show <username>",
	Aliases: []string{"get"},
	Short:   "Affiche les données d'un utilisateur",
	Long:    `Affiche les données d'un utilisateur.`,
	Args:    cobra.ExactArgs(1),
	ValidArgsFunction: func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
		cfg, _ := getConfig()
		res, err := kube.ListUsers(cfg)
		checkError(err)
		return res, cobra.ShellCompDirectiveDefault
	},
	Run: func(cmd *cobra.Command, args []string) {
		cfg, prod := getConfig()
		kc, err := kube.GetUserKubeConfig(cfg, args[0], cfg.ClusterName, prod)
		checkError(err)
		printer, err := genericclioptions.NewPrintFlags("").WithTypeSetter(scheme.Scheme).WithDefaultOutput("yaml").ToPrinter()
		checkError(err)
		convCfg, err := latest.Scheme.ConvertToVersion(kc, latest.ExternalVersion)
		checkError(err)
		err = printer.PrintObj(convCfg, os.Stdout)
		checkError(err)
	},
}

var userNewCmd = &cobra.Command{
	Use:     "new <username>",
	Aliases: []string{"add", "create"},
	Short:   "Crée un nouvel utilisateur",
	Long:    `Crée un nouvel utilisateur`,
	Args:    cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		cfg, _ := getConfig()
		err := kube.AddUser(cfg, args[0], standardUserBindings)
		checkError(err)
	},
}

var userDeleteCmd = &cobra.Command{
	Use:     "delete <username>",
	Aliases: []string{"remove"},
	Short:   "Supprime un utilisateur",
	Long:    `Supprime l'utilisateur spécifié'`,
	Args:    cobra.ExactArgs(1),
	ValidArgsFunction: func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
		cfg, _ := getConfig()
		res, err := kube.ListUsers(cfg)
		checkError(err)
		return res, cobra.ShellCompDirectiveDefault
	},
	Run: func(cmd *cobra.Command, args []string) {
		cfg, _ := getConfig()
		fmt.Printf("Êtes-vous sûr de vouloir supprimer l'utilisateur '%s' ? [o/N] ", args[0])
		scanner := bufio.NewScanner(os.Stdin)
		scanner.Scan()
		if strings.ToUpper(scanner.Text()) == "O" || strings.ToUpper(scanner.Text()) == "Y" {
			checkError(kube.DeleteUser(cfg, args[0], standardUserBindings))
			fmt.Printf("Utilisateur '%s' supprimé.\n", args[0])
			os.Exit(0)
		}
		fmt.Println("Suppression annulée.")
	},
}

func init() {
	userCmd.AddCommand(userListCmd)
	userCmd.AddCommand(userShowCmd)
	userCmd.AddCommand(userNewCmd)
	userCmd.AddCommand(userDeleteCmd)
}
