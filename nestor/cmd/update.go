// Copyright 2020 NDP Systèmes. All Rights Reserved.
// See LICENSE file for full licensing details.

package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/nars/nestor/pkg/nestor"
)

var updateCmd = &cobra.Command{
	Use:   "update <instance> <module>|all",
	Short: "Met à jour un module Odoo",
	Long: `Fait un pull sur les sources Odoo puis met à jour le module Odoo spécifié.
Par défaut, le code source des modules sera à nouveau téléchargé.
Vous pouvez forcer la mise à jour sans faire ce téléchargement avec le flag --no-pull.`,
	Args: cobra.ExactArgs(2),
	ValidArgsFunction: func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
		cfg, _ := getConfig()
		var res []string
		switch len(args) {
		case 0:
			res = getInstanceList(cfg, toComplete)
		}
		return res, cobra.ShellCompDirectiveNoFileComp
	},
	Run: func(cmd *cobra.Command, args []string) {
		cfg, _ := getConfig()
		instance, err := nestor.InstanceByName(cfg, args[0])
		checkError(err)
		err = instance.UpdateModule(args[1])
		checkError(err)
		fmt.Printf("L'instance '%s' est en cours de redémarrage.\n", instance.Name())
	},
}
