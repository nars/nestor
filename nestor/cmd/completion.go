// Copyright 2020 NDP Systèmes. All Rights Reserved.
// See LICENSE file for full licensing details.

package cmd

import (
	"io"
	"os"

	"github.com/spf13/cobra"
)

var completionFile string

var completionCmd = &cobra.Command{
	Use: "completion bash|zsh",
	Short: "Crée les fichiers d'autocomplétion pour bash ou zsh",
	Long: `Crée les fichiers d'autocomplétion pour bash ou zsh.

BASH:
	sudo nestor completion bash -o /etc/bash_completion.d/nestor

ZSH:

	mkdir ~/.oh-my-zsh/custom/plugins/nestor && nestor completion zsh -o ~/.oh-my-zsh/custom/plugins/nestor/_nestor

puis ajouter le plugin 'nestor' à la fin de la liste des plugins dans ~/.zshrc
`,
	Args: cobra.ExactArgs(1),
	ValidArgs: []string{"bash", "zsh"},
	Run: func(cmd *cobra.Command, args []string) {
		var (
			err error
			out io.Writer
		)
		out = os.Stdout
		if completionFile != "" {
			out, err = os.Create(completionFile)
			checkError(err)
		}

		switch args[0] {
		case "bash":
			err = RootCmd.GenBashCompletion(out)
		case "zsh":
			err = RootCmd.GenZshCompletion(out)
		}
		checkError(err)
	},
}

func init() {
	completionCmd.Flags().StringVarP(&completionFile, "output", "o", "", "Fichier dans lequel écrire les commandes de complétion (stdout par défaut)")
}
