// Copyright 2020 NDP Systèmes. All Rights Reserved.
// See LICENSE file for full licensing details.

package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/nars/nestor/pkg/nestor"
)

var startCmd = &cobra.Command{
	Use:   "start <instance>",
	Short: "Démarre les pods de l'instance",
	Long: `Démarre les pods Odoo de l'instance donnée en passant les répliques au nombre d'instance dans la configuration'.

Si l'instance est déjà démarrée, start ne fait rien de plus.
`,
	Args: cobra.ExactArgs(1),
	ValidArgsFunction: func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
		cfg, _ := getConfig()
		res := getInstanceList(cfg, toComplete)
		return res, cobra.ShellCompDirectiveDefault
	},
	Run: func(cmd *cobra.Command, args []string) {
		cfg, _ := getConfig()
		instance, err := nestor.InstanceByName(cfg, args[0])
		checkError(err)
		err = instance.ScaleOdooUp()
		checkError(err)
		fmt.Printf("Le serveur Odoo de l'instance '%s' est en cours de démarrage.\n", instance.Name())
	},
}
