// Copyright 2020 NDP Systèmes. All Rights Reserved.
// See LICENSE file for full licensing details.

package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/nars/nestor/pkg/nestor"
)

var stopCmd = &cobra.Command{
	Use:   "stop <instance>",
	Short: "Stoppe les pods de l'instance",
	Long: `Stoppe les pods Odoo de l'instance donnée en réduisant les répliques à 0.
La base de donnée reste active.
`,
	Args: cobra.ExactArgs(1),
	ValidArgsFunction: func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
		cfg, _ := getConfig()
		res := getInstanceList(cfg, toComplete)
		return res, cobra.ShellCompDirectiveDefault
	},
	Run: func(cmd *cobra.Command, args []string) {
		cfg, _ := getConfig()
		instance, err := nestor.InstanceByName(cfg, args[0])
		checkError(err)
		err = instance.ScaleOdooToZero()
		checkError(err)
		fmt.Printf("Le serveur Odoo de l'instance '%s' est en cours d'arrêt.\n", instance.Name())
	},
}
