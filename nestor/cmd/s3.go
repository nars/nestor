// Copyright 2021 NDP Systèmes. All Rights Reserved.
// See LICENSE file for full licensing details.

package cmd

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/nars/nestor/pkg/s3"
)

var (
	s3AccessKey string
	s3SecretKey string
	s3Host      string
	s3Region    string
)

var s3Cmd = &cobra.Command{
	Use:   "s3",
	Short: "Gestion des services S3",
}

var s3ListCmd = &cobra.Command{
	Use:     "list",
	Aliases: []string{"ls"},
	Short:   "Liste les services S3",
	Long:    `Liste les différents services S3 configurés dans ce cluster.`,
	Args:    cobra.NoArgs,
	Run: func(cmd *cobra.Command, args []string) {
		cfg, _ := getConfig()
		secrets, err := s3.List(cfg)
		checkError(err)
		for _, secret := range secrets {
			fmt.Println(secret)
		}
	},
}

var s3NewCmd = &cobra.Command{
	Use:     "new <service>",
	Aliases: []string{"add", "create"},
	Short:   "Crée un nouveau service S3",
	Long: `Crée la configuration d'accès à un nouveau service S3. Vous devez spécifier les 4 flags de configuration:

	nestor s3 new mon_s3 --access-key=yyyy --secret-key=xxxx --host=s3.fr-par.scw.cloud --region=fr-par`,
	Args: cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		cfg, _ := getConfig()
		data := s3.ServiceData{
			Name:      args[0],
			AccessKey: s3AccessKey,
			SecretKey: s3SecretKey,
			Host:      s3Host,
			Region:    s3Region,
		}
		checkError(s3.New(cfg, &data))
		fmt.Printf("Service S3 '%s' configuré.\n", args[0])
	},
}

var s3DeleteCmd = &cobra.Command{
	Use:     "delete <service>",
	Aliases: []string{"remove", "rm"},
	Short:   "Supprime la configuration d'un service S3",
	Long: `Supprime la configuration du service S3 spécifié.
Cette commande ne supprime pas les données stockées sur le service.`,
	Args: cobra.ExactArgs(1),
	ValidArgsFunction: func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
		var res []string
		cfg, _ := getConfig()
		secrets, err := s3.List(cfg)
		checkError(err)
		for _, secret := range secrets {
			if strings.HasPrefix(secret, toComplete) {
				res = append(res, secret)
			}
		}
		return res, cobra.ShellCompDirectiveNoFileComp
	},
	Run: func(cmd *cobra.Command, args []string) {
		cfg, _ := getConfig()
		_, err := s3.Get(cfg, args[0])
		checkError(err)
		if args[0] == s3DefaultService {
			checkError(errors.New("il n'est pas permis de supprimer le service par défaut"))
		}
		fmt.Println("La suppression de la configuration S3 ne supprime pas les données.")
		fmt.Printf("Êtes-vous sûr de vouloir supprimer la configuration S3 '%s' ? [o/N] ", args[0])
		scanner := bufio.NewScanner(os.Stdin)
		scanner.Scan()
		if strings.ToUpper(scanner.Text()) == "O" || strings.ToUpper(scanner.Text()) == "Y" {
			checkError(s3.Delete(cfg, args[0]))
			fmt.Printf("Configuration du service S3 '%s' supprimée.\n", args[0])
			fmt.Println("Les données stockées sur le service n'ont pas été supprimées.")
			os.Exit(0)
		}
		fmt.Println("Suppression annulée.")
	},
}

func init() {
	s3NewCmd.Flags().StringVar(&s3AccessKey, "access-key", "", "Access key du service S3")
	checkError(s3NewCmd.MarkFlagRequired("access-key"))
	s3NewCmd.Flags().StringVar(&s3SecretKey, "secret-key", "", "Secret key du service S3")
	checkError(s3NewCmd.MarkFlagRequired("secret-key"))
	s3NewCmd.Flags().StringVar(&s3Host, "host", "", "Hôte du service S3 (sans https://)")
	checkError(s3NewCmd.MarkFlagRequired("host"))
	s3NewCmd.Flags().StringVar(&s3Region, "region", "", "Région du service S3.")
	checkError(s3NewCmd.MarkFlagRequired("region"))

	s3Cmd.AddCommand(s3ListCmd)
	s3Cmd.AddCommand(s3NewCmd)
	s3Cmd.AddCommand(s3DeleteCmd)
}
