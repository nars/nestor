// Copyright 2020 NDP Systèmes. All Rights Reserved.
// See LICENSE file for full licensing details.

package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/nars/nestor/pkg/nestor"
)

var consoleOdooDB string

var consoleOdooCmd = &cobra.Command{
	Use:   "console-odoo <instance>",
	Short: "Lance la console Odoo",
	Long:  "Lance la console Odoo sur l'instance spécifiée",
	Args:  cobra.ExactArgs(1),
	ValidArgsFunction: func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
		cfg, _ := getConfig()
		res := getInstanceList(cfg, toComplete)
		return res, cobra.ShellCompDirectiveDefault
	},
	Run: func(cmd *cobra.Command, args []string) {
		cfg, _ := getConfig()
		instance, err := nestor.InstanceByName(cfg, args[0])
		checkError(err)
		err = instance.Console(consoleOdooDB)
		checkError(err)
	},
}

func init() {
	consoleOdooCmd.Flags().StringVarP(&consoleOdooDB, "database", "d", "",
		"Nom de la base de données à utiliser. Si la valeur n'est pas définie, le nom de l'instance sera utilisé.")
}
