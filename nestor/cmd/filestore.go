// Copyright 2021 NDP Systèmes. All Rights Reserved.
// See LICENSE file for full licensing details.

package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/nars/nestor/pkg/nestor"
	"gitlab.com/nars/nestor/pkg/s3"
)

var (
	fsS3       string
	fsBucket   string
	fsDatabase string
	fsNoRename bool
	fsProgress bool
)

var filestoreCmd = &cobra.Command{
	Use:     "filestore",
	Aliases: []string{"fs"},
	Short:   "Gestion des fichiers du filestore",
}

var filestoreDumpCmd = &cobra.Command{
	Use:     "dump <instance> (local|s3) <dossier>",
	Aliases: []string{"backup"},
	Short:   "Dump le filestore de l'instance",
	Long: `Dump le filestore de l'instance spécifiée.
Le second argument est la destination du dossier de dump qui peut être:
- local: Le dump va être sauvegardé sur votre disque
- s3: Le dump va être sauvegardé sur le S3

Le troisième argument est le nom du dossier où mettre le filestore.
Il n'est pas nécessaire que le dossier existe.

EXEMPLES:
	nestor filestore dump my-instance local path-to/filestore
	nestor filestore dump my-instance s3 path-to/filestore
`,
	Args: cobra.ExactArgs(3),
	ValidArgsFunction: func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
		cfg, _ := getConfig()
		var (
			res    []string
			shComp cobra.ShellCompDirective
		)
		switch len(args) {
		case 0:
			res = getInstanceList(cfg, toComplete)
			shComp = cobra.ShellCompDirectiveNoFileComp
		case 1:
			res = []string{"local", "s3"}
			shComp = cobra.ShellCompDirectiveNoFileComp
		case 2:
			shComp = cobra.ShellCompDirectiveDefault
		}
		return res, shComp
	},
	Run: func(cmd *cobra.Command, args []string) {
		cfg, _ := getConfig()
		instance, err := nestor.InstanceByName(cfg, args[0])
		checkError(err)
		switch args[1] {
		case "local":
			err = instance.FSLocalDump(args[2], fsProgress)
			checkError(err)
		case "s3":
			s3Name := s3DefaultService
			if fsS3 != "" {
				s3Name = fsS3
			}
			s3Service, err := s3.Get(cfg, s3Name)
			checkError(err)
			err = instance.FSS3Dump(s3Service, fsBucket, args[2], fsProgress)
			checkError(err)
		default:
			checkError(fmt.Errorf("%s: destination inconnue", args[1]))
		}
	},
}

var filestoreRestoreCmd = &cobra.Command{
	Use:   "restore <instance> (local|s3) <dossier>",
	Short: "Restaure le filestore de l'instance",
	Long: `Restaure le filestore de l'instance spécifiée.
Le second argument est la provenance du dossier de dump qui peut être:
- local: Le dossier à restaurer est sur votre disque
- s3: Le dossier à restaurer est sur le S3

Le troisième argument est le nom du dossier du filestore à restaurer.

EXEMPLES:
	nestor filestore restore my-instance local path-to/filestore
	nestor filestore restore my-instance s3 path-to/filestore

NOTES:
"nestor filestore restore" va renommer le dossier de la base de données contenu dans le dossier filestore.
Passez le flag --database pour spécifier un autre nom de base de données que celui de l'instance.
Si le dossier du filestore contient plusieurs dossiers (correspondant à plusieurs bases de données), ou si le flag
--no-rename est spécifié, alors le renommage n'est pas effectué.
`,
	Args: cobra.ExactArgs(3),
	ValidArgsFunction: func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
		cfg, _ := getConfig()
		var (
			res    []string
			shComp cobra.ShellCompDirective
		)
		switch len(args) {
		case 0:
			res = getInstanceList(cfg, toComplete)
			shComp = cobra.ShellCompDirectiveNoFileComp
		case 1:
			res = []string{"local", "s3"}
			shComp = cobra.ShellCompDirectiveNoFileComp
		case 2:
			shComp = cobra.ShellCompDirectiveDefault
		}
		return res, shComp
	},
	Run: func(cmd *cobra.Command, args []string) {
		cfg, _ := getConfig()
		instance, err := nestor.InstanceByName(cfg, args[0])
		checkError(err)
		switch args[1] {
		case "local":
			err = instance.FSLocalRestore(args[2], fsDatabase, fsNoRename, fsProgress)
			checkError(err)
		case "s3":
			s3Name := s3DefaultService
			if fsS3 != "" {
				s3Name = fsS3
			}
			s3Service, err := s3.Get(cfg, s3Name)
			checkError(err)
			err = instance.FSS3Restore(s3Service, fsBucket, args[2], fsDatabase, fsNoRename, fsProgress)
			checkError(err)
		default:
			checkError(fmt.Errorf("%s: destination inconnue", args[1]))
		}
	},
}

func init() {
	filestoreCmd.AddCommand(filestoreDumpCmd)
	filestoreCmd.AddCommand(filestoreRestoreCmd)

	filestoreRestoreCmd.Flags().StringVar(&fsDatabase, "database", "", "Nom de la base de donnée s'il est différent du nom de l'instance")
	filestoreRestoreCmd.Flags().BoolVar(&fsNoRename, "no-rename", false, "Ne pas renommer le dossier de la base de données")

	filestoreCmd.PersistentFlags().BoolVarP(&fsProgress, "progress", "P", false, "Affiche une barre de progression du transfert")
	filestoreCmd.PersistentFlags().StringVar(&fsS3, "s3", s3DefaultService, "Service S3 à utiliser")
	filestoreCmd.PersistentFlags().StringVar(&fsBucket, "bucket", s3DefaultBucket, "Bucket à utiliser pour le dump s3")
	err := filestoreCmd.RegisterFlagCompletionFunc("bucket", func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
		return s3WriteAllowedBuckets, cobra.ShellCompDirectiveNoFileComp
	})
	checkError(err)
}
