// Copyright 2020 NDP Systèmes. All Rights Reserved.
// See LICENSE file for full licensing details.

package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/nars/nestor/pkg/nestor"
)

var (
	logsTail       int64
	logsFollow     bool
	logsPostgresql bool
)

var logsCmd = &cobra.Command{
	Use:   "logs <instance>",
	Short: "Affiche les logs d'une instance",
	Long: `Affiche les logs de l'instance spécifiée. 
Par défaut, logs affiche les logs Odoo. 
Pour avoir les logs de postgresql, utilisez --postgresql.`,
	Args: cobra.ExactArgs(1),
	ValidArgsFunction: func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
		cfg, _ := getConfig()
		res := getInstanceList(cfg, toComplete)
		return res, cobra.ShellCompDirectiveDefault
	},
	Run: func(cmd *cobra.Command, args []string) {
		cfg, _ := getConfig()
		instance, err := nestor.InstanceByName(cfg, args[0])
		checkError(err)
		err = instance.Logs(logsPostgresql, logsFollow, logsTail)
		checkError(err)
	},
}

func init() {
	logsCmd.Flags().BoolVarP(&logsFollow, "follow", "f", false, "Affiche les logs en continu.")
	logsCmd.Flags().Int64VarP(&logsTail, "tail", "t", -1, "Nombre de ligne à afficher depuis la fin du log.")
	logsCmd.Flags().BoolVar(&logsPostgresql, "postgresql", false, "Execute la commande sur le pod postgresql au lieu de ceux d'Odoo.")
}
