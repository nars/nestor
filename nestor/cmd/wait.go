// Copyright 2020 NDP Systèmes. All Rights Reserved.
// See LICENSE file for full licensing details.

package cmd

import (
	"errors"
	"fmt"
	"time"

	"github.com/spf13/cobra"
	"gitlab.com/nars/nestor/pkg/nestor"
)

var (
	timeout        int64
	waitPostgresql bool
)

var waitCmd = &cobra.Command{
	Use:   "wait <instance> (up|down)",
	Short: "Attends que l'instance soit dans l'état spécifié",
	Long: `Attends que l'instance donnée soit dans l'état spécifié, c'est-à-dire soit "up" soit "down".
Par défaut, seul l'état des pods Odoo sont regardés. 

Si le flag --postgresql est donné:
- up attendra simplement que le pod postgresql soit démarré.
- down attendra que les pods Odoo et le pod postgresql soit arrêté.`,
	Args: func(cmd *cobra.Command, args []string) error {
		if len(args) != 2 {
			return fmt.Errorf("accepts %d arg(s), received %d", 2, len(args))
		}
		if args[1] != "up" && args[1] != "down" {
			return errors.New("l'état attendu doit être 'up' ou 'down'")
		}
		return nil
	},
	ValidArgsFunction: func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
		cfg, _ := getConfig()
		var (
			res    []string
			shComp cobra.ShellCompDirective
		)
		switch len(args) {
		case 0:
			res = getInstanceList(cfg, toComplete)
			shComp = cobra.ShellCompDirectiveNoFileComp
		case 1:
			res = []string{"up", "down"}
			shComp = cobra.ShellCompDirectiveNoFileComp
		}
		return res, shComp
	},
	Run: func(cmd *cobra.Command, args []string) {
		cfg, _ := getConfig()
		instance, err := nestor.InstanceByName(cfg, args[0])
		checkError(err)
		to := time.Duration(timeout) * time.Second
		switch args[1] {
		case "up":
			err = instance.WaitForUp(to, waitPostgresql)
		case "down":
			err = instance.WaitForDown(to, waitPostgresql)
		}
		checkError(err)
	},
}

func init() {
	waitCmd.Flags().Int64Var(&timeout, "timeout", 120, "Timeout en secondes pour l'attente")
	waitCmd.Flags().BoolVar(&waitPostgresql, "postgresql", false, "Execute la commande sur le pod postgresql au lieu de ceux d'Odoo.")
}
