// Copyright 2020 NDP Systèmes. All Rights Reserved.
// See LICENSE file for full licensing details.

package cmd

import (
	"errors"
	"fmt"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/nars/nestor/pkg/nestor"
)

var cpPostgresql bool

var cpCmd = &cobra.Command{
	Use:   "cp [<instance>:]<src> [<instance>:]<dest>",
	Short: "Copie un fichier depuis ou vers une instance",
	Long: `Copie un fichier depuis ou vers une instance.
Exemples:
	nestor cp foo:/bar.txt baz.txt
	nestor cp bar.txt foo:/baz.txt

Vous devez spécifier l'instance sur la source ou la destination, et un chemin local sur l'autre.`,
	Args: cobra.ExactArgs(2),
	Run: func(cmd *cobra.Command, args []string) {
		cfg, _ := getConfig()
		instanceName, src, dst, toInstance := getCpData(args)
		instance, err := nestor.InstanceByName(cfg, instanceName)
		checkError(err)
		err = instance.Copy(src, dst, toInstance, cpPostgresql)
	},
}

func getCpData(args []string) (string, string, string, bool) {
	srcToks := strings.Split(args[0], ":")
	dstToks := strings.Split(args[1], ":")
	if len(srcToks) == 1 && len(dstToks) == 1 {
		checkError(errors.New("vous devez spécifier une instance en source ou en destination"))
	}
	if len(srcToks) > 2 {
		checkError(fmt.Errorf("source '%s' malformée", args[0]))
	}
	if len(dstToks) > 2 {
		checkError(fmt.Errorf("destination '%s' malformée", args[1]))
	}
	if len(srcToks) == 2 && len(dstToks) == 2 {
		checkError(errors.New("vous ne pouvez spécifier une instance qu'en source ou qu'en destination"))
	}
	if len(srcToks) == 2 {
		return srcToks[0], srcToks[1], dstToks[0], false
	}
	return dstToks[0], srcToks[0], dstToks[1], true
}

func init() {
	cpCmd.Flags().BoolVar(&cpPostgresql, "postgresql", false, "Execute la commande sur le pod postgresql au lieu de ceux d'Odoo.")
}
