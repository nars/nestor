// Copyright 2020 NDP Systèmes. All Rights Reserved.
// See LICENSE file for full licensing details.

package cmd

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"sort"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/nars/nestor/pkg/kube"
)

var (
	configAddFile string
)

var configCmd = &cobra.Command{
	Use:   "config",
	Short: "Configuration de l'accès aux clusters",
}

var configListCmd = &cobra.Command{
	Use:   "list",
	Short: "Liste les clusters configurés",
	Long:  `Liste les clusters configurés dans le fichier .kube/config`,
	Args:  cobra.NoArgs,
	Run: func(cmd *cobra.Command, args []string) {
		contexts, err := kube.ListConfigs()
		checkError(err)
		sort.Slice(contexts, func(i, j int) bool {
			return contexts[i].Name < contexts[j].Name
		})
		for _, ctx := range contexts {
			star := " "
			if ctx.Current {
				star = "*"
			}
			prod := "    "
			if ctx.Production() {
				prod = "PROD"
			}
			fmt.Printf("%s %s %s\n", star, prod, ctx.Name)
		}
	},
}

var configSelectCmd = &cobra.Command{
	Use:   "select <config>",
	Short: "Sélectionne le cluster courant",
	Long: `Sélectionne le cluster courant pour les opérations de nestor.

Notez que cette commande ne permet pas de sélectionner le cluster de production.
Pour les opérations de production, vous devez utiliser le flag --production avec chaque commande.`,
	Args: cobra.ExactArgs(1),
	ValidArgsFunction: func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
		if len(args) > 0 {
			return nil, cobra.ShellCompDirectiveDefault
		}
		return getConfigList(false), cobra.ShellCompDirectiveNoFileComp
	},
	Run: func(cmd *cobra.Command, args []string) {
		err := kube.SelectConfig(args[0])
		checkError(err)
	},
}

var configAddCmd = &cobra.Command{
	Use:     "new",
	Aliases: []string{"add", "create"},
	Short:   "Crée/modifie la configuration Kubernetes",
	Long: `Crée ou modifie la configuration Kubernetes utilisée. 
Si vous spécifiez un fichier kubeconfig avec --config, les données du fichier seront utilisées.
Dans le cas contraire, il vous sera demandé de copier/coller le contenu du fichier.

ATTENTION: cette commande change la configuration générale de kubectl dans ~/.kube/config.`,
	Args: cobra.NoArgs,
	Run: func(cmd *cobra.Command, args []string) {
		var (
			err  error
			data []byte
		)
		switch configAddFile {
		case "":
			fmt.Println("Veuillez copier/coller le contenu du fichier kubeconfig, puis terminez par Ctrl-D")
			data, err = io.ReadAll(os.Stdin)
			checkError(err)
		default:
			data, err = os.ReadFile(configAddFile)
			checkError(err)
		}
		err = kube.AddConfig(data)
		checkError(err)
	},
}

var configRemoveCmd = &cobra.Command{
	Use:     "delete <config>",
	Aliases: []string{"rm", "remove"},
	Short:   "Supprime la configuration Kubernetes spécifiée",
	Long: `Supprime la configuration Kubernetes spécifiée.

ATTENTION: cette commande change la configuration générale de kubectl dans ~/.kube/config.`,
	Args: cobra.ExactArgs(1),
	ValidArgsFunction: func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
		if len(args) > 0 {
			return nil, cobra.ShellCompDirectiveDefault
		}
		return getConfigList(false), cobra.ShellCompDirectiveNoFileComp
	},
	Run: func(cmd *cobra.Command, args []string) {
		err := kube.RemoveConfig(args[0])
		checkError(err)
	},
}

var configRenameCmd = &cobra.Command{
	Use:   "rename <config> <new name>",
	Short: "Renomme une configuration Kubernetes",
	Long: `Renomme la configuration Kubernetes spécifiée.

ATTENTION: cette commande change la configuration générale de kubectl dans ~/.kube/config.`,
	Args: cobra.ExactArgs(2),
	ValidArgsFunction: func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
		if len(args) > 0 {
			return nil, cobra.ShellCompDirectiveDefault
		}
		return getConfigList(true), cobra.ShellCompDirectiveNoFileComp
	},
	Run: func(cmd *cobra.Command, args []string) {
		err := kube.RenameConfig(args[0], args[1])
		checkError(err)
	},
}

var configSetProductionCmd = &cobra.Command{
	Use:   "set-production <config>",
	Short: "Définit la configuration donnée comme étant une configuration de production",
	Long: `Définit la configuration donnée comme étant une configuration de production.

ATTENTION: cette commande change la configuration générale de kubectl dans ~/.kube/config.`,
	Args: cobra.ExactArgs(1),
	ValidArgsFunction: func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
		if len(args) > 0 {
			return nil, cobra.ShellCompDirectiveDefault
		}
		return getConfigList(false), cobra.ShellCompDirectiveNoFileComp
	},
	Run: func(cmd *cobra.Command, args []string) {
		err := kube.SetContextTypeProduction(args[0], true)
		checkError(err)
	},
}

var configUnsetProductionCmd = &cobra.Command{
	Use:   "unset-production <config>",
	Short: "Définit la configuration donnée comme n'étant plus une configuration de production",
	Long: `Définit la configuration donnée comme n'étant plus une configuration de production.

ATTENTION: 
- cette commande est potentiellement dangereuse car elle retire la sécurité sur les productions.
- cette commande change la configuration générale de kubectl dans ~/.kube/config.`,
	Args: cobra.ExactArgs(1),
	ValidArgsFunction: func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
		if len(args) > 0 {
			return nil, cobra.ShellCompDirectiveDefault
		}
		return getConfigList(false), cobra.ShellCompDirectiveNoFileComp
	},
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println("Attention, cette commande va supprimer la sécurité de production sur la configuration", args[0])
		fmt.Print("Êtes-vous sûre de vouloir continuer ? [o/N] ")
		scanner := bufio.NewScanner(os.Stdin)
		scanner.Scan()
		if strings.ToUpper(scanner.Text()) == "O" || strings.ToUpper(scanner.Text()) == "Y" {
			err := kube.SetContextTypeProduction(args[0], false)
			checkError(err)
		}
		fmt.Println("Opération annulée")
	},
}

func init() {
	configAddCmd.Flags().StringVarP(&configAddFile, "config", "c", "", "Fichier kubeconfig de l'utilisateur.")
	configCmd.AddCommand(configListCmd)
	configCmd.AddCommand(configSelectCmd)
	configCmd.AddCommand(configAddCmd)
	configCmd.AddCommand(configRemoveCmd)
	configCmd.AddCommand(configRenameCmd)
	configCmd.AddCommand(configSetProductionCmd)
	configCmd.AddCommand(configUnsetProductionCmd)
}
