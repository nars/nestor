// Copyright 2021 NDP Systèmes. All Rights Reserved.
// See LICENSE file for full licensing details.

package cmd

import (
	"fmt"
	"os"
	"strconv"

	"gitlab.com/nars/nestor/pkg/kube"

	"github.com/spf13/cobra"
	"gitlab.com/nars/nestor/pkg/clean"
	"gitlab.com/nars/nestor/pkg/nestor"
	"gitlab.com/nars/nestor/pkg/s3"
)

var (
	dbS3                      string
	dbBucket                  string
	dbDatabase                string
	dbDumpVerbose             bool
	dbRestoreVerbose          bool
	dbRestoreNoClean          bool
	dbRestoreNoResetPasswords bool
	dbRestoreSetAllUsers      bool
)

var databaseCmd = &cobra.Command{
	Use:     "database",
	Aliases: []string{"db"},
	Short:   "Opérations sur la base de données",
}

var databaseDumpCmd = &cobra.Command{
	Use:     "dump <instance> (local|s3) <fichier>",
	Aliases: []string{"backup"},
	Short:   "Dump la base de données de l'instance",
	Long: `Dump la base de données de l'instance spécifiée.
Le second argument est la destination du dump qui peut être:
- local: Le dump va être sauvegardé sur votre disque
- s3: Le dump va être sauvegardé sur le S3

Exemples:
	nestor database dump my-instance local path-to/db.dump
	nestor database dump my-instance s3 path-to/db.dump
`,
	Args: cobra.ExactArgs(3),
	ValidArgsFunction: func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
		cfg, _ := getConfig()
		var (
			res    []string
			shComp cobra.ShellCompDirective
		)
		switch len(args) {
		case 0:
			res = getInstanceList(cfg, toComplete)
			shComp = cobra.ShellCompDirectiveNoFileComp
		case 1:
			res = []string{"local", "s3"}
			shComp = cobra.ShellCompDirectiveNoFileComp
		case 2:
			shComp = cobra.ShellCompDirectiveDefault
		}
		return res, shComp
	},
	Run: func(cmd *cobra.Command, args []string) {
		cfg, _ := getConfig()
		instance, err := nestor.InstanceByName(cfg, args[0])
		checkError(err)
		switch args[1] {
		case "local":
			err = instance.PgLocalDump(dbDatabase, args[2], dbDumpVerbose)
			checkError(err)
		case "s3":
			s3Name := s3DefaultService
			if dbS3 != "" {
				s3Name = dbS3
			}
			s3Service, err := s3.Get(cfg, s3Name)
			checkError(err)
			err = instance.PgS3Dump(s3Service, dbDatabase, dbBucket, args[2], dbDumpVerbose)
			checkError(err)
		default:
			checkError(fmt.Errorf("%s: destination inconnue", args[1]))
		}
	},
}

var databaseRestoreCmd = &cobra.Command{
	Use:   "restore <instance> (local|s3) <fichier>",
	Short: "Restaure la base de données de l'instance",
	Long: `Restaure et nettoie la base de données de l'instance spécifiée.
Le second argument est la provenance du dump qui peut être:
- local: Le dump à restaurer est sur votre disque
- s3: Le dump à restaurer est sur le S3

Exemples:
	nestor database restore my-instance local path/to/db.dump
	nestor database restore my-instance s3 path/to/db.dump

Notes:
- Cette commande va couper Odoo, supprimer la base de donnée et en recréer une nouvelle avec le dump.
- Cette commande nettoie automatiquement le dump, et change le mot de passe admin pour celui affiché avec 'nestor show', sauf si --no-clean est passé en paramètres.
`,
	Args: cobra.ExactArgs(3),
	ValidArgsFunction: func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
		cfg, _ := getConfig()
		var (
			res    []string
			shComp cobra.ShellCompDirective
		)
		switch len(args) {
		case 0:
			res = getInstanceList(cfg, toComplete)
			shComp = cobra.ShellCompDirectiveNoFileComp
		case 1:
			res = []string{"local", "s3"}
			shComp = cobra.ShellCompDirectiveNoFileComp
		case 2:
			shComp = cobra.ShellCompDirectiveDefault
		}
		return res, shComp
	},
	Run: func(cmd *cobra.Command, args []string) {
		cfg, _ := getConfig()
		instance, err := nestor.InstanceByName(cfg, args[0])
		checkError(err)
		options := clean.DBCleanOptions{
			NoResetPassword: dbRestoreNoResetPasswords,
			AllUsers:        dbRestoreSetAllUsers,
		}
		switch args[1] {
		case "local":
			err = instance.PgLocalRestore(dbDatabase, args[2], !dbRestoreNoClean, options, dbRestoreVerbose)
			checkError(err)
		case "s3":
			s3Name := s3DefaultService
			if dbS3 != "" {
				s3Name = dbS3
			}
			s3Service, err := s3.Get(cfg, s3Name)
			checkError(err)
			err = instance.PgS3Restore(s3Service, dbDatabase, dbBucket, args[2], !dbRestoreNoClean, options, dbRestoreVerbose)
			checkError(err)
		default:
			checkError(fmt.Errorf("%s: provenance inconnue", args[1]))
		}
		fmt.Println("Restore terminé. Odoo est en cours de redémarrage.")
	},
}

var databasePortCmd = &cobra.Command{
	Use:     "port <instance> <port_local>",
	Aliases: []string{"dbport"},
	Short:   "Forward du port de la base de donnée en local",
	Long: `Permet de faire un forward de port depuis le port de la base de donnée dans le pod vers le port local spécifié.

Exemple:
	nestor database port my-instance 2345

Il est ensuite possible de se connecter à la base de donnée de l'instance my-instance sur localhost:2345`,
	Args: cobra.ExactArgs(2),
	ValidArgsFunction: func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
		cfg, _ := getConfig()
		var res []string
		switch len(args) {
		case 0:
			res = getInstanceList(cfg, toComplete)
		}
		return res, cobra.ShellCompDirectiveNoFileComp
	},
	Run: func(cmd *cobra.Command, args []string) {
		cfg, _ := getConfig()
		instance, err := nestor.InstanceByName(cfg, args[0])
		checkError(err)
		if instance.CRD.Spec.Postgresql.Secret != "" {
			host, _ := kube.GetSecretValue(cfg, instance.CRD.Spec.Postgresql.Secret, "DB_HOST")
			port, _ := kube.GetSecretValue(cfg, instance.CRD.Spec.Postgresql.Secret, "DB_PORT")
			user, _ := kube.GetSecretValue(cfg, instance.CRD.Spec.Postgresql.Secret, "DB_USER")
			password, _ := kube.GetSecretValue(cfg, instance.CRD.Spec.Postgresql.Secret, "DB_PASSWORD")
			fmt.Println("`nestor database port` ne peut pas être utilisé sur une base de données externe.")
			fmt.Println("Connectez-vous directement à la base de données à la place:")
			fmt.Printf("    Connexion: %s@%s:%s \n", user, host, port)
			fmt.Println("    Mot de passe:", password)
			os.Exit(1)
		}
		port, err := strconv.Atoi(args[1])
		checkError(err)
		err = instance.DBPort(port)
		checkError(err)
	},
}

var databaseClientCmd = &cobra.Command{
	Use:     "client <instance>",
	Aliases: []string{"psql"},
	Short:   "Execute le client SQL",
	Long:    "Execute le client SQL sur la base de donnée de l'instance (psql)",
	Args:    cobra.ExactArgs(1),
	ValidArgsFunction: func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
		cfg, _ := getConfig()
		res := getInstanceList(cfg, toComplete)
		return res, cobra.ShellCompDirectiveDefault
	},
	Run: func(cmd *cobra.Command, args []string) {
		cfg, _ := getConfig()
		instance, err := nestor.InstanceByName(cfg, args[0])
		checkError(err)
		err = instance.Psql(dbDatabase)
		checkError(err)
	},
}

var databasePGActivityCmd = &cobra.Command{
	Use:   "pg_activity <instance>",
	Short: "Lance pg_activity",
	Long: `Lance pg_activity sur l'instance spécifiée.
Vous devez avoir pg_activity installé sur votre PC.`,
	Args: cobra.ExactArgs(1),
	ValidArgsFunction: func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
		cfg, _ := getConfig()
		res := getInstanceList(cfg, toComplete)
		return res, cobra.ShellCompDirectiveDefault
	},
	Run: func(cmd *cobra.Command, args []string) {
		cfg, _ := getConfig()
		instance, err := nestor.InstanceByName(cfg, args[0])
		checkError(err)
		err = instance.PgActivity()
		checkError(err)
	},
}

func init() {
	databaseCmd.AddCommand(databaseClientCmd)
	databaseCmd.AddCommand(databaseDumpCmd)
	databaseCmd.AddCommand(databasePGActivityCmd)
	databaseCmd.AddCommand(databasePortCmd)
	databaseCmd.AddCommand(databaseRestoreCmd)
	databaseCmd.AddCommand(dbSecretCmd)

	databaseCmd.PersistentFlags().StringVarP(&dbDatabase, "database", "d", "",
		"Nom de la base de données à utiliser. Si la valeur n'est pas définie, le nom de l'instance sera utilisé.")
	databaseCmd.PersistentFlags().StringVar(&dbS3, "s3", s3DefaultService, "Service S3 à utiliser")
	databaseCmd.PersistentFlags().StringVar(&dbBucket, "bucket", s3DefaultBucket, "Bucket à utiliser pour le dump s3")
	err := databaseCmd.RegisterFlagCompletionFunc("bucket", func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
		return s3WriteAllowedBuckets, cobra.ShellCompDirectiveNoFileComp
	})
	checkError(err)

	databaseDumpCmd.Flags().BoolVarP(&dbDumpVerbose, "verbose", "v", false,
		"Affiche les logs détaillés lors du dump")

	databaseRestoreCmd.Flags().BoolVar(&dbRestoreNoClean, "no-clean", false,
		"Ne pas nettoyer le dump lors de la restoration.")
	databaseRestoreCmd.Flags().BoolVar(&dbRestoreNoResetPasswords, "no-reset-passwords", false,
		"Ne modifie pas les mots de passe lors du nettoyage")
	databaseRestoreCmd.Flags().BoolVar(&dbRestoreSetAllUsers, "set-password-to-all", false,
		"Applique le mot de passe admin à tous les utilisateurs lors du nettoyage")
	databaseRestoreCmd.Flags().BoolVarP(&dbRestoreVerbose, "verbose", "v", false,
		"Affiche les logs détaillés lors de la restauration")
}
