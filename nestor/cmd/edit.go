// Copyright 2020 NDP Systèmes. All Rights Reserved.
// See LICENSE file for full licensing details.

package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/nars/nestor/pkg/nestor"
)

var (
	editSet         string
	editInteractive bool
)

var editCmd = &cobra.Command{
	Use:   "edit <instance>",
	Short: "Modifie la configuration de l'instance",
	Long: `Modifie les variables de configuration de l'instance. 
Si des variables ont été modifiées, l'instance est redémarrée 
automatiquement avec ces nouvelles variables`,
	Args: cobra.ExactArgs(1),
	ValidArgsFunction: func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
		cfg, _ := getConfig()
		res := getInstanceList(cfg, toComplete)
		return res, cobra.ShellCompDirectiveDefault
	},
	Run: func(cmd *cobra.Command, args []string) {
		cfg, _ := getConfig()
		vals := make(map[string]interface{})
		if editSet != "" {
			options, err := parseSetOptions(editSet)
			checkError(err)

			for _, opt := range options {
				nestor.SetValueInTable(opt.key, opt.value, &vals)
			}
		}
		instance, err := nestor.InstanceByName(cfg, args[0])
		checkError(err)
		err = instance.Edit(vals, editInteractive)
		checkError(err)
		fmt.Println("Configuration de l'instance modifiée.")
		fmt.Printf("L'instance '%s' est en cours de redémarrage.\n", args[0])
	},
}

func init() {
	editCmd.Flags().StringVar(&editSet, "set", "", "Spécifie les options depuis la ligne de commande séparées par des virgules.")
	editCmd.Flags().BoolVarP(&editInteractive, "interactive", "i", false, "Force l'ouverture de l'éditeur après intégration des paramètres")
}
