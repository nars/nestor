// Copyright 2020 NDP Systèmes. All Rights Reserved.
// See LICENSE file for full licensing details.

package cmd

import (
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/nars/nestor/pkg/nestor"
)

var execPostgresql bool

var execCmd = &cobra.Command{
	Use:   "exec <instance> <command>",
	Short: "Execute une commande dans le pod Odoo d'une instance",
	Long:  `Execute une commande dans un pod Odoo de l'instance spécifiée.`,
	Args:  cobra.MinimumNArgs(2),
	ValidArgsFunction: func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
		cfg, _ := getConfig()
		var res []string
		switch len(args) {
		case 0:
			res = getInstanceList(cfg, toComplete)
		}
		return res, cobra.ShellCompDirectiveNoFileComp
	},
	Run: func(cmd *cobra.Command, args []string) {
		cfg, _ := getConfig()
		instance, err := nestor.InstanceByName(cfg, args[0])
		checkError(err)
		err = instance.Exec(strings.Join(args[1:], " "), execPostgresql, true)
		checkError(err)
	},
}

func init() {
	execCmd.Flags().BoolVar(&execPostgresql, "postgresql", false, "Execute la commande sur le pod postgresql au lieu de ceux d'Odoo.")
}
