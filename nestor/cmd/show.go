// Copyright 2020 NDP Systèmes. All Rights Reserved.
// See LICENSE file for full licensing details.

package cmd

import (
	"os"

	"github.com/spf13/cobra"
	"gitlab.com/nars/nestor/pkg/nestor"
)

var (
	showOutput string
)

var showCmd = &cobra.Command{
	Use:     "show <instance>",
	Aliases: []string{"get"},
	Short:   "Affiche les informations d'une instance",
	Long:    "Affiche les informations de l'instance donnée.",
	Args:    cobra.ExactArgs(1),
	ValidArgsFunction: func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
		cfg, _ := getConfig()
		res := getInstanceList(cfg, toComplete)
		return res, cobra.ShellCompDirectiveDefault
	},
	Run: func(cmd *cobra.Command, args []string) {
		cfg, _ := getConfig()
		instance, err := nestor.InstanceByName(cfg, args[0])
		checkError(err)
		switch showOutput {
		case "yaml":
			err = instance.YAML(os.Stdout, 0)
		case "spec":
			err = instance.Spec(os.Stdout)
		default:
			err = instance.Details(os.Stdout)
		}
		checkError(err)
	},
}

func init() {
	showCmd.Flags().StringVarP(&showOutput, "output", "o", "text", "Format de sortie 'text', 'yaml' ou 'spec'")
	err := showCmd.RegisterFlagCompletionFunc("output", func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
		return []string{"text", "yaml", "spec"}, cobra.ShellCompDirectiveNoFileComp
	})
	checkError(err)
}
