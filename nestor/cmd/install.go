// Copyright 2020 NDP Systèmes. All Rights Reserved.
// See LICENSE file for full licensing details.

package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
	"gitlab.com/nars/nestor/pkg/nestor"
)

var (
	installDatabase string
	installDeleteDB bool
)

var installCmd = &cobra.Command{
	Use:   "install <instance> <module>",
	Short: "Installe module Odoo",
	Long: `Installe le module Odoo spécifié sur l'instance spécifiée.
Cette commande crée également la base de donnée si elle n'existe pas.'`,
	Args: cobra.ExactArgs(2),
	ValidArgsFunction: func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
		cfg, _ := getConfig()
		var (
			res    []string
			shComp cobra.ShellCompDirective
		)
		switch len(args) {
		case 0:
			res = getInstanceList(cfg, toComplete)
			shComp = cobra.ShellCompDirectiveNoFileComp
		case 1:
			shComp = cobra.ShellCompDirectiveDefault
		}
		return res, shComp
	},
	Run: func(cmd *cobra.Command, args []string) {
		cfg, _ := getConfig()
		instance, err := nestor.InstanceByName(cfg, args[0])
		checkError(err)
		if installDeleteDB {
			err = instance.DropDB(installDatabase)
			checkError(err)
		}
		err = instance.InstallModule(installDatabase, args[1])
		checkError(err)
		fmt.Printf("L'instance '%s' est en cours de redémarrage.\n", instance.Name())
	},
}

func init() {
	installCmd.Flags().StringVarP(&installDatabase, "database", "d", "",
		"Nom de la base de données à utiliser. Si la valeur n'est pas définie, le nom de l'instance sera utilisé.")
	installCmd.Flags().BoolVar(&installDeleteDB, "delete-db", false,
		"Supprimer la base de donnée avant de la recréer et d'installer le module.")
}
