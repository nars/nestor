// Copyright 2020 NDP Systèmes. All Rights Reserved.
// See LICENSE file for full licensing details.

package cmd

import (
	"errors"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/nars/nestor/pkg/kube"
	"gitlab.com/nars/nestor/pkg/nestor"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	clientcmdapi "k8s.io/client-go/tools/clientcmd/api"
)

const (
	kubernetesSecretsDir = "/var/run/secrets/kubernetes.io/serviceaccount"
	s3DefaultService     = "s3-ovh-gra"
	s3DefaultBucket      = "nestor-dumps"
	nestorVersion        = "v0.3.1"
)

var (
	kubeContext           string
	s3ReadAllowedBuckets  = []string{s3DefaultBucket}
	s3WriteAllowedBuckets = []string{s3DefaultBucket}
)

var RootCmd = &cobra.Command{
	Use:     "nestor",
	Short:   "Nestor est un utilitaire de gestion d'instances",
	Version: nestorVersion,
	Long: fmt.Sprintf(`Nestor %s
Nestor est l'utilitaire de gestion des instances de qualification et de production de NDP Systèmes.`, nestorVersion),
}

func checkError(err error) {
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

// getInClusterConfig checks whether we are running inside a Kubernetes cluster and returns
// a configuration and a namespace from /var/run/secrets/kubernetes.io/serviceaccount/ if
// it is the case
func getInClusterConfig() (*rest.Config, string, bool) {
	cfg, err := rest.InClusterConfig()
	if err != nil {
		return nil, "", false
	}
	namespace, err := os.ReadFile(filepath.Join(kubernetesSecretsDir, "namespace"))
	checkError(err)
	return cfg, string(namespace), true
}

// getConfig returns the kubernetes configuration to use (from default kubernetes config)
// Second returned argument is true if this is a production configuration
func getConfig() (*kube.Config, bool) {
	// If we are in-cluster, then we use in-cluster config
	if icc, ns, ok := getInClusterConfig(); ok {
		return &kube.Config{
			Config:    icc,
			Namespace: ns,
		}, false
	}
	// Get selected context
	pathOption := clientcmd.NewDefaultPathOptions()
	sc, err := pathOption.GetStartingConfig()
	checkError(err)
	// Force context if defined by kubeContext
	kCtx := sc.CurrentContext
	if kubeContext != "" {
		kCtx = kubeContext
	}
	cfg, ctxt := getNamedConfig(kCtx)
	if kube.IsProductionContext(ctxt) && kubeContext == "" {
		// Production contexts must be called with --kube
		checkError(errors.New("vous ne pouvez pas exécuter de commande sur une configuration de production sans le flag --kube"))
	}
	return cfg, kube.IsProductionContext(ctxt)
}

// getNamedConfig returns the config with the given name (kubeconfig context name)
func getNamedConfig(configName string) (*kube.Config, *clientcmdapi.Context) {
	// Get selected context
	pathOption := clientcmd.NewDefaultPathOptions()
	sc, err := pathOption.GetStartingConfig()
	checkError(err)

	// Check kube context exists
	if _, ok := sc.Contexts[configName]; !ok {
		checkError(fmt.Errorf("la configuration '%s' est inconnue.\n"+
			"Tapez 'nestor config list' pour avoir la liste des configurations valides.", configName))
	}

	// Return configuration
	context := clientcmdapi.NewContext()
	sc.Contexts[configName].DeepCopyInto(context)
	config := clientcmd.NewDefaultClientConfig(*sc, &clientcmd.ConfigOverrides{
		Context: *context,
	})
	clientConfig, err := config.ClientConfig()
	checkError(err)
	return &kube.Config{
		Config:      clientConfig,
		ClusterName: context.Cluster,
		Namespace:   context.Namespace,
	}, context
}

// getConfigList returns a list of configuration for autocompletion
// If withProduction is true, also include production configs
func getConfigList(withProduction bool) []string {
	contexts, err := kube.ListConfigs()
	checkError(err)
	var res []string
	for _, ctx := range contexts {
		if ctx.Production() && !withProduction {
			continue
		}
		res = append(res, ctx.Name)
	}
	return res
}

func getInstanceList(cfg *kube.Config, prefix string) []string {
	instances, _ := nestor.InstanceNames(cfg)
	var res []string
	for _, instance := range instances {
		if strings.HasPrefix(instance, prefix) {
			res = append(res, instance)
		}
	}
	return res
}

func init() {
	RootCmd.PersistentFlags().StringVar(&kubeContext, "kube", "",
		"La commande va s'appliquer sur la configuration kubernetes donnée")
	err := RootCmd.RegisterFlagCompletionFunc("kube", func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
		return getConfigList(true), cobra.ShellCompDirectiveNoFileComp
	})
	checkError(err)

	RootCmd.AddCommand(completionCmd)
	RootCmd.AddCommand(configCmd)
	RootCmd.AddCommand(consoleOdooCmd)
	RootCmd.AddCommand(cpCmd)
	RootCmd.AddCommand(databaseCmd)
	RootCmd.AddCommand(dbportCmd)
	RootCmd.AddCommand(deleteCmd)
	RootCmd.AddCommand(dumpCmd)
	RootCmd.AddCommand(editCmd)
	RootCmd.AddCommand(execCmd)
	RootCmd.AddCommand(filestoreCmd)
	RootCmd.AddCommand(installCmd)
	RootCmd.AddCommand(listCmd)
	RootCmd.AddCommand(logsCmd)
	RootCmd.AddCommand(newCmd)
	RootCmd.AddCommand(pgActivityCmd)
	RootCmd.AddCommand(psqlCmd)
	RootCmd.AddCommand(restartCmd)
	RootCmd.AddCommand(restoreCmd)
	RootCmd.AddCommand(s3Cmd)
	RootCmd.AddCommand(showCmd)
	RootCmd.AddCommand(startCmd)
	RootCmd.AddCommand(stopCmd)
	RootCmd.AddCommand(updateCmd)
	RootCmd.AddCommand(userCmd)
	RootCmd.AddCommand(versionCmd)
	RootCmd.AddCommand(waitCmd)
}
