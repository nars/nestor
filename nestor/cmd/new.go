// Copyright 2020 NDP Systèmes. All Rights Reserved.
// See LICENSE file for full licensing details.

package cmd

import (
	"errors"
	"fmt"
	"io"
	"os"
	"strings"

	"sigs.k8s.io/yaml"

	"github.com/spf13/cobra"
	"gitlab.com/nars/nestor/pkg/kube"
	"gitlab.com/nars/nestor/pkg/nestor"
)

var (
	newRepository        string
	newOdooVersion       string
	newBranch            string
	newPGDiskSize        string
	newQueueJobsEnabled  bool
	newQueueJobsChannels string
	newSet               string
	newInteractive       bool
)

var newCmd = &cobra.Command{
	Use:   "new <instance> [-]",
	Short: "Crée une nouvelle instance",
	Long: `Crée une nouvelle instance avec le nom donné.
Si au moins un flag est spécifié, les valeurs des flags seront utilisées comme variables de l'instance.
Sinon, la liste des variable sera proposée dans l'éditeur.

Si vous ajoutez - à la fin de la commande, les paramètres seront lus depuis stdin au format yaml.

En passant le flag --interactive, vous pouvez forcer l'ouverture de l'éditeur après intégration des données des flags.`,
	Args: cobra.RangeArgs(1, 2),
	ValidArgsFunction: func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
		var res []string
		cfg, _ := getConfig()
		switch len(args) {
		case 0:
			res = getInstanceList(cfg, toComplete)
		case 1:
			res = []string{"-"}
		}
		return res, cobra.ShellCompDirectiveNoFileComp
	},
	Run: func(cmd *cobra.Command, args []string) {
		name := args[0]
		if len(name) > 33 {
			checkError(errors.New("le nom de l'instance doit faire moins de 33 caractères"))
		}
		cfg, _ := getConfig()
		values := createValuesMap(cfg, newSet, newRepository, newOdooVersion, newBranch, newPGDiskSize, newQueueJobsEnabled, newQueueJobsChannels)
		if len(args) > 1 && args[1] == "-" {
			fmt.Println("Veuillez copier/coller votre configuration au format YAML, puis terminez par Ctrl-D")
			data, err := io.ReadAll(os.Stdin)
			checkError(err)
			var iVals map[string]interface{}
			err = yaml.Unmarshal(data, &iVals)
			checkError(err)
			values = nestor.CoalesceTables(values, iVals)
		}
		_, err := nestor.NewInstance(cfg, name, values, newInteractive)
		checkError(err)
		fmt.Printf("L'instance '%s' est en cours de création\n", args[0])
	},
}

type option struct {
	key   string
	value interface{}
}

func parseSetOptions(setOptions string) ([]option, error) {
	var res []option
	options := strings.Split(setOptions, ",")
	for _, opt := range options {
		toks := strings.Split(opt, "=")
		if len(toks) != 2 {
			return nil, fmt.Errorf("erreur de syntaxe dans les options fournies dans --set : l'opt '%s' n'est pas sous la forme 'chemin=valeur'", opt)
		}
		// get the value with the correct type
		var value interface{}
		err := yaml.Unmarshal([]byte(toks[1]), &value)
		if err != nil {
			return nil, err
		}
		res = append(res, option{
			key:   toks[0],
			value: value,
		})
	}
	return res, nil
}

func createValuesMap(_ *kube.Config, setOptions, repository, odooVersion, branch, pgDiskSize string, queueJobsEnabled bool, queueJobsChannels string) map[string]interface{} {
	res := make(map[string]interface{})
	if repository != "" {
		toks := strings.Split(repository, "@")
		nestor.SetValueInTable("spec.sources.repositories.DEPOT_GIT.path", toks[0], &res)
		if len(toks) > 1 {
			nestor.SetValueInTable("spec.sources.repositories.DEPOT_GIT.branch", toks[1], &res)
		}
	}
	if odooVersion != "" {
		nestor.SetValueInTable("spec.version", odooVersion, &res)
	}
	if branch != "" {
		nestor.SetValueInTable("spec.sources.branch", branch, &res)
	}
	if pgDiskSize != "" {
		nestor.SetValueInTable("spec.postgresql.volume.size", pgDiskSize, &res)
	}
	if queueJobsChannels != "" {
		nestor.SetValueInTable("spec.options.queueJobs.channels", queueJobsChannels, &res)
		queueJobsEnabled = true
	}
	if queueJobsEnabled {
		nestor.SetValueInTable("spec.options.queueJobs.enabled", queueJobsEnabled, &res)
	}
	if setOptions != "" {
		options, err := parseSetOptions(setOptions)
		checkError(err)
		for _, opt := range options {
			nestor.SetValueInTable(opt.key, opt.value, &res)
		}
	}
	if len(res) == 0 {
		return nil
	}
	return res
}

func init() {
	newCmd.Flags().StringVar(&newRepository, "repository", "", "Spécifie un path de dépôt git pour les sources du projet. "+
		"Remplit la variable git.repositories.DEPOT_GIT.path. Peut être spécifié sous la forme 'path@branch'.")
	newCmd.Flags().StringVar(&newOdooVersion, "odoo-version", "", "Spécifie la version Odoo (par défaut 12.0)")
	newCmd.Flags().StringVar(&newSet, "set", "", "Spécifie les options depuis la ligne de commande séparées par des virgules. "+
		"Ces options sont prioritaires sur toutes les autres.")
	newCmd.Flags().StringVar(&newBranch, "branch", "", "Spécifie la branche à utiliser pour tous les dépôt. "+
		"Par défaut, il s'git de la branche avec le même nom que la version d'Odoo.")
	newCmd.Flags().StringVar(&newPGDiskSize, "pg-disk-size", "", "Spécifie une taille de disque pour PG ('8G' par défaut).")
	newCmd.Flags().BoolVar(&newQueueJobsEnabled, "queue-job-enabled", false, "Active les queue jobs dans l'instance")
	newCmd.Flags().StringVar(&newQueueJobsChannels, "queue-job-channels", "", "Définit les channels pour les queue jobs. "+
		"Si ce flag est défini, les queue jobs sont automatiquement activés.")
	newCmd.Flags().BoolVarP(&newInteractive, "interactive", "i", false, "Force l'ouverture de l'éditeur après intégration des paramètres")
}
