// Copyright 2020 NDP Systèmes. All Rights Reserved.
// See LICENSE file for full licensing details.

package cmd

import (
	"bufio"
	"fmt"
	"os"
	"strings"

	"github.com/spf13/cobra"
	"gitlab.com/nars/nestor/pkg/nestor"
)

var deleteCmd = &cobra.Command{
	Use:   "delete <instance>",
	Short: "Supprime une instance",
	Long: `Supprime l'instance donnée. 
Nestor refusera par principe de supprimer une instance de production.`,
	Args: cobra.ExactArgs(1),
	ValidArgsFunction: func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
		cfg, _ := getConfig()
		res := getInstanceList(cfg, toComplete)
		return res, cobra.ShellCompDirectiveDefault
	},
	Run: func(cmd *cobra.Command, args []string) {
		cfg, isProduction := getConfig()
		if isProduction {
			fmt.Println("nestor ne peut pas supprimer d'instance de production")
			os.Exit(1)
		}
		instance, err := nestor.InstanceByName(cfg, args[0])
		checkError(err)
		fmt.Println("Toutes les données vont être supprimées. Cette action est irreversible.")
		fmt.Printf("Êtes-vous sûr de vouloir supprimer l'instance '%s'? [o/N] ", args[0])
		scanner := bufio.NewScanner(os.Stdin)
		scanner.Scan()
		if strings.ToUpper(scanner.Text()) == "O" || strings.ToUpper(scanner.Text()) == "Y" {
			err = instance.Delete()
			checkError(err)
			fmt.Printf("Instance '%s' en cours de suppression.\n", args[0])
			os.Exit(0)
		}
		fmt.Println("Suppression annulée.")
	},
}
