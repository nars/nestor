// Copyright 2020 NDP Systèmes. All Rights Reserved.
// See LICENSE file for full licensing details.

package cmd

import (
	"github.com/spf13/cobra"
	"gitlab.com/nars/nestor/pkg/nestor"
)

var restartPostgresql bool

var restartCmd = &cobra.Command{
	Use:   "restart <instance>",
	Short: "Redémarre l'instance",
	Long: `Redémarre le pod Odoo de l'instance en le supprimant.
Si le flag --postgresql est spécifié, le pod posqtgresql sera redémarré à la place.`,
	Args: cobra.ExactArgs(1),
	ValidArgsFunction: func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
		cfg, _ := getConfig()
		res := getInstanceList(cfg, toComplete)
		return res, cobra.ShellCompDirectiveDefault
	},
	Run: func(cmd *cobra.Command, args []string) {
		cfg, _ := getConfig()
		instance, err := nestor.InstanceByName(cfg, args[0])
		checkError(err)
		err = instance.Restart(restartPostgresql)
		checkError(err)
	},
}

func init() {
	restartCmd.Flags().BoolVar(&restartPostgresql, "postgresql", false, "Execute la commande sur le pod postgresql au lieu de ceux d'Odoo.")
}
