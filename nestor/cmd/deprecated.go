// Copyright 2021 NDP Systèmes. All Rights Reserved.
// See LICENSE file for full licensing details.

package cmd

import (
	"github.com/spf13/cobra"
)

var (
	dumpS3       string
	dumpBucket   string
	dumpDatabase string
)

var dumpCmd = &cobra.Command{
	Use:        "dump <instance> (local|s3) <fichier>",
	Short:      "OBSOLETE - Remplacé par 'nestor database dump'",
	Long:       `OBSOLETE - Remplacé par 'nestor database dump'`,
	Args:       cobra.ExactArgs(3),
	Deprecated: "cette commande sera supprimée dans une prochaine version.\nUtilisez 'nestor database dump' à la place.",
	Run: func(cmd *cobra.Command, args []string) {
		dbS3 = dumpS3
		dbBucket = dumpBucket
		dbDatabase = dumpDatabase
		databaseDumpCmd.Run(cmd, args)
	},
}

func init() {
	dumpCmd.Flags().StringVarP(&dumpDatabase, "database", "d", "",
		"Nom de la base de données à utiliser. Si la valeur n'est pas définie, le nom de l'instance sera utilisé.")
	dumpCmd.Flags().StringVar(&dumpS3, "s3", s3DefaultService, "Service S3 à utiliser")
	dumpCmd.Flags().StringVar(&dumpBucket, "bucket", s3DefaultBucket, "Bucket à utiliser pour le dump s3")
	err := dumpCmd.RegisterFlagCompletionFunc("bucket", func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
		return s3WriteAllowedBuckets, cobra.ShellCompDirectiveNoFileComp
	})
	checkError(err)
}

var (
	restoreS3               string
	restoreBucket           string
	restoreDatabase         string
	restoreNoClean          bool
	restoreNoResetPasswords bool
	restoreSetAllUsers      bool
)

var restoreCmd = &cobra.Command{
	Use:        "restore <instance> (local|s3) <fichier>",
	Short:      "OBSOLETE - Remplacé par 'nestor database restore'",
	Long:       "OBSOLETE - Remplacé par 'nestor database restore'",
	Deprecated: "cette commande sera supprimée dans une prochaine version.\nUtilisez 'nestor database restore' à la place.",
	Args:       cobra.ExactArgs(3),
	Run: func(cmd *cobra.Command, args []string) {
		dbS3 = restoreS3
		dbBucket = restoreBucket
		dbDatabase = restoreDatabase
		dbRestoreNoClean = restoreNoClean
		dbRestoreNoResetPasswords = restoreNoResetPasswords
		dbRestoreSetAllUsers = restoreSetAllUsers
		databaseRestoreCmd.Run(cmd, args)
	},
}

func init() {
	restoreCmd.Flags().BoolVar(&restoreNoClean, "no-clean", false,
		"Ne pas nettoyer le dump lors de la restoration.")
	restoreCmd.Flags().StringVarP(&restoreDatabase, "database", "d", "",
		"Nom de la base de données à utiliser. Si la valeur n'est pas définie, le nom de l'instance sera utilisé.")
	restoreCmd.Flags().BoolVar(&restoreNoResetPasswords, "no-reset-passwords", false,
		"Ne modifie pas les mots de passe lors du nettoyage")
	restoreCmd.Flags().BoolVar(&restoreSetAllUsers, "set-password-to-all", false,
		"Applique le mot de passe admin à tous les utilisateurs lors du nettoyage")
	restoreCmd.Flags().StringVar(&restoreS3, "s3", s3DefaultService, "Service S3 à utiliser")
	restoreCmd.Flags().StringVar(&restoreBucket, "bucket", s3DefaultBucket, "Bucket à utiliser pour la restoration s3")
	err := restoreCmd.RegisterFlagCompletionFunc("bucket", func(cmd *cobra.Command, args []string, toComplete string) ([]string, cobra.ShellCompDirective) {
		return s3ReadAllowedBuckets, cobra.ShellCompDirectiveNoFileComp
	})
	checkError(err)
}

var dbportCmd = &cobra.Command{
	Use:        "dbport <instance> <port_local>",
	Short:      "OBSOLETE - Remplacé par 'nestor database port'",
	Long:       `OBSOLETE - Remplacé par 'nestor database port'`,
	Deprecated: "cette commande sera supprimée dans une prochaine version.\nUtilisez 'nestor database port' à la place.",
	Args:       cobra.ExactArgs(2),
	Run: func(cmd *cobra.Command, args []string) {
		databasePortCmd.Run(cmd, args)
	},
}

var psqlDatabase string

var psqlCmd = &cobra.Command{
	Use:        "psql <instance>",
	Short:      "OBSOLETE - Remplacé par 'nestor database client'",
	Long:       "OBSOLETE - Remplacé par 'nestor database client'",
	Deprecated: "cette commande sera supprimée dans une prochaine version.\nUtilisez 'nestor database client' à la place.",
	Args:       cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		dbDatabase = psqlDatabase
		databaseClientCmd.Run(cmd, args)
	},
}

func init() {
	psqlCmd.Flags().StringVarP(&psqlDatabase, "database", "d", "",
		"Nom de la base de données à utiliser. Si la valeur n'est pas définie, le nom de l'instance sera utilisé.")
}

var pgActivityCmd = &cobra.Command{
	Use:        "pg_activity <instance>",
	Short:      "OBSOLETE - Remplacé par 'nestor database pg_activity'",
	Long:       "OBSOLETE - Remplacé par 'nestor database pg_activity'",
	Deprecated: "cette commande sera supprimée dans une prochaine version.\nUtilisez 'nestor database pg_activity' à la place.",
	Args:       cobra.ExactArgs(1),
	Run: func(cmd *cobra.Command, args []string) {
		databasePGActivityCmd.Run(cmd, args)
	},
}
