// Copyright 2020 NDP Systèmes. All Rights Reserved.
// See LICENSE file for full licensing details.

package cmd

import (
	"fmt"
	"strings"

	"github.com/gosuri/uitable"
	"github.com/spf13/cobra"
	"gitlab.com/nars/nestor/pkg/nestor"
)

var (
	listQuiet  bool
	listFormat string
)

var listCmd = &cobra.Command{
	Use:     "list",
	Short:   "Liste les instances",
	Long:    "Liste les instances déployées.",
	Aliases: []string{"ls"},
	Args:    cobra.NoArgs,
	Run: func(cmd *cobra.Command, args []string) {
		cfg, _ := getConfig()
		if listQuiet {
			names, err := nestor.InstanceNames(cfg)
			checkError(err)
			fmt.Println(strings.Join(names, "\n"))
			return
		}
		instances, err := nestor.Instances(cfg)
		checkError(err)
		data := nestor.FormatInstances(instances, listFormat, true)
		table := uitable.New()
		for _, r := range data {
			table.AddRow(r...)
		}
		fmt.Println(table.String())
	},
}

func init() {
	listCmd.Flags().BoolVarP(&listQuiet, "quiet", "q", false,
		"Affiche uniquement la liste des noms des instances")
	listCmd.Flags().StringVarP(&listFormat, "format", "f", "name,url,age",
		`Définit les colonnes à afficher.
Le format est une liste de paths dans le YAML, séparés par des virgules.
Les chemins spéciaux suivants sont disponibles également: 
	name, url, namespace, age, password, pg-password
`)
}
