module gitlab.com/nars/nestor

go 1.16

require (
	github.com/gosuri/uitable v0.0.4
	github.com/imdario/mergo v0.3.9
	github.com/rclone/rclone v1.55.0
	github.com/spf13/cobra v1.1.1
	github.com/zalando/postgres-operator v1.5.1-0.20200903060246-03437b63749e
	gitlab.com/nars/odoo-operator v0.0.0-20220708160856-ea1db6eea576
	golang.org/x/crypto v0.0.0-20210220033148-5ea612d1eb83
	k8s.io/api v0.19.2
	k8s.io/apiextensions-apiserver v0.19.2
	k8s.io/apimachinery v0.19.2
	k8s.io/cli-runtime v0.19.2
	k8s.io/client-go v0.19.2
	k8s.io/kubectl v0.19.2
	sigs.k8s.io/controller-runtime v0.6.2
	sigs.k8s.io/yaml v1.2.0
)
