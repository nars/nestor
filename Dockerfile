FROM golang:1.19-alpine AS build_base

RUN apk add git openssh-client

WORKDIR /tmp/nestor

COPY . .

RUN go build -o ./out/nestor gitlab.com/nars/nestor


# Start fresh from a smaller image
FROM alpine:3.12.4

COPY --from=build_base /tmp/nestor/out/nestor /usr/local/bin/

ENTRYPOINT ["nestor"]
